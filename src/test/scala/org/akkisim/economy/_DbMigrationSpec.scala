/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy

import org.akkisim.economy.services.FlywayService
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.slf4j.{Logger, LoggerFactory}


/**
  * @author Akira Sonoda
  */
class _DbMigrationSpec extends WordSpecLike with Matchers with BeforeAndAfterAll
    with Configuration {
    val log: Logger = LoggerFactory.getLogger("_DbMigrationTest")

    // Only use to override the configured url
    // dbJDBCUrl = "jdbc:mysql://db.docker/akkigrid_test"

    val flywayService = new FlywayService(dbJDBCUrl, dbUsername, dbPassword)

    "The Database Migration" should {
        "be performed and produce a correct Return Code" in {
            val migrationResult = flywayService.migrateDatabaseSchema()
            assert((migrationResult == 1) || (migrationResult == 0))
        }
    }
}
