/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.utils

import org.akkisim.economy.Configuration
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.slf4j.{Logger, LoggerFactory}
import slick.codegen.SourceCodeGenerator


/**
  * @author Akira Sonoda
  */
class CodegenSpec extends WordSpecLike with Matchers with BeforeAndAfterAll with Configuration {
    val log: Logger = LoggerFactory.getLogger("Codegen")

    "Slick Codegen " should {
        "Generate the Database Classes and produce a Sourcecode File" in {
            // assert(true)
            val slickDriver = "slick.jdbc.MySQLProfile"
            val jdbcDriver = "com.mysql.jdbc.Driver"
            val outputFolder = "db/codegen"
            val pkg = "akkisim.db"
            val dbUssername = "akkieconomy"
            val dbPassword = "akkieconomy"

            SourceCodeGenerator.main(
                Array(slickDriver, jdbcDriver, dbJDBCUrl, outputFolder, pkg, dbUsername, dbPassword)
            )
        }
    }
}
