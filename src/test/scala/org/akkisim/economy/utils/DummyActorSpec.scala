/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.utils

import akka.actor.{Actor, ActorLogging, ActorRef}
import org.akkisim.economy.dataproviders.types.{DeRegisterUser, RegisterPushUrl, RegisterUser, SendHeartbeat}

/**
  * @author Akira Sonoda
  */
class DummyActorSpec extends Actor with ActorLogging  {
    def receive: Receive = {
        case SendHeartbeat =>
            log.debug("SendHeartbeat");
        case RegisterUser(avatarUUID: String, actorRef: ActorRef) =>
            log.debug("RegisterUser: avatarUUID: "+avatarUUID+" - @actorRef" )
        case DeRegisterUser(avatarUUID: String) =>
            log.debug("DeRegisterUser: avatarUUID: " + avatarUUID)
        case RegisterPushUrl(avatarUUID: String, pushUrl: String) =>
            log.debug("RegisterPushUrl: @avatarUUID:"+avatarUUID+" - pushUrl: "+pushUrl)
    }
}
