package org.akkisim.economy.dataproviders.items

import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.ItemSpecEntity
import org.scalatest.{BeforeAndAfterAll, FunSuite}

/**
  * @author Akira Sonoda
  */
class InventoryItemTypeTest extends FunSuite with BeforeAndAfterAll {

    override def beforeAll ( ): Unit = {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20))
    }

    test ( "test get Int from ItemType" ) {
        val result = ItemSpec.getIntOfKind("WaterTank")
        assert(result == 1)
    }

    test ( "test get String from ItemType(1)" ) {
        val result = ItemSpec.getKindOfInt(1)
        assert(result.equalsIgnoreCase("WaterTank"))
    }

}
