/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.dataproviders.types.UUID_ZERO
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.jawn._
import io.circe.syntax._
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.items.{Plant, PlantUpdate, StoreItem}
import org.scalatest.{FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}


/**
  * @author Akira Sonoda
  */
class JSonConversionSpec extends FeatureSpec with GivenWhenThen with FailFastCirceSupport {
    val log: Logger = LoggerFactory.getLogger("JSonConversionSpec")
    // A Plant


    info("As economy User")
    info("I want to be able to convert data from and to json")
    info("in order to implement the datatransfer as REST service")
    
    feature("Convert datatypes") {

        scenario("Convert PlantUpdate to JSON and back") {
            Given("a PlantUpdate exists")
                val plantUpdate = PlantUpdate(
                    itemType = "PLANT",
                    avatarUUID = BaseTestData.inventoryItemPlant1.itemownerid,
                    growthChange = 100,
                    modification = "Subtraction"
                )
            When("I convert the PlantUpdate to JSon")
                val jsonString = plantUpdate.asJson.toString()
            Then("The Json String should look like: {\n  \"avatarUUID\" : \"102dba0-4711-11e7-9598-0800200d9e01\",\n  \"itemType\" : \"PLANT\",\n  \"volumeChange\" : 0,\n  \"growthChange\" : 100,\n  \"modification\" : \"Subtraction\"\n}")
                assert(jsonString.equalsIgnoreCase("{\n  \"avatarUUID\" : \"102dba0-4711-11e7-9598-0800200d9e01\",\n  \"itemType\" : \"PLANT\",\n  \"volumeChange\" : 0,\n  \"growthChange\" : 100,\n  \"modification\" : \"Subtraction\"\n}"))
            When("I cronvert this String to PlantUpdate: {\n  \"avatarUUID\" : \"102dba0-4711-11e7-9598-0800200d9e01\",\n  \"itemType\" : \"PLANT\",\n  \"volumeChange\" : 0,\n  \"growthChange\" : 100,\n  \"modification\" : \"Subtraction\"\n}")
            Then("growthChange value has to be 100")
                decode[PlantUpdate](jsonString) match {
                    case Left(s) => fail("Error" + s + "when convert JsonString: " + jsonString)
                    case Right(p) =>
                        assert(p.growthChange == 100)
                }
        }

        scenario("Convert Plant to JSON and back") {
            Given("a Plant exists")
                val plant = BaseTestData.plant1
            When("I convert the Plant to JSon")
                val jsonString = plant.asJson.toString()
            Then ("The generated String needs to contain the element plantType")
                assert(jsonString.contains("plantType"))
            When ("I decode the JSON String to a Plant")
            Then ("The Field plantType needs to have a value of PLANT_TREE")
                decode[Plant](jsonString) match {
                    case Left(s) => fail("Error" + s + "when converting JsonString: " + jsonString)
                    case Right(p) =>
                        assert(p.plantType.equalsIgnoreCase("PLANT_TREE"))
                }
        }

        scenario("Convert MarketInventory to JSON and back") {
            Given("a MarketInventory exists")
                val marketinventory = BaseTestData.marketInventory1
            When("I convert the marketinventory to JSon")
                val jsonString = marketinventory.asJson.toString()
            Then ("The generated String needs to contain the element origin")
                assert(jsonString.contains("origin"))
            When ("I decode the JSON String to a MarketInventory")
            Then ("The Field origin needs to have a value of " + UUID_ZERO)
                decode[MarketInventory](jsonString) match {
                    case Left(s) => fail("Error" + s + "when converting JsonString: " + jsonString)
                    case Right(p) =>
                        assert(p.origin.equalsIgnoreCase(UUID_ZERO))
                }
        }

        scenario("Convert ItemSpecEntity to JSON and back") {
            Given("a ItemSpecEntity exists")
                val itemSpecEntity = BaseTestData.itemSpec1
            When("I convert the marketinventory to JSon")
                val jsonString = itemSpecEntity.asJson.toString()
            Then ("The generated String needs to contain the element origin")
                assert(jsonString.contains("kind"))
            When ("I decode the JSON String to a ItemSpecEntity")
            Then ("The Field kind needs to have a value of BASKET_COCONUT")
                decode[ItemSpecEntity](jsonString) match {
                    case Left(s) => fail("Error" + s + "when converting JsonString: " + jsonString)
                    case Right(p) =>
                        assert(p.kind.equalsIgnoreCase("BASKET_COCONUT"))
            }
        }

        scenario("Convert StoreItem to JSON and back") {
            Given("a StoreItem  exists")
                val storeItem = BaseTestData.storeItem1
            When("I convert the marketinventory to JSon")
                val jsonString = storeItem.asJson.toString()
            Then ("The generated String needs to contain the element origin")
                assert(jsonString.contains("itemtype"))
            When ("I decode the JSON String to a StoreItem")
            Then ("The Field itemtype needs to have a value of BASKET_COCONUT")
            decode[StoreItem](jsonString) match {
                case Left(s) => fail("Error" + s + "when converting JsonString: " + jsonString)
                case Right(p) =>
                    assert(p.itemtype.equalsIgnoreCase("BASKET_COCONUT"))
            }
        }


        //        scenario("Convert: {\"price\":0,\"id\":\"102dba0-4711-11e7-9598-0800200c9a66\",\"ownerid\":\"102dba0-4bc7-4711-9598-0800200c9a66\",\"customer\":\"102dba0-4bc7-4711-9598-0800200c9a66\",\"kind\":\"GrowVegetables\",\"daystocomplete\":0} to TaskEntity") {
//
//            Given("a JsonString like: {\"price\":0,\"id\":\"102dba0-4711-11e7-9598-0800200c9a66\",\"ownerid\":\"102dba0-4bc7-4711-9598-0800200c9a66\",\"customer\":\"102dba0-4bc7-4711-9598-0800200c9a66\",\"kind\":\"GrowVegetables\",\"daystocomplete\":0}")
//            val jsonString = "{\"price\":0,\"id\":\"102dba0-4711-11e7-9598-0800200c9a66\",\"ownerid\":\"102dba0-4bc7-4711-9598-0800200c9a66\",\"customer\":\"102dba0-4bc7-4711-9598-0800200c9a66\",\"kind\":\"GrowVegetables\",\"daystocomplete\":0}"
//
//            When("converted to TaskEntity")
//            decode[TaskEntity](jsonString) match {
//                case Left(s) => fail("Error" + s + "when convert JsonString: " + jsonString)
//                case Right(s) =>
//                    Then("id = \"102dba0-4711-11e7-9598-0800200c9a66\"")
//                    assert(s.id.equals("102dba0-4711-11e7-9598-0800200c9a66"))
//
//                    And("kind = Some(\"GrowVegetables\")")
//                    s.kind match {
//                        case Some(r) => assert(r.equals("GrowVegetables"))
//                        case None => fail("No kind given")
//                    }
//
//                    And("price = Some(0)")
//                    s.price match {
//                        case Some(p) => assert(p == 0)
//                        case None => fail("No price given")
//                    }
//
//                    And("transactionid = none")
//                    s.transactionid match {
//                        case Some(t) => fail("TransactionId should be None got: " + t)
//                        case None => assert(true)
//                    }
//
//                    And("daystocomplete = Some(0)")
//                    s.daystocomplete match {
//                        case Some(d) => assert(d == 0)
//                        case None => fail("No daystocomplete given")
//                    }
//            }
//        }
    }
}


