/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.akkisim.economy.dataproviders

import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.ItemSpecEntity
import org.akkisim.economy.dataproviders.items.ItemState
import org.akkisim.economy.dataproviders.types.MeasureUnit
import org.scalatest.{BeforeAndAfterAll, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}


/**
  * @author Akira Sonoda
  */
class EnumConversionSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll{

    val log: Logger = LoggerFactory.getLogger("EnumConversionSpec")

    override def beforeAll ( ): Unit = {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20))
    }


    info("As economy User")
    info("I want to be able to convert use enumerations as String")
    info("in Order to easily convert Strings to the values stored in the database")

    feature("Convert Enums") {

        scenario("convert WATERTANK from ItemType Enumeration to Value") {
            Given("A ItemType Enumeration is available")
            When("I request to convert WaterTank to a value")
            val result = ItemSpec.getIntOfKind("waTerTank")
            Then("The result should be 1")
            result match {
                case 1 => assert(true)
                case _ => fail("No WaterTAnk Enumeration in ItemType found")
            }
        }

        scenario("convert 1 from ItemType Enumeration to WATERTANK") {
            Given("A ItemType Enumeration is available")
            When("I request to convert 1 to a CONTAINER")
            val result = ItemSpec.getKindOfInt(1)
            Then("The result should be WATERTANK")
            result match {
                case "WATERTANK" => assert(true)
                case _ => fail("No 1 Enumeration in ItemType found")
            }
        }

        scenario("convert INACTIVE from ItemState Enumeration to Value") {
            Given("A ItemState Enumeration is available")
            When("I request to convert INACTIVE to a value")
            val result = ItemState.getItemState("INactIVE")
            Then("The result should be 3")
            result match {
                case 3 => assert(true)
                case _ => fail("No INACTIVE Enumeration in ItemState found")
            }
        }

        scenario("convert 3 from ItemState Enumeration to INACTIVE") {
            Given("A ItemState Enumeration is available")
            When("I request to convert 1 to a INACTIVE")
            val result = ItemState.getItemState(3)
            Then("The result should be INACTIVE")
            result match {
                case "INACTIVE" => assert( true )
                case _ => fail("No 3 Enumeration in ItemState found")
            }
        }

        scenario("convert UNIT from MeasureUnit Enumeration to Value") {
            Given("A MeasureUnit Enumeration is available")
            When("I request to convert UNIT to a value")
            val result = MeasureUnit.getMeasureUnit("unit")
            Then("The result should be 999")
            result match {
                case 999 => assert(true)
                case _ => fail("No UNIT Enumeration in MeasureUnit found")
            }
        }

        scenario("convert 999 from MeasureUnit Enumeration to UNIT") {
            Given("A MeasureUnit Enumeration is available")
            When("I request to convert 1 to a UNIT")
            val result = MeasureUnit.getMeasureUnit(999)
            Then("The result should be UNIT")
            result match {
                case "UNIT" => assert(true)
                case _ => fail("No 3 Enumeration in MeasureUnit found")
            }
        }

    }
}
