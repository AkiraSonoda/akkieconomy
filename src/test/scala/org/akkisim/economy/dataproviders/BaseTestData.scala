package org.akkisim.economy.dataproviders

import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.items.ItemState._
import org.akkisim.economy.dataproviders.items._
import org.akkisim.economy.dataproviders.types.{MeasureUnit, UUID_ZERO}

/**
  * @author Akira Sonoda
  */
object BaseTestData {
    val inventoryItem1 = InventoryEntity (
        itemid = "102dba0-4711-11e7-9598-0800200d9c66",
        itemname = "Container",
        itemtype = 1,
        itemprice = 100,
        itemstate = NEW.id,
        itemownerid = "102dba0-4bc7-4711-9598-0800200c9a66"
    )

    val containerItemEntity1 = ContainerItemEntity(
        id = "102dba0-4711-11e7-9598-0800200d9c66",
        maxvolume = 20,
        currentvolume = 20,
        measureunit = MeasureUnit.DECILITER.id
    )

    val user1 = UserEntity(
        avataruuid = "102dba0-4711-11e7-9598-0800200d9e01",
        avatarname = "Zaki Test1",
        avatarpassword = "password",
        avatarhunger = Some(0),
        avatarthirst = Some(0),
        avatarlevel = Some(0)
    )

    val user2 = UserEntity(
        avataruuid = "88888888-4711-11e7-4711-0800200d9e01",
        avatarname = "Test User2",
        avatarpassword = "password",
        avatarhunger = Some(0),
        avatarthirst = Some(0),
        avatarlevel = Some(0)
    )

    val container1 = Container(
        id = "102dba0-4711-11e7-9598-0800200d9c66",
        ownerid = user1.avataruuid,
        name = "WaterTank",
        itemType = "WATERTANK",
        price = 100,
        maxVolume = 20,
        currentVolume = 20,
        measureUnit = "DECILITER")

    val vendor1 = Trader(
        id = "102dba0-4711-11e7-9598-0800200d9c66",
        ownerid = user1.avataruuid,
        name = "TestVendor",
        itemType = "VENDOR"
    )

    val marketInventory1 = MarketInventory (
        id = UUID_ZERO,
        ownerid = user2.avataruuid,
        itemtype = "WATERTANK"
    )

    // A Plant
    val inventoryItemPlant1 = InventoryEntity (
        itemid = "11111111-1111-11e7-9598-0800200d9c66",
        itemname = "AE Palm Tree",
        itemtype = 12,
        itemprice = 1000,
        itemstate = NEW.id,
        itemownerid = user1.avataruuid
    )

    val inventoryItemPlant2 = InventoryEntity (
        itemid = "11111111-2222-11e7-9598-0800200d9c66",
        itemname = "AE Palm Tree",
        itemtype = 13,
        itemprice = 1000,
        itemstate = NEW.id,
        itemownerid = user1.avataruuid
    )

    val inventoryItemPlant3 = InventoryEntity (
        itemid = "11111111-3333-11e7-9598-0800200d9c66",
        itemname = "AE Palm Tree",
        itemtype = 14,
        itemprice = 1000,
        itemstate = NEW.id,
        itemownerid = user1.avataruuid
    )

    val marketInventoryPlant1 = MarketInventory (
        id = inventoryItemPlant1.itemid,
        ownerid = user1.avataruuid,
        itemtype = "PLANT"
    )

    val plantEntity1 = PlantEntity (
        id = inventoryItemPlant1.itemid
        , plantstate = PlantState.PLANT_HARVEST.id
        , planttype =  12
        , plantstateack = PlantStateAck.PLANT_PRODUCING_FRUITS_ACK.id
        , growthpercent = 100
        , productioncycles = 5
        , pushurl = "http://localhost:4246/"
    )

    val plantEntity2 = PlantEntity (
        id = inventoryItemPlant1.itemid
        , plantstate = PlantState.PLANT_GROWING.id
        , planttype =  14
        , growthpercent = 20
        , pushurl = "http://localhost:4711"
    )


    val plant1 = Plant (
        id = inventoryItemPlant1.itemid,
        ownerid = inventoryItemPlant1.itemownerid,
        name = inventoryItemPlant1.itemname,
        itemType = "PLANT_TREE",
        price = 1000,
        plantType = "PLANT_TREE",
        plantState = "PLANT_PRODUCING_FRUITS",
        pushurl = "http://localhost"
    )

    val vendorPlant1 = Trader(
        id = "11111111-4444-11e7-9598-0800200d9c66",
        ownerid = user1.avataruuid,
        name = "PlantVendor",
        itemType = "VENDOR"
    )

    val buyer1 = Trader(
        id = "22222222-4444-11e7-9598-0800200d9c66",
        ownerid = user1.avataruuid,
        name = "TestBuyer",
        itemType = "MARKET_BUYER"
    )

    val store1 = Trader(
        id = "33333333-4444-11e7-9598-0800200d9c66",
        ownerid = user1.avataruuid,
        name = "TestStore",
        itemType = "STORE_10"
    )

    val storeItem1 = StoreItem(
        itemtype = "BASKET_COCONUT"
    )

    val itemSpec1 = ItemSpecEntity(
        kind = "BASKET_COCONUT"
        ,id = 5
        ,price = 50
        ,storageQuantity = 20
    )

    val basketCoconut1 = Container(
        id = "bb705eef-49b2-4aab-a9dc-cad0281fcf77"
        ,ownerid = user1.avataruuid
        ,name = "Korb voller Kokosnüsse"
        ,itemType = "BASKET_COCONUT"
        ,price = 50
        ,maxVolume = 20
        ,currentVolume = 20
        ,measureUnit = "KILOGRAM"
        ,origin = UUID_ZERO
    )

    val testMarketInventoryItems: Seq[MarketInventory] = Seq(
        MarketInventory(id = "102dba0-4711-11e7-9598-0800200d9d66", ownerid = "102dba0-4bc7-4711-9598-0800200c9a66", itemtype = "WATERTANK"),
        MarketInventory(id = "102dba0-4711-11e7-9598-0800200d9d67", ownerid = "102dba0-4bc7-4711-9598-0800200c9a66", itemtype = "WATERTANK"),
        MarketInventory(id = "102dba0-4711-11e7-9598-0800200d9d68", ownerid = "102dba0-4bc7-4711-9598-0800200c9a66", itemtype = "WATERTANK")
    )

    val testStoreItems: Seq[StoreItem] = Seq(
        StoreItem(itemtype = "WATERTANK"),
        StoreItem(itemtype = "VENDOR"),
        StoreItem(itemtype = "ESSEN"),
        StoreItem(itemtype = "PLANT_TREE"),
        StoreItem(itemtype = "BASKET_COCONUT"),
        StoreItem(itemtype = "MARKET_BUYER"),
        StoreItem(itemtype = "STORE_10"),
        StoreItem(itemtype = "STORE_20"),
        StoreItem(itemtype = "STORE_30")
    )

}
