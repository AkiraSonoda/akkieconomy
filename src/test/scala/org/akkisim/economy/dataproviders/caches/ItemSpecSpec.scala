/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.akkisim.economy.dataproviders.caches

import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.services.{DatabaseService, ItemSpecService}
import org.scalatest.{BeforeAndAfterAll, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * @author Akira Sonoda
  */
class ItemSpecSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll {
    val log: Logger = LoggerFactory.getLogger("ItemSpecSpec")

    import scala.concurrent.ExecutionContext.Implicits.global

    val itemSpecService = new ItemSpecService(new DatabaseService())

    override def beforeAll ( ): Unit = {
        val itemSpecFuture = itemSpecService.createItemSpec(BaseTestData.itemSpec1)
        Await.result(itemSpecFuture, 10.seconds)
    }

    override def afterAll ( ): Unit = {
        val itemSpecFuture = itemSpecService.deleteAllItemSpec()
        Await.result(itemSpecFuture, 10.seconds)
    }

    info("As economy User")
    info("I want to avoid unnecessary database calls in order to get better Performance")

    feature("ItemSpec Cache") {

        scenario("Initialize ItemSpec") {
            Given("A ItemSpec is initialized properly")
                val initResult = ItemSpec.initialize(itemSpecService)
                assert( initResult )
            When("""I request the Int of "BASKET_COCONUT" """)
                val intOfItem = ItemSpec.getIntOfKind("basket_coconut")
            Then("I should get the Int = 5")
                assert(intOfItem == 5)
            When("""I request the String of "5" """)
                 val stringOfItem = ItemSpec.getKindOfInt(5)
            Then("""I should get as String "BASKET_COCONUT" """)
                assert(stringOfItem.equals("BASKET_COCONUT"))
        }
    }
}
