/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.pattern.ask
import akka.testkit.TestKit
import akka.util.Timeout
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.akkisim.economy.dataproviders.types._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpecLike, Matchers}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

class UserActorSpec (_system: ActorSystem)
    extends TestKit(_system)
        with Matchers
        with FlatSpecLike
        with BeforeAndAfterEach
        with BeforeAndAfterAll {

    val log: Logger = LoggerFactory.getLogger("UserActorSpec")
    implicit val timeout: Timeout = Timeout(5 seconds)

    def this() = this(ActorSystem("UserActorSpec"))

    val Port = 4243
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    override def beforeEach {
        wireMockServer.start()
        WireMock.configureFor(Host, Port)
    }

    override def afterEach {
        wireMockServer.stop()
    }

    override def afterAll: Unit = {
        shutdown(system)
    }

    "A UserActor" should "process a Hunger Update Addition correctly" in  {
        stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
        val userActor: ActorRef = system.actorOf(Props(new UserActor(null, "", "", null)),"UserActor0")
        userActor ! RegisterPushUrl("", "http://localhost:4243/")
        val userFuture: Future[Any] = userActor ? StateUpdate(StateName.HUNGER, 1, Modification.ADDITION)
        // Convert Future[Any] to Future[AvatarStatus]
        val responseFuture: Future[AvatarStatus] = userFuture.mapTo[AvatarStatus]
        val result = Await.result(responseFuture, 5000 millis)
        result.avatarhunger should be(1)
        result.avatarthirst should be(0)
        userActor ! PoisonPill
    }

    "A UserActor" should "process a Hunger Update Subtraction correctly" in  {
        stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
        val userActor: ActorRef = system.actorOf(Props(new UserActor(null, "", "", null)),"UserActor1")
        userActor ! RegisterPushUrl("", "http://localhost:4243/")
        val userFuture0: Future[Any] = userActor ? StateUpdate(StateName.HUNGER, 10, Modification.ADDITION)
        // Convert Future[Any] to Future[AvatarStatus]
        val responseFuture0: Future[AvatarStatus] = userFuture0.mapTo[AvatarStatus]
        val result0 = Await.result(responseFuture0, 5000 millis)
        result0.avatarhunger should be(10)
        result0.avatarthirst should be(0)
        val userFuture: Future[Any] = userActor ? StateUpdate(StateName.HUNGER, 1, Modification.SUBTRACTION)
        // Convert Future[Any] to Future[AvatarStatus]
        val responseFuture: Future[AvatarStatus] = userFuture.mapTo[AvatarStatus]
        val result = Await.result(responseFuture, 5000 millis)
        result.avatarhunger should be(9)
        result.avatarthirst should be(0)
        userActor ! PoisonPill
    }

    "A UserActor" should "process a Thirst Update Addition correctly" in  {
        stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
        val userActor: ActorRef = system.actorOf(Props(new UserActor(null, "", "", null)),"UserActor2")
        userActor ! RegisterPushUrl("", "http://localhost:4243/")
        val userFuture: Future[Any] = userActor ? StateUpdate(StateName.THIRST, 1, Modification.ADDITION)
        // Convert Future[Any] to Future[AvatarStatus]
        val responseFuture: Future[AvatarStatus] = userFuture.mapTo[AvatarStatus]
        val result = Await.result(responseFuture, 5000 millis)
        result.avatarhunger should be(0)
        result.avatarthirst should be(1)
        userActor ! PoisonPill
    }

    "A UserActor" should "process a Thirst Update Subtraction correctly" in  {
        stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
        val userActor: ActorRef = system.actorOf(Props(new UserActor(null, "", "", null)),"UserActor3")
        userActor ! RegisterPushUrl("", "http://localhost:4243/")
        val userFuture0: Future[Any] = userActor ? StateUpdate(StateName.THIRST, 10, Modification.ADDITION)
        // Convert Future[Any] to Future[AvatarStatus]
        val responseFuture0: Future[AvatarStatus] = userFuture0.mapTo[AvatarStatus]
        val result0 = Await.result(responseFuture0, 5000 millis)
        result0.avatarhunger should be(0)
        result0.avatarthirst should be(10)

        val userFuture: Future[Any] = userActor ? StateUpdate(StateName.THIRST, 1, Modification.SUBTRACTION)
        // Convert Future[Any] to Future[AvatarStatus]
        val responseFuture: Future[AvatarStatus] = userFuture.mapTo[AvatarStatus]
        val result = Await.result(responseFuture, 5000 millis)
        result.avatarhunger should be(0)
        result.avatarthirst should be(9)
        userActor ! PoisonPill
    }
}
