/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.TestKit
import akka.util.Timeout
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{ItemSpecEntity, PlantEntity}
import org.akkisim.economy.dataproviders.items.PlantState
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.services.{DatabaseService, InventoryService, PlantService, UserService}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpecLike, Matchers}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.language.postfixOps


class PlantFactoryActorSpec ( _system: ActorSystem)
    extends TestKit(_system)
        with Matchers
        with FlatSpecLike
        with BeforeAndAfterEach
        with BeforeAndAfterAll {

    val log: Logger = LoggerFactory.getLogger("PlantFactoryActorSpec")
    implicit val timeout: Timeout = Timeout(5 seconds)
    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global

    def this() = this(ActorSystem("PlantFactoryActorSpec"))

    val Port = 4244
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    val databaseService = new DatabaseService()
    val inventoryService = new InventoryService(databaseService)
    val plantService = new PlantService(databaseService)

    var userActorRef: ActorRef = _
    var centralActor: ActorRef = _

    override def beforeAll: Unit = {
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_BUSH",13,300,10,1800,1080,10))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_TREE",14,500,20,3600,1800,20))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_VEGETABLE",12,100,5,700,700,5))

        centralActor = system.actorOf(Props[CentralActor], "CentralActor")
        val plantFactoryActor: ActorRef = system.actorOf(Props(new PlantFactoryActor(inventoryService, plantService ,system)), "PlantFactoryActor")
        Central.optCentral = Some(centralActor)
        Central.optPlantFactory = Some(plantFactoryActor)

        val userService = new UserService(databaseService)(centralActor)
        Central.optUserService = Some(userService)

        // Clenup the Inventory
        val result = inventoryService.deleteAllInventoryItems()
        Await.result(result, 10.seconds)

        // We need a User
        val userFuture = userService.createUser(BaseTestData.user1)
        Await.result(userFuture, 10.seconds)


        // We need some plants in the Inventory
        for {
            p1 <- inventoryService.createInventoryItem(BaseTestData.inventoryItemPlant1)
            p2 <- inventoryService.createInventoryItem(BaseTestData.inventoryItemPlant2)
            p3 <- inventoryService.createInventoryItem(BaseTestData.inventoryItemPlant3)
            p4 <- plantService.createPlant(PlantEntity(
                id = BaseTestData.inventoryItemPlant1.itemid
                , plantstate = PlantState.PLANT_PRODUCING_FRUITS.id
                , planttype =  ItemSpec.getIntOfKind("PLANT_TREE")
                , pushurl = "http://localhost:4244/"))
            p5 <- plantService.createPlant(PlantEntity(
                id = BaseTestData.inventoryItemPlant2.itemid
                , plantstate = PlantState.PLANT_PRODUCING_FRUITS.id
                , planttype =  ItemSpec.getIntOfKind("PLANT_TREE")
                , pushurl = "http://localhost:4244/"))
            p6 <- plantService.createPlant(PlantEntity(
                id = BaseTestData.inventoryItemPlant3.itemid
                , plantstate = PlantState.PLANT_PRODUCING_FRUITS.id
                , planttype =  ItemSpec.getIntOfKind("PLANT_TREE")
                , pushurl = "http://localhost:4244/"))
        } yield (p1, p2, p3, p4, p5, p6)

    }

    override def beforeEach: Unit = {
        wireMockServer.start()
        WireMock.configureFor(Host, Port)
    }

    override def afterEach {
        wireMockServer.stop()
    }

    override def afterAll: Unit = {
        Central optUserService match {
            case Some(userService) =>
                val userServiceResult = userService.deleteUser(BaseTestData.user1.avataruuid)
                Await.result(userServiceResult, 10.seconds)
            case None =>
                log.error("Failed to retrieve User Service")
        }
        val invServiceResult = inventoryService.deleteAllInventoryItems()
        Await.result(invServiceResult, 10.seconds)
        val plantServiceResult = plantService.deleteAllPlants()
        Await.result(plantServiceResult, 10.seconds)
        shutdown(system)
    }

    "CentralActor" should "register 3 Plants for a given User and create the necessary PlantActors" in  {
        stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
        stubFor(post(urlEqualTo("/"))
            .withRequestBody(containing("hunger"))
            .willReturn(aResponse().withStatus(200)))
        stubFor(put(urlEqualTo("/"))
            .withRequestBody(containing("HEARTBEAT"))
            .willReturn(aResponse().withStatus(200)))

        Central.optUserService match {
            case Some(userService) =>
                userActorRef = system.actorOf(Props(new UserActor(userService, "102dba0-4711-11e7-9598-0800200d9e01", "http://localhost:4244/", centralActor)),"UserActor0")
                userActorRef ! RegisterPushUrl("", "http://localhost:4244/")
                Central.optCentral match {
                    case Some(central) =>
                        central ! RegisterUser(BaseTestData.user1.avataruuid, userActorRef)
                        Thread.sleep(5000) // We wait some time to let the Actors complete
                        assert(Central.plantActors.size == 3)
                        // Send them some Heartbeats
                        central ! SendHeartbeat
                        Thread.sleep(2000)
                        central ! SendHeartbeat
                        Thread.sleep(2000)
                    case None => fail("No CentralActor available")
                }
            case None =>
                fail("No UserService available")
        }

    }

    "CentralActor" should "deregister 3 Plants for a given User and destroy the necessary PlantActors and destroy the UserActor" in  {
        stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
        stubFor(put(urlEqualTo("/"))
            .withRequestBody(containing("HEARTBEAT"))
            .willReturn(aResponse().withStatus(200)))

        Central.optCentral match {
            case Some(central) =>
                central ! DeRegisterUser(BaseTestData.user1.avataruuid)
                Thread.sleep(5000) // We wait some time to let the Actors complete
                assert(Central.plantActors.isEmpty)
                assert(Central.userActors.isEmpty)
            case None => fail("No CentralActor available")
        }
    }

}
