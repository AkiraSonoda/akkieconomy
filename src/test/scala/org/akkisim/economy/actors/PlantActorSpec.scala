/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.testkit.TestKit
import akka.util.Timeout
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{InventoryEntity, ItemSpecEntity, MarketInventory, PlantEntity}
import org.akkisim.economy.dataproviders.items.{PlantState, PlantStateAck}
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.services.{DatabaseService, InventoryService, MarketInventoryService, PlantService}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpecLike, Matchers}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}


class PlantActorSpec ( _system: ActorSystem)
    extends TestKit(_system)
        with Matchers
        with FlatSpecLike
        with BeforeAndAfterEach
        with BeforeAndAfterAll {

    val log: Logger = LoggerFactory.getLogger("PlantActorSpec")
    implicit val timeout: Timeout = Timeout(5 seconds)
    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global

    def this() = this(ActorSystem("PlantActorSpec"))

    val Port = 4245
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    val databaseService = new DatabaseService()
    val plantService = new PlantService(databaseService)
    val inventoryService = new InventoryService(databaseService)
    val marketInventoryService = new MarketInventoryService(databaseService)

    Central.optPlantService = Some(plantService)
    Central.optMarketInventoryService = Some(marketInventoryService)
    Central.optInventoryService = Some(inventoryService)

    var plantActorRef: ActorRef = _

    override def beforeAll: Unit = {
        // val centralActor: ActorRef = system.actorOf(Props[CentralActor], "CentralActor")
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_BUSH",13,300,10,1800,1080,10) )
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_TREE",14,500,20,3600,1800,20) )
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_VEGETABLE",12,100,5,700,700,5) )

        // Clenup the Inventory
        val result = plantService.deleteAllPlants()
        Await.result(result, 10.seconds)

        // We need some plants
        val plantFuture = plantService.createPlant(PlantEntity(
                id = BaseTestData.inventoryItemPlant1.itemid
                , plantstate = PlantState.PLANT_PRODUCING_FRUITS.id
                , planttype =  ItemSpec.getIntOfKind("PLANT_TREE")
                , heartbeatcycles = 1799
                , pushurl = "http://localhost:4245/"))
        Await.result(plantFuture, 10 seconds)

        val inventoryFuture = inventoryService.createInventoryItem(
            InventoryEntity(
                itemid = BaseTestData.inventoryItemPlant1.itemid
                , itemownerid = BaseTestData.inventoryItemPlant1.itemownerid
                , itemname =  BaseTestData.inventoryItemPlant1.itemname
                , itemtype = BaseTestData.inventoryItemPlant1.itemtype
                , itemprice = BaseTestData.inventoryItemPlant1.itemprice
                , itemstate = BaseTestData.inventoryItemPlant1.itemstate
            )
        )
        Await.result(inventoryFuture, 10 seconds)

        val marketInventoryServiceFuture = marketInventoryService.createMarketInventoryEntity(
            MarketInventory (
                id = BaseTestData.inventoryItemPlant1.itemid
                , ownerid = BaseTestData.inventoryItemPlant1.itemownerid
                , itemtype = ItemSpec.getKindOfInt(BaseTestData.inventoryItemPlant1.itemtype)
            )
        )
        Await.result(marketInventoryServiceFuture, 10 seconds)
    }

    override def beforeEach: Unit = {
        wireMockServer.start()
        WireMock.configureFor(Host, Port)
    }

    override def afterEach {
        wireMockServer.stop()
    }

    override def afterAll: Unit = {
        val plantServiceResult = plantService.deleteAllPlants()
        Await.result(plantServiceResult, 10.seconds)
        val inventoryServiceResult = inventoryService.deleteAllInventoryItems()
        Await.result(inventoryServiceResult, 10.seconds)
        val marketInventoryServiceResult = marketInventoryService.deleteAllMarketInventory()
        Await.result(marketInventoryServiceResult, 10.seconds)
        shutdown(system)
    }

    "PlantActor" should "change the state from PLANT_PRODUCING_FRUITS to PLANT_HARVEST" in  {
        stubFor(put(urlEqualTo("/"))
            .withRequestBody(containing("PLANT_HARVEST"))
            .willReturn(aResponse()
                    .withHeader("Content-Type", "text/plain")
                    .withBody("PLANT_HARVEST_OK")
                    .withStatus(200))
        )
        // Get the Plant
        val plantEntityFuture = plantService.getPlantById(BaseTestData.inventoryItemPlant1.itemid)
        val result: Try[Option[PlantEntity]] = Await.ready(plantEntityFuture, 10 seconds).value.get
        result match {
            case Success ( optPlantEntity ) =>
                optPlantEntity match {
                    case Some(plant) =>
                        plantActorRef = system.actorOf(Props(new PlantActor(plant)),"PlantActor0")
                        plantActorRef ! SendHeartbeat
                        Thread.sleep(2000) // We wait some time to let the Actors complete
                        // Read the Plant Again

                        val plantEntityFuture2 = plantService.getPlantById(BaseTestData.inventoryItemPlant1.itemid)
                        val result2: Try[Option[PlantEntity]] = Await.ready(plantEntityFuture2, 10 seconds).value.get
                        result2 match {
                            case Success( optPlantEntity2 ) =>
                                optPlantEntity2 match {
                                    case Some(plant2) =>
                                        assert(plant2.productioncycles == 1)
                                        assert(plant2.plantstate == PlantState.PLANT_HARVEST.id)
                                        assert(plant2.plantstateack == PlantStateAck.PLANT_PRODUCING_FRUITS_ACK.id)
                                    case None =>
                                        fail("PlantService did not return a plant")
                                }
                            case Failure( _ ) =>
                                fail("Could not get PlantEntity")
                        }
                        plantActorRef ! PoisonPill
                        Thread.sleep(2000)
                    case None => fail("PlantService did not return a plant")
                }
            case Failure ( _ ) =>
                fail("Could not get PlantEntity")
        }
    }

    "PlantActor" should "change the state from PLANT_HARVEST to PLANT_PRODUCING_FRUITS" in  {
        stubFor(put(urlEqualTo("/"))
            .withRequestBody(containing("PLANT_HARVEST"))
            .willReturn(aResponse()
                .withHeader("Content-Type", "text/plain")
                .withBody("PLANT_HARVEST_OK")
                .withStatus(200))
        )
        // Get the Plant
        val plantEntityFuture = plantService.getPlantById(BaseTestData.inventoryItemPlant1.itemid)
        val result: Try[Option[PlantEntity]] = Await.ready(plantEntityFuture, 10 seconds).value.get
        result match {
            case Success ( optPlantEntity ) =>
                optPlantEntity match {
                    case Some(plant) =>
                        plantActorRef = system.actorOf(Props(new PlantActor(plant)),"PlantActor0")
                        plantActorRef ! StateChange
                        Thread.sleep(2000) // We wait some time to let the Actors complete
                        // Read the Plant Again

                        val plantEntityFuture2 = plantService.getPlantById(BaseTestData.inventoryItemPlant1.itemid)
                        val result2: Try[Option[PlantEntity]] = Await.ready(plantEntityFuture2, 10 seconds).value.get
                        result2 match {
                            case Success( optPlantEntity2 ) =>
                                optPlantEntity2 match {
                                    case Some(plant2) =>
                                        assert(plant2.productioncycles == 1)
                                        assert(plant2.plantstate == PlantState.PLANT_PRODUCING_FRUITS.id)
                                        assert(plant2.plantstateack == PlantStateAck.UNDEFINED.id)
                                    case None =>
                                        fail("PlantService did not return a plant")
                                }
                            case Failure( _ ) =>
                                fail("Could not get PlantEntity")
                        }
                        plantActorRef ! PoisonPill
                        Thread.sleep(2000)
                    case None => fail("PlantService did not return a plant")
                }
            case Failure ( _ ) =>
                fail("Could not get PlantEntity")
        }
    }
    "PlantActor" should "change the state from PLANT_HARVEST to PLANT_DEAD" in  {
        stubFor(put(urlEqualTo("/"))
            .withRequestBody(containing("PLANT_DEAD"))
            .willReturn(aResponse()
                .withHeader("Content-Type", "text/plain")
                .withBody("PLANT_DEAD_OK")
                .withStatus(200))
        )
        // Get the Plant
        val plant = PlantEntity(
            id = BaseTestData.inventoryItemPlant1.itemid
            , plantstate = PlantState.PLANT_HARVEST.id
            , plantstateack = PlantStateAck.PLANT_PRODUCING_FRUITS_ACK.id
            , planttype =  ItemSpec.getIntOfKind("PLANT_VEGETABLE")
            , productioncycles = 5
            , pushurl = "http://localhost:4245/")

        plantActorRef = system.actorOf(Props(new PlantActor(plant)),"PlantActor0")
        Central.plantActors += plant.id -> plantActorRef

        plantActorRef ! StateChange
        Thread.sleep(2000) // We wait some time to let the Actors complete
        // Read the Plant Again

        // Now the plants should be deleted.

        val plantEntityFuture2 = plantService.getPlantById(BaseTestData.inventoryItemPlant1.itemid)
        val result2: Try[Option[PlantEntity]] = Await.ready(plantEntityFuture2, 10 seconds).value.get
        result2 match {
            case Success( optPlantEntity2 ) =>
                optPlantEntity2 match {
                    case Some( _ ) =>
                        fail("We should not have any Plants now")
                    case None =>
                        assert(true)
                }
                case Failure( _ ) =>
                    fail("Could not get PlantEntity")
        }

        Central.plantActors.get(plant.id) match {
            case Some( _ ) =>
                fail("We should not have any Plants now")
            case None => assert(true)
        }
    }
}
