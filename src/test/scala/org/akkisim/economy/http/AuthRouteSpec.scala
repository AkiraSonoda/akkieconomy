/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.db.TokenEntity
import org.akkisim.economy.dataproviders.types.LoginPassword
import org.scalatest.BeforeAndAfter
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class AuthRouteSpec extends BaseServiceTest with BeforeAndAfter with ScalaFutures {
    import io.circe.generic.auto._
    import io.circe.syntax._

    trait Context {
        val authRoute: Route = httpService.authServiceRouter.route
    }

    before {
        val userFuture: Future[Int] = userService.createUser(BaseTestData.user1)
        Await.result(userFuture, 10.seconds)
    }

    after {
        val userFuture: Future[Int] = userService.deleteUser(BaseTestData.user1.avataruuid)
        Await.result(userFuture, 10.seconds)

        val tokenFuture: Future[Int] = authService.deleteToken(BaseTestData.user1.avataruuid)
        Await.result(tokenFuture, 10.seconds)
    }

    "Auth Service" should {
        "retrieve a Token after signIn" in new Context {
            val login = LoginPassword (
                avatarUUID = BaseTestData.user1.avataruuid,
                avatarName = BaseTestData.user1.avatarname,
                password = BaseTestData.user1.avatarpassword,
                newPassword = ""
            )
            val requestEntity = HttpEntity(MediaTypes.`application/json`, login.asJson.toString())
            Post("/auth/signIn",requestEntity).withHeaders(RawHeader("pushurl","http://dereos.org")) ~> authRoute ~> check {
                responseAs[Option[TokenEntity]] should be(defined)
            }
        }
    }
}
