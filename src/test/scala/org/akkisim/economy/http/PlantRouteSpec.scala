/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import java.util.UUID

import akka.http.javadsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.items.Plant
import org.akkisim.economy.dataproviders.types.UUID_ZERO
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, BeforeAndAfterEach}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


class PlantRouteSpec extends BaseServiceTest with BeforeAndAfter with BeforeAndAfterEach with BeforeAndAfterAll with ScalaFutures {
    import containerItemService._
    import io.circe.generic.auto._
    import io.circe.syntax._
    
    val log: Logger = LoggerFactory.getLogger("ContainerRouteSpec")
    val user1: UserEntity = BaseTestData.user1
    val plant1: Plant = BaseTestData.plant1
    val marketInventory1: MarketInventory = BaseTestData.marketInventory1

    trait Context {
        val plantRoute: Route = httpService.plantRouter.route
    }

    override def beforeAll {
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_BUSH",13,300,10,1800,1080,10))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_TREE",14,500,20,3600,1800,20))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_VEGETABLE",12,100,5,700,700,5))

        // We also need an empty MarketInventory Item
        val marketInventoryFuture = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = user1.avataruuid,
                itemtype = "PLANT_TREE"
            )
        )
        Await.result(marketInventoryFuture, 10.seconds)

    }

    override def afterAll: Unit = {
        val testContainerItemsFuture: Future[Int] = plantService.deleteAllPlants()
        Await.result(testContainerItemsFuture, 10.seconds)
        val testInventoryItemsFuture = inventoryService.deleteAllInventoryItems()
        Await.result(testInventoryItemsFuture, 10.seconds)
        val testMarketInventoryFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(testMarketInventoryFuture, 10.seconds)
    }


    "Plant Service" should {
        "create a new Plant" in new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, plant1.asJson.toString())
            Post("/item/plant", requestEntity) ~> plantRoute ~> check {
                response.status should be(StatusCodes.OK)
            }
        }
        "not create a new Plant without open Slot in MarketInfentory" in new Context {
            val plantCreate = Plant(
                id = UUID.randomUUID().toString,
                ownerid = user1.avataruuid,
                name = "Sonnenblume",
                itemType = "PLANT",
                price = 100,
                plantState = "PLANT_PRODUCING_FRUITS",
                plantType = "PLANT_TREE",
                pushurl = "http://localhost"
            )

            val requestEntity = HttpEntity(MediaTypes.`application/json`, plantCreate.asJson.toString())
            Post("/item/plant", requestEntity) ~> plantRoute ~> check {
                response.status should be(StatusCodes.NOT_FOUND)
            }
            Thread.sleep(2000)

            // Ensure there is no InventoryItem
            val inventoryFuture: Future[Option[InventoryEntity]] = inventoryService.getInventoryItemById(plantCreate.id)
            val invResult: Option[InventoryEntity] = Await.result(inventoryFuture, 10.seconds)
            invResult match {
                case Some(_) => assert(false)
                case None => assert(true)
            }

            // Ensure there is no PlantItem
            val plantFuture: Future[Option[PlantEntity]] = plantService.getPlantById(plantCreate.id)
            val plantResult: Option[PlantEntity] = Await.result(plantFuture, 10.seconds)
            plantResult match {
                case Some(_) => assert(false)
                case None => assert(true)
            }

        }
        "retrieve Plant by id" in new Context {
            Get("/item/plant/"+plant1.id) ~> plantRoute ~> check {
                responseAs[Plant] should be(plant1)
            }
        }
        "insert a MarketInventory via the Plant" in  new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, marketInventory1.asJson.toString())

            Put("/item/plant/"+plant1.id, requestEntity) ~> plantRoute ~> check {
                responseAs[Int] should be(1)
            }
        }
        "delete Plant" in new Context {
            Delete("/item/plant/"+plant1.id) ~> plantRoute ~> check {
                response.status should be(OK)
                whenReady(getContainerItemById(plant1.id)) {
                    result => result should be(None: Option[Plant])
                }
            }
        }
    }

}
