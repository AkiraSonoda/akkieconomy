/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import akka.actor.Props
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import org.akkisim.economy.actors.PlantActor
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{ItemSpecEntity, MarketInventory, MarketInventoryEntity, UserEntity}
import org.akkisim.economy.dataproviders.items.Trader
import org.akkisim.economy.dataproviders.types.UUID_ZERO
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, BeforeAndAfterEach}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration._


/**
  * In the context of this Test,
  *
  * User1 is the Owner of the Trader
  * User2 is the Buyier of something
  */
class VendorRouteSpec extends BaseServiceTest with BeforeAndAfter with BeforeAndAfterEach with BeforeAndAfterAll with ScalaFutures {
    import inventoryService._
    import io.circe.generic.auto._
    import io.circe.syntax._
    
    val log: Logger = LoggerFactory.getLogger("ContainerRouteSpec")
    val user1: UserEntity = BaseTestData.user1
    val vendor1: Trader = BaseTestData.vendor1
    val vendorPlant1: Trader = BaseTestData.vendorPlant1
    val marketInventoryPlant1: MarketInventory = BaseTestData.marketInventoryPlant1

    trait Context {
        val vendorRoute: Route = httpService.vendorRouter.route
    }

    val Port = 4246
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    override def beforeAll {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20))
        ItemSpec.addToCache(ItemSpecEntity ("VENDOR",2,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("ESSEN",3,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT",4,0,0))

        // We need a Plant including Inventory and Plant
        val plantFuture = plantService.createPlant(
            BaseTestData.plantEntity1
        )
        Await.result(plantFuture, 10.seconds)
        val inventoryFuture = inventoryService.createInventoryItem(
            BaseTestData.inventoryItemPlant1
        )

        // We need a PlantActor
        val plantActor = actorSystem.actorOf(Props(new PlantActor(BaseTestData.plantEntity1)), BaseTestData.plantEntity1.id)
        Central.plantActors += BaseTestData.plantEntity1.id -> plantActor

        // We need an empty MarketInventory Item for the Plant Test
        val marketInventoryFuture = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = user1.avataruuid,
                itemtype = "VENDOR"
            )
        )
        Await.result(marketInventoryFuture, 10.seconds)

        // We also need an empty MarketInventory Item for the regular Test
        val marketInventoryFuture2 = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = user1.avataruuid,
                itemtype = "VENDOR"
            )
        )
        Await.result(marketInventoryFuture2, 10.seconds)

    }

    override def beforeEach: Unit = {
        wireMockServer.start()
        WireMock.configureFor(Host, Port)
    }

    override def afterEach {
        wireMockServer.stop()
    }

    override def afterAll: Unit = {
        val testInventoryItemsFuture = inventoryService.deleteAllInventoryItems()
        Await.result(testInventoryItemsFuture, 10.seconds)
        val testMarketInventoryFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(testMarketInventoryFuture, 10.seconds)
        val testPlantFuture = plantService.deleteAllPlants( )
        Await.result(testPlantFuture, 10.seconds)
    }


    "VendorRoute Service" should {
        "create a new Vendor" in new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, vendor1.asJson.toString())
            Post("/item/vendor", requestEntity) ~> vendorRoute ~> check {
                responseAs[String] should be(vendor1.ownerid)
            }
        }
        "create a new Vendor for the Plant Test" in new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, vendorPlant1.asJson.toString())
            Post("/item/vendor", requestEntity) ~> vendorRoute ~> check {
                responseAs[String] should be(vendorPlant1.ownerid)
            }
        }

        "retrieve Vendor by id" in new Context {
            Get("/item/vendor/"+vendor1.id) ~> vendorRoute ~> check {
                responseAs[Trader] should be(vendor1)
            }
        }

        "insert a MarketInventory for a Plant via the Vendor" in  new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, marketInventoryPlant1.asJson.toString())
            // val requestEntity = HttpEntity(MediaTypes.`application/json`, "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"ownerid\":\"88888888-4711-11e7-4711-0800200d9e01\",\"itemType\":\"WATERTANK\"}")

            Put("/item/vendor/"+vendor1.id+"/"+vendor1.ownerid+"/"+marketInventoryPlant1.origin, requestEntity) ~> vendorRoute ~> check {
                responseAs[Int] should be(1)
            }
        }

        "check the existence of a MarketInventory Item" in  new Context {
            Get("/item/vendor/"+marketInventoryPlant1.id+"/"+marketInventoryPlant1.ownerid+"/"+marketInventoryPlant1.itemtype+"/"+marketInventoryPlant1.origin) ~> Route.seal(vendorRoute) ~> check {
                response.status should be(StatusCodes.OK)
            }
        }

        "delete a FruitVendor and inform the Plant Actor" in new Context {

            stubFor(put(urlEqualTo("/"))
                .withRequestBody(containing("PLANT_DEAD"))
                .willReturn(aResponse()
                    .withHeader("Content-Type", "text/plain")
                    .withBody("PLANT_DEAD_OK")
                    .withStatus(200))
            )

            Delete("/item/vendor/"+vendorPlant1.id+"/PLANT/"+BaseTestData.plantEntity1.id) ~> vendorRoute ~> check {
                response.status should be(OK)
                Thread.sleep(2000)
                whenReady(inventoryService.getInventoryItemById(vendorPlant1.id)) {
                    result => result should be(None: Option[Inventory])
                }
                whenReady(inventoryService.getInventoryItemById(marketInventoryPlant1.id)) {
                    result => result should be (None: Option[Inventory])
                }
                whenReady(marketInventoryService.getUniqueMarketInventoryItem (
                    BaseTestData.inventoryItemPlant1.itemid
                    , BaseTestData.inventoryItemPlant1.itemownerid
                    , BaseTestData.inventoryItemPlant1.itemtype
                    , UUID_ZERO
                )) {
                    result => result should be ( None: Option[MarketInventoryEntity])
                }
            }
        }

        "delete Vendor" in new Context {
            Delete("/item/vendor/"+vendor1.id) ~> vendorRoute ~> check {
                response.status should be(OK)
                whenReady(inventoryService.getInventoryItemById(vendor1.id)) {
                    result => result should be(None: Option[Inventory])
                }
            }
        }
    }

}
