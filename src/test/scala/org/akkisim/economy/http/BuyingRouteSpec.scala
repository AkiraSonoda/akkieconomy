/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{ItemSpecEntity, MarketInventory}
import org.akkisim.economy.dataproviders.items.{Price, Trader}
import org.akkisim.economy.dataproviders.types.UUID_ZERO
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, BeforeAndAfterEach}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration._


/**
  * In the context of this Test,
  *
  * User1 is the Owner of the Trader
  * User2 is the Buyier of something
  */
class BuyingRouteSpec extends BaseServiceTest with BeforeAndAfter with BeforeAndAfterEach with BeforeAndAfterAll with ScalaFutures {
    import inventoryService._
    import io.circe.generic.auto._
    import io.circe.syntax._
    
    val log: Logger = LoggerFactory.getLogger("BuyingRouteSpec")

    trait Context {
        val buyingRoute: Route = httpService.buyingRouter.route
    }


    override def beforeAll {
        ItemSpec.addToCache(ItemSpecEntity ("BASKET_COCONUT",5,50,20))
        ItemSpec.addToCache(ItemSpecEntity ("MARKET_BUYER",6,0,0))

        // We need an empty MarketInventory Item for the Plant Test
        val marketInventoryFuture = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = BaseTestData.user1.avataruuid,
                itemtype = "MARKET_BUYER"
            )
        )
        Await.result(marketInventoryFuture, 10.seconds)
        // We need a MarketInventory for the Basket of Coconuts
        val marketInventoryFutureBasket = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = BaseTestData.user1.avataruuid,
                itemtype = "BASKET_COCONUT"
            )
        )
        Await.result(marketInventoryFuture, 10.seconds)
        // Insert some Prices
        val priceServiceFuture = itemSpecService.createItemSpec(
            ItemSpecEntity(
                kind = "BASKET_COCONUT"
                , id = 5
                , price = 50
                , storageQuantity = 20
            )
        )
        Await.result(priceServiceFuture, 10.seconds)
    }

    override def afterAll: Unit = {
        val testInventoryItemsFuture = inventoryService.deleteAllInventoryItems()
        Await.result(testInventoryItemsFuture, 10.seconds)
        val testMarketInventoryFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(testMarketInventoryFuture, 10.seconds)
        val testPlantFuture = plantService.deleteAllPlants( )
        Await.result(testPlantFuture, 10.seconds)
        val testPriceFuture = itemSpecService.deleteAllItemSpec( )
        Await.result(testPriceFuture, 10.seconds)

    }


    "BuyerServiceRoute" should {
        "create a new Buyer" in new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, BaseTestData.buyer1.asJson.toString())
            Post("/item/buyer", requestEntity) ~> buyingRoute ~> check {
                responseAs[String] should be(BaseTestData.buyer1.ownerid)
            }
        }

        "retrieve Buyer by id" in new Context {
            Get("/item/buyer/"+BaseTestData.buyer1.id) ~> buyingRoute ~> check {
                responseAs[Trader] should be(BaseTestData.buyer1)
            }
        }

        "get the price for BASKET_COCONUT" in new Context {
            Get("/item/buyer/price/BASKET_COCONUT") ~> buyingRoute ~> check {
                responseAs[Price] should be(Price("BASKET_COCONUT",50))
            }
        }

        "get the price of non existing BASKET_APPLE" in new Context {
            Get("/item/buyer/price/BASKET_APPLE") ~> Route.seal(buyingRoute) ~> check {
                response.status should be( StatusCodes.NotFound)
            }
        }

        "check the existence of the MarketInventory for an Item to be sold" in new Context {
            val trader = Trader(
                id = UUID.randomUUID().toString
                , ownerid = BaseTestData.user1.avataruuid
                , name = "Basket of Coconuts"
                , itemType = "BASKET_COCONUT"
                , price = 50
                , origin = UUID_ZERO
            )
            val jsonString = trader.asJson.toString()
            val requestEntity = HttpEntity(MediaTypes.`application/json`, jsonString)
            Put("/item/buyer/"+BaseTestData.buyer1.id+"/"+BaseTestData.buyer1.ownerid+"/"+BaseTestData.buyer1.origin, requestEntity) ~> Route.seal(buyingRoute) ~> check {
                response.status should be( StatusCodes.OK)
            }

        }

        "delete Buyer" in new Context {
            Delete("/item/buyer/"+BaseTestData.buyer1.id) ~> buyingRoute ~> check {
                response.status should be(OK)
                whenReady(inventoryService.getInventoryItemById(BaseTestData.buyer1.id)) {
                    result => result should be(None: Option[Inventory])
                }
            }
        }
    }

}
