/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import org.akkisim.economy.dataproviders.caches.Central
import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.items.SignInResult
import org.akkisim.economy.dataproviders.types.{DeRegisterUser, PushUrl}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

class UserRouteSpec extends BaseServiceTest with BeforeAndAfterAll with ScalaFutures {
    val log: Logger = LoggerFactory.getLogger("UserRouteSpec")

    import io.circe.generic.auto._
    import io.circe.syntax._
    val user1 = UserEntity(
        avataruuid = "cbb09e2e-5b1b-4961-9d18-0fc4f3a1f0fc",
        avatarname = "Zaki Test1",
        avatarpassword = "password",
        avatarhunger = Some(0),
        avatarthirst = Some(0),
        avatarlevel = Some(0)
    )


    trait Context {
        val userRoute: Route = httpService.userServiceRouter.route
    }

    var token: String = ""

    override def beforeAll: Unit = {
        val userFuture: Future[Option[SignInResult]] = authService.signIn(
            user1.avataruuid,
            user1.avatarname,
            user1.avatarpassword,
            "",
            "http://dereos.org"
        )
        val result: Try[Option[SignInResult]] = Await.ready(userFuture, 10.seconds).value.get
        result match {
            case Success(tokenEntity) => tokenEntity match {
                case Some(tk) => this.token = tk.token
                case None => this.token = ""
            }
            case Failure(ex) =>
                ex.printStackTrace()
                this.token = ""
        }
    }

    override def afterAll: Unit = {
        Central.optCentral match {
            case Some(centralRef) =>
                centralRef ! DeRegisterUser(user1.avataruuid)
                Thread.sleep(1000)
            case None =>
                log.error("No Central available")
        }

        val userFuture1: Future[Int] = userService.deleteUser(user1.avataruuid)
        Await.result(userFuture1, 10.seconds)

        val tokenFuture: Future[Int] = authService.deleteToken(user1.avataruuid)
        Await.result(tokenFuture, 10.seconds)

    }

    "UserService Service" should {
        "retrieve my UserData" in new Context {
            Get("/user/me").withHeaders(RawHeader("token",token)) ~> userRoute ~> check {
                responseAs[UserEntity].avatarname should be(user1.avatarname)
            }
        }
        "push a new Url to the UserActor" in new Context {
            val pushUrl = PushUrl("http://someotherDomain")
            val requestEntity = HttpEntity(MediaTypes.`application/json`, pushUrl.asJson.toString())
            Put("/user/pushurl", requestEntity).withHeaders(RawHeader("token",token)) ~> userRoute ~> check {
                response.status should be(StatusCodes.OK)
            }
        }
    }
}
