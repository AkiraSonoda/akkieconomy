/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http


import java.util.UUID

import akka.actor.{PoisonPill, Props}
import akka.http.javadsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.akkisim.economy.actors.UserActor
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{ContainerItemEntity, ItemSpecEntity, MarketInventory, UserEntity}
import org.akkisim.economy.dataproviders.items.{Container, ContainerUpdate}
import org.akkisim.economy.dataproviders.types.{RegisterPushUrl, RegisterUser, UUID_ZERO}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, BeforeAndAfterEach}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}


class ContainerRouteSpec extends BaseServiceTest with BeforeAndAfter with BeforeAndAfterEach with BeforeAndAfterAll with ScalaFutures {
    import containerItemService._
    import io.circe.generic.auto._
    import io.circe.syntax._
    
    val log: Logger = LoggerFactory.getLogger("ContainerRouteSpec")
    val user1: UserEntity = BaseTestData.user1
    val container1: Container = BaseTestData.container1

    trait Context {
        val containerRoute: Route = httpService.containerRouter.route
    }

    val Port = 4242
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    override def beforeAll {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20))

        val userServiceFuture = userService.createUser(avatarUUID = user1.avataruuid, avatarName = user1.avatarname, avatarPassword = user1.avatarpassword)
        Await.result(userServiceFuture, 10.seconds)
        userServiceFuture.onComplete {
            case Success(optUser) => optUser match {
                case Some( _ ) =>
                    val register = system.actorOf(Props(new UserActor(userService, user1.avataruuid, "", centralActor)), user1.avataruuid)
                    centralActor ! RegisterUser(user1.avataruuid,register)
                case None => fail("None returned from UserService")
            }
            case Failure(ex) =>
                fail("Could not create User", ex)
        }
        // We also need an empty MarketInventory Item
        val marketInventoryFuture = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = user1.avataruuid,
                itemtype = "WaterTank",
                origin = "e38d011e-b579-41f8-b0de-cad14182cd1a"
            )
        )
        Await.result(marketInventoryFuture, 10.seconds)
        val marketInventoryFuture2 = marketInventoryService.createMarketInventoryEntity(
            MarketInventory(
                id = UUID_ZERO,
                ownerid = user1.avataruuid,
                itemtype = "WaterTank",
                origin = "7d39fe82-9171-4a1a-a613-3a5d0d05905a"
            )
        )
        Await.result(marketInventoryFuture2, 10.seconds)

    }

    override def afterAll: Unit = {
        centralActor ! PoisonPill
        val userServiceFuture = userService.deleteUser(user1.avataruuid)
        Await.result(userServiceFuture, 10.seconds)
        val testContainerItemsFuture: Future[Int] = containerItemService.deleteContainerItem(container1.id)
        Await.result(testContainerItemsFuture, 10.seconds)
        val testInventoryItemsFuture = inventoryService.deleteAllInventoryItems()
        Await.result(testInventoryItemsFuture, 10.seconds)
        val testMarketInventoryFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(testMarketInventoryFuture, 10.seconds)
    }


    "ContainerService" should {
        "create a new Container" in new Context {
            wireMockServer.start()
            WireMock.configureFor(Host, Port)
            stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
            stubFor(post(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
            Central.userActors.get(user1.avataruuid) match {
                case Some(actorRef) => actorRef ! RegisterPushUrl(user1.avataruuid, "http://localhost:4242")
                case None => fail()
            }
            val waterJarCreate = Container(
                id = container1.id,
                ownerid = user1.avataruuid,
                name = "WaterTank",
                itemType = "WaterTank",
                price = 100,
                maxVolume = 20,
                currentVolume = 20,
                measureUnit = "Deciliter",
                origin = "e38d011e-b579-41f8-b0de-cad14182cd1a"
            )

            val json: String = waterJarCreate.asJson.toString()

            val requestEntity = HttpEntity(MediaTypes.`application/json`, waterJarCreate.asJson.toString())
            Post("/item/container", requestEntity) ~> containerRoute ~> check {
                response.status should be(StatusCodes.OK)
            }
            log.debug("Success")
        }
        "not create a new Container without open Slot in MarketInfentory" in new Context {
            wireMockServer.start()
            WireMock.configureFor(Host, Port)
            stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
            stubFor(post(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))

            Central.userActors.get(user1.avataruuid) match {
                case Some(actorRef) => actorRef ! RegisterPushUrl(user1.avataruuid, "http://localhost:4242")
                case None => fail()
            }
            val waterJarCreate = Container(
                id = UUID.randomUUID().toString,
                ownerid = user1.avataruuid,
                name = "WaterTank",
                itemType = "WaterTank",
                price = 100,
                maxVolume = 20,
                currentVolume = 20,
                measureUnit = "Deciliter"
            )

            val json: String = waterJarCreate.asJson.toString()

            val requestEntity = HttpEntity(MediaTypes.`application/json`, waterJarCreate.asJson.toString())
            // In Order to
            Post("/item/container", requestEntity) ~> Route.seal(containerRoute) ~> check {
                status should be(StatusCodes.NOT_FOUND)
            }
            log.debug("Success")
        }
        "not create a new Container with an unregistered origin" in new Context {
            wireMockServer.start()
            WireMock.configureFor(Host, Port)
            stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))
            stubFor(post(urlEqualTo("/")).willReturn(aResponse().withStatus(200)))

            Central.userActors.get(user1.avataruuid) match {
                case Some(actorRef) => actorRef ! RegisterPushUrl(user1.avataruuid, "http://localhost:4242")
                case None => fail()
            }
            val waterJarCreate = Container(
                id = UUID.randomUUID().toString,
                ownerid = user1.avataruuid,
                name = "WaterTank",
                itemType = "WaterTank",
                price = 100,
                maxVolume = 20,
                currentVolume = 20,
                measureUnit = "Deciliter",
                origin = "c70032e6-ca41-4d16-8111-7458fb5e871c"
            )

            val json: String = waterJarCreate.asJson.toString()

            val requestEntity = HttpEntity(MediaTypes.`application/json`, waterJarCreate.asJson.toString())
            // In Order to
            Post("/item/container", requestEntity) ~> Route.seal(containerRoute) ~> check {
                status should be(StatusCodes.NOT_FOUND)
            }
            log.debug("Success")
        }
        "retrieve Container by id" in new Context {
            Get("/item/container/"+container1.id) ~> containerRoute ~> check {
                responseAs[Container] should be(container1)
            }
        }
        "update the volume of the Container" in new Context {
            val containerUpdate = ContainerUpdate (
                avatarUUID = user1.avataruuid,
                itemType = "WaterTank",
                volume = 3,
                stateAffected = "thirst",
                stateModification = "subtraction"
            )

            val json: String = containerUpdate.asJson.toString()

            val requestEntity = HttpEntity(MediaTypes.`application/json`,containerUpdate.asJson.toString())
            Put("/item/container/"+container1.id, requestEntity) ~> containerRoute ~> check {
                responseAs[ContainerItemEntity].currentvolume should be(17)
            }
        }
        "update the volume of an unregistered Container" in new Context {
            val containerUpdate = ContainerUpdate (
                avatarUUID = user1.avataruuid,
                itemType = "WaterTank",
                volume = 3,
                stateAffected = "thirst",
                stateModification = "subtraction"
            )

            val json: String = containerUpdate.asJson.toString()

            val requestEntity = HttpEntity(MediaTypes.`application/json`,containerUpdate.asJson.toString())
            Put("/item/container/"+UUID.randomUUID().toString, requestEntity) ~> containerRoute ~> check {
                response.status should be(StatusCodes.NOT_FOUND)
            }
        }
        "delete Container" in new Context {
            Delete("/item/container/"+container1.id) ~> containerRoute ~> check {
                response.status should be(OK)
                whenReady(getContainerItemById("102dba0-4711-11e7-9598-0800200c9b69")) {
                    result => result should be(None: Option[Container])
                }
            }
        }
    }

}
