/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.akkisim.economy.actors.{CentralActor, PlantFactoryActor}
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.services._
import org.scalatest.{Matchers, WordSpec}

trait BaseServiceTest extends WordSpec with Matchers with FailFastCirceSupport with ScalatestRouteTest {

    private val databaseService = new DatabaseService()

    val actorSystem = ActorSystem("BaseServiceTest")
    val centralActor: ActorRef = actorSystem.actorOf(Props[CentralActor], "CentralActor")
    Central.optCentral = Some(centralActor)

    val inventoryService = new InventoryService(databaseService)
    val containerItemService = new ContainerItemsService(databaseService)
    val userService = new UserService(databaseService)(centralActor)
    val authService = new AuthService(databaseService,userService,actorSystem)
    val marketInventoryService = new MarketInventoryService(databaseService)
    val plantService = new PlantService(databaseService)
    val itemSpecService = new ItemSpecService(databaseService)
    val storeService = new StoreService(databaseService)

    // We need a PlantFactoryActor
    val plantFactoryActor: ActorRef = actorSystem.actorOf(
        Props(new PlantFactoryActor(inventoryService,plantService, actorSystem)), name = "PlantFactoryActor"
    )

    Central.optPlantFactory = Some(plantFactoryActor)
    Central.optPlantService = Some(plantService)
    Central.optInventoryService = Some(inventoryService)
    Central.optMarketInventoryService = Some(marketInventoryService)
    Central.optUserService = Some(userService)

    ItemSpec.initialize(itemSpecService)

    val httpService = new HttpService(
        inventoryService,containerItemService,
        userService, authService,marketInventoryService,
        plantService, itemSpecService, storeService
    )

}
