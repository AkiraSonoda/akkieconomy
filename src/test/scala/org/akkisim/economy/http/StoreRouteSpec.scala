/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http

import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.BaseTestData.user1
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{InventoryEntity, ItemSpecEntity, MarketInventory, StoreEntity}
import org.akkisim.economy.dataproviders.items.ItemState.NEW
import org.akkisim.economy.dataproviders.items.{StoreItem, StoreItemOsName}
import org.akkisim.economy.dataproviders.types.UUID_ZERO
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, BeforeAndAfterEach}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

/**
  * In the context of this Test,
  *
  * User1 is the Owner of the Trader
  * User2 is the Buyier of something
  */
class StoreRouteSpec extends BaseServiceTest with BeforeAndAfter with BeforeAndAfterEach with BeforeAndAfterAll with ScalaFutures {
    import io.circe.generic.auto._
    import io.circe.syntax._
    
    val log: Logger = LoggerFactory.getLogger("StoreRouteSpec")
    implicit val timeout: Timeout = Timeout(5 seconds)

    trait Context {
        val storeRoute: Route = httpService.storeRouter.route
    }


    override def beforeAll {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20,0,0,0,"AEWatertank"))
        ItemSpec.addToCache(ItemSpecEntity ("VENDOR",2,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("ESSEN",3,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_TREE",14,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("BASKET_COCONUT",5,50,20,0,0,0,"AE Basket Coconut"))
        ItemSpec.addToCache(ItemSpecEntity ("MARKET_BUYER",6,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_10",7,100,10))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_20",8,200,20))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_30",9,300,30))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_40",10,500,40))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_50",11,800,50))



        // We need an empty MarketInventory Item for the Store
        val marketInventoryFuture = marketInventoryService.createMarketInventoryEntity(
            MarketInventory( id = UUID_ZERO, ownerid = BaseTestData.user1.avataruuid, itemtype = "STORE_10" )
        )
        Await.result(marketInventoryFuture, 10.seconds)

        // We need an empty MarketInventory Item for Store_40
        val marketInventoryFuture2 = marketInventoryService.createMarketInventoryEntity(
            MarketInventory( id = UUID_ZERO, ownerid = BaseTestData.user1.avataruuid, itemtype = "STORE_40" )
        )
        Await.result(marketInventoryFuture2, 10.seconds)

        // We need an empty MarketInventory Item for Store_40
        val marketInventoryFuture3 = marketInventoryService.createMarketInventoryEntity(
            MarketInventory( id = UUID_ZERO, ownerid = BaseTestData.user1.avataruuid, itemtype = "STORE_50" )
        )
        Await.result(marketInventoryFuture3, 10.seconds)

        // We need an empty MarketInventory Item for BASKET_COCONUT
        val marketInventoryFuture4 = marketInventoryService.createMarketInventoryEntity(
            MarketInventory( id = UUID_ZERO, ownerid = BaseTestData.user1.avataruuid, itemtype = "BASKET_COCONUT" )
        )
        Await.result(marketInventoryFuture4, 10.seconds)

        // Create a Store Inventory Item
        val store = InventoryEntity (
            itemid = "33333333-4444-11e7-9598-0800200d9c66",
            itemname = "Store_10",
            itemtype = 7,
            itemstate = NEW.id,
            itemownerid = user1.avataruuid)
        val storeFuture = inventoryService.createInventoryItem(store)
        Await.result(storeFuture, 10.seconds )

        val testStoreInsert: Seq[Future[Int]] = BaseTestData.testStoreItems.map(s =>
            storeService.addStoreEntity(store.itemid,s))
        Await.result(Future.sequence(testStoreInsert), 10.seconds)
    }

    override def afterAll: Unit = {
        val testInventoryItemsFuture = inventoryService.deleteAllInventoryItems()
        Await.result(testInventoryItemsFuture, 10.seconds)
        val testMarketInventoryFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(testMarketInventoryFuture, 10.seconds)
        val testStoreFuture = storeService.deleteAllStores( )
        Await.result(testStoreFuture, 10.seconds)

    }


    "StoreRoute" should {
        "create a new Store" in new Context {
            val requestEntity = HttpEntity(MediaTypes.`application/json`, BaseTestData.store1.asJson.toString())
            Post("/item/store", requestEntity) ~> Route.seal(storeRoute) ~> check {
                responseAs[String] should be(BaseTestData.buyer1.ownerid)
            }
        }

        "Insert a new Store Item into Store_10" in new Context {
            val storeItem = StoreItem( itemtype = "STORE_40")
            val requestEntity = HttpEntity(MediaTypes.`application/json`, storeItem.asJson.toString())
            Put("/item/store/"+BaseTestData.store1.id, requestEntity) ~> storeRoute ~> check {
                response.status should be(StatusCodes.OK)
            }
        }

        "Insert 11th ItemType into Store_10" in new Context {
            val storeItem = StoreItem( itemtype = "STORE_50")
            val requestEntity = HttpEntity(MediaTypes.`application/json`, storeItem.asJson.toString())
            Put("/item/store/"+BaseTestData.store1.id, requestEntity) ~> storeRoute ~> check {
                response.status should be(StatusCodes.InsufficientStorage)
            }
        }

        """Insert a new "BASKET_COCONUT" with Quantity 20 into Store_10 """ in new Context {
            val storeItem = StoreItem( itemtype = "BASKET_COCONUT" )
            Put("/item/store/"+BaseTestData.store1.id, storeItem) ~> storeRoute ~> check {
                response.status should be(StatusCodes.OK)
            }
            val resultFuture: Future[Option[StoreEntity]] = storeService.getStoreEntity(BaseTestData.store1.id, 5)
            val result: Option[StoreEntity] = Await.result(resultFuture, 10.seconds)
            result match {
                case Some( se ) =>
                    assert(se.quantity == 40)
                case None =>
                    fail("""No "BASKET_COCONUT" found """)
            }
        }

        """Get a "WATERTANK" from Store_10 """ in new Context {
            Get("/item/store/"+BaseTestData.store1.id+"/WATERTANK/"+user1.avataruuid) ~> storeRoute ~> check {
                responseAs[StoreItemOsName] should be(StoreItemOsName("AEWatertank"))
                whenReady(storeService.getStoreEntity(BaseTestData.store1.id, 1)) {
                    result => result should be(None: Option[StoreEntity])
                }
            }
        }
    }

}
