/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import akka.actor.{ActorRef, ActorSystem, Props}
import org.akkisim.economy.dataproviders.db.{TokenEntity, TokenEntityTable, UserEntity, UserEntityTable}
import org.akkisim.economy.dataproviders.items.SignInResult
import org.akkisim.economy.utils.DummyActorSpec
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._

/**
  * @author Akira Sonoda
  */
class AuthServiceSpec extends FeatureSpec with BeforeAndAfter with GivenWhenThen {
    val log: Logger = LoggerFactory.getLogger("TaskServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global
    
    val actorSystem = ActorSystem("AuthServiceSpec")
    val dummyActor: ActorRef = actorSystem.actorOf(Props[DummyActorSpec], "CentralActor")
    
    
    val databaseService = new DatabaseService()
    val userService = new UserService(databaseService)(dummyActor)
    val authService = new AuthService(databaseService,userService,actorSystem)

    before {
        val userFuture: Future[Option[UserEntity]] = userService.createUser(
            avatarUUID = "99999999-4711-11e7-9598-0800200c9a66",
            avatarName = "Akira Sonoda",
            avatarPassword = "password"
        )
        Await.result(userFuture, 10.seconds)
    }


    after {
        val userFuture: Future[Int] = userService.deleteUser("99999999-4711-11e7-9598-0800200c9a66")
        Await.result(userFuture, 10.seconds)

        val tokenFuture: Future[Int] = authService.deleteToken("99999999-4711-11e7-9598-0800200c9a66")
        Await.result(tokenFuture, 10.seconds)
    }



    info("As AuthService UserActor")
    info("I want to be able to signIn with various possibilities")
    info("in order to have a basic Userid Password security")

    feature("A UserService to manage UserActor") {

        scenario("signIn a UserActor") {

            Given("a UserActor is available")
            When("I signIn with this")
            val tokenFuture = authService.signIn(
                avatarUUID = "99999999-4711-11e7-9598-0800200c9a66",
                avatarName = "Akira Sonoda",
                password = "password",
                newPassword = "",
                pushUrl = "http://dereos.org"
            )

            Then("the result should be a new Token")
            val result: Try[Option[SignInResult]] = Await.ready(tokenFuture, Duration.Inf).value.get
            result match {
                case Success(t) =>
                    t match {
                        case Some(token) => assert(token.token.length > 0)
                        case None => fail("No Token Returned")
                    }
                case Failure(e) => fail("Failure in signIn UserActor:", e)
            }
        }
    }
}


