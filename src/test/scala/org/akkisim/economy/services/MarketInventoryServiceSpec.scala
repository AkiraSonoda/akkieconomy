/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import java.util.UUID

import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{ItemSpecEntity, MarketInventory, MarketInventoryEntity}
import org.akkisim.economy.dataproviders.types.UUID_ZERO
import org.scalatest.{BeforeAndAfterAll, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}
import scala.util.{Failure, Success, Try}


/**
  * @author Akira Sonoda
  */
class MarketInventoryServiceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll {
    val log: Logger = LoggerFactory.getLogger("MarketInventoryServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global
    val marketInventoryService = new MarketInventoryService(new DatabaseService())

    val testOwnerId = "102dba0-4bc7-4711-9598-0800200c9a66"
    val testItemID = "102dba0-4711-11e7-9598-0800200c9a66"

    override def beforeAll ( ): Unit = {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20))
        ItemSpec.addToCache(ItemSpecEntity ("BASKET_COCONUT",5,50,20))
    }

    override def afterAll ( ): Unit = {
        val deleteAllFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(deleteAllFuture, 10.seconds )
    }

    info("As MarketInventory User")
    info("I want to be able to create, read, update, delete MarketInventoryEntities")
    info("in order to have a quick overview of each grid users Inventory Items")

    feature("A MarketInventory to manage Inventory Items") {

        scenario("insert an initial MarketInventoryEntity") {

            Given("a MarketInventoryEntity is available")
            val marketInventory = MarketInventory(
                id = UUID_ZERO,
                ownerid = testOwnerId,
                itemtype = "WaterTank"
            )

            When("a MarketInventoryEntity is created with the MarketInventoryService")
            val marketInventoryFuture= marketInventoryService.createMarketInventoryEntity(marketInventory)

            Then("the result should be the number of created MarketInventoryEntityEntities")
            val result: Try[Int] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in createMarketInventoryEntity:", e)
            }
        }

        scenario("Read all MarketInventoryEntitys") {
            Given("Some MarketInventoryEntitys are inserted")
            When("I read all MarketInventoryEntitys")
            val marketInventoryFuture = marketInventoryService.getMarketInventoryEntities
            Then("I should get a List of MarketInventoryEntitys")
            val result: Try[Seq[MarketInventoryEntity]] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
            result match {
                case Success(resultset) =>
                    assert(resultset.size == 1)
                    resultset.foreach(f = a => {
                        assert(a.id.equalsIgnoreCase(UUID_ZERO))
                        assert(a.itemtype == 1)
                    })
                case Failure(e) => fail("failure in getMarketInventoryEntities:", e)
            }
        }

        scenario("Read specific MarketInventoryEntity") {
            Given("Some MarketInventories are inserted")
            When("I read the MarketInventoryEntity with the correct Unique Key")
            val marketInventoryFuture = marketInventoryService.getMarketInventoryItem(
                id = UUID_ZERO,
                ownerid = testOwnerId,
                itemType = ItemSpec.getIntOfKind("WaterTank")
            )
            Then("I should get exactly one MarketInventoryEntity")
            val result: Try[Option[MarketInventoryEntity]] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
            result match {
                case Success(mi) =>
                    mi match {
                        case Some(it) =>
                            assert(it.id.equalsIgnoreCase(UUID_ZERO))
                            assert(it.ownerid.equalsIgnoreCase(testOwnerId))
                            assert(it.itemtype == 1)
                        case None => fail("No MarketInventoryEntity found with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in getMarketInventoryEntityById ", e)
            }
        }

        scenario("Update a specific MarketInventoryEntity") {
            Given("Some MarketInventories are inserted")
            When("I update the MarketInventoryEntity with id \"102dba0-4711-11e7-9598-0800200c9a66\" to Kind \"Gold\"")
            val marketInventoryUpdate = MarketInventory(
                id = testItemID,
                ownerid = testOwnerId,
                itemtype = "WaterTank"
            )

            val marketInventoryFuture = marketInventoryService.updateMarketInventoryEntity(
                id = UUID_ZERO,
                ownerid = testOwnerId,
                "WaterTank",
                origin = UUID_ZERO,
                marketInventoryUpdate)

            Then("I should get one MarketInventoryEntity with id \"102dba0-4711-11e7-9598-0800200c9a66\" and some kind of \"Restaurant\"")
            val result: Try[Option[MarketInventoryEntity]] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
            result match {
                case Success(item) =>
                    item match {
                        case Some(it) =>
                            assert(it.id.equalsIgnoreCase(testItemID))
                            assert(it.ownerid.equalsIgnoreCase(testOwnerId))
                            assert(it.itemtype == 1)
                        case None => fail("No MarketInventoryEntity found with id: " + testItemID +
                                " - ownerid: " + testOwnerId
                            )
                    }
                case Failure(e) => log.error("Failure in updateMarketInventoryEntity ", e)
            }
        }


        scenario("delete a MarketInventoryEntity") {
            Given("a MarketInventoryEntity of the id \"102dba0-4711-11e7-9598-0800200c9a66\" is inserted")
            When("I delete the MarketInventoryEntity of id \"102dba0-4711-11e7-9598-0800200c9a66\"")
            val marketInventoryFuture = marketInventoryService.deleteMarketInventoryEntity(
                id = "102dba0-4711-11e7-9598-0800200c9a66"
            )
            Then("should get the number of deleted Industries")
            val result: Try[Int] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in deleteMarketInventoryEntity:", e)
            }
        }

        scenario("Create Two initial MarketInventoryEntities and delete them sequentially using the exact Method") {

            Given("Two MarketInventory Items are inserted and all have an id: UUID_ZERO")
            for( _ <- 1 to 2) {
                val createFuture = marketInventoryService.createMarketInventoryEntity(
                    MarketInventory(
                        id = UUID_ZERO,
                        ownerid = testOwnerId,
                        itemtype = "Basket_Coconut"
                    )
                )
                val result: Try[Int] = Await.ready(createFuture, Duration.Inf).value.get
                result match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in createMarketInventoryEntity:", e)
                }

            }
            When("I delete the first MarketInventoryEntity with id UUID_ZERO")

                val deleteFuture = marketInventoryService.deleteUniqueMarketInventoryItem( UUID_ZERO, testOwnerId, 5, UUID_ZERO)

                val updateresult: Try[Int] = Await.ready(deleteFuture, Duration.Inf).value.get
                updateresult match {
                    case Success(numDeleted) => assert(numDeleted == 1)
                    case Failure(e) => log.error("Failure in deleteUniqueMarketInventoryItem ", e)
                }

            Then("I should still get one MarketInventoryEntity with id: "+UUID_ZERO+" and ownerId: " +testOwnerId)
                val marketInventoryFuture = marketInventoryService.getMarketInventoryItem(
                    id = UUID_ZERO,
                    ownerid = testOwnerId,
                    itemType = ItemSpec.getIntOfKind("Basket_Coconut")
                )
                val checkresult1: Try[Option[MarketInventoryEntity]] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
                checkresult1 match {
                    case Success(mi) =>
                        mi match {
                            case Some(it) =>
                                assert(it.id.equalsIgnoreCase(UUID_ZERO))
                                assert(it.ownerid.equalsIgnoreCase(testOwnerId))
                                assert(it.itemtype == 5)
                            case None => fail("No MarketInventoryEntity found with ownerid: " +testOwnerId+" and id: " + UUID_ZERO)
                        }
                    case Failure(e) =>
                        log.error("Failure in getMarketInventoryEntityById ", e)
                }

            When("I delete the second MarketInventoryEntity with id UUID_ZERO")

                val deleteFuture2 = marketInventoryService.deleteUniqueMarketInventoryItem( UUID_ZERO, testOwnerId, 5, UUID_ZERO)

                val deleteresult2: Try[Int] = Await.ready(deleteFuture2, Duration.Inf).value.get
                deleteresult2 match {
                    case Success(numDeleted) => assert(numDeleted == 1)
                    case Failure(e) => log.error("Failure in updateMarketInventoryEntity ", e)
                }

            Then("I should still get no more MarketInventoryEntity with id: "+UUID_ZERO+" and ownerId: " +testOwnerId)
                val marketInventoryFuture2 = marketInventoryService.getMarketInventoryItem(
                    id = UUID_ZERO,
                    ownerid = testOwnerId,
                    itemType = ItemSpec.getIntOfKind("Basket_Coconut")
                )
                val checkresult2: Try[Option[MarketInventoryEntity]] = Await.ready(marketInventoryFuture2, Duration.Inf).value.get
                checkresult2 match {
                    case Success(mi) =>
                        mi match {
                            case Some(_) =>
                                fail("A MarketInventoryEntity found with ownerid: " +testOwnerId+ " and id: " + UUID_ZERO)
                            case None =>
                                assert( true)
                        }
                    case Failure(e) =>
                        log.error("Failure in getMarketInventoryEntityById ", e)
                }

        }




        scenario("Create Two initial MarketInventoryEntities and update them sequentially") {
            val uuid_1 = UUID.randomUUID().toString
            val uuid_2 = UUID.randomUUID().toString

            Given("Two MarketInventory Items are inserted and all have an id: UUID_ZERO")
                for( _ <- 1 to 2) {
                    val createFuture = marketInventoryService.createMarketInventoryEntity(
                        MarketInventory(
                            id = UUID_ZERO,
                            ownerid = testOwnerId,
                            itemtype = "WaterTank"
                        )
                    )
                    val result: Try[Int] = Await.ready(createFuture, Duration.Inf).value.get
                    result match {
                        case Success(t) => assert(t == 1)
                        case Failure(e) => fail("Failure in createMarketInventoryEntity:", e)
                    }

                }
            When("I update the first MarketInventoryEntity with id UUID_ZERO to a valid UUID")
                val marketInventoryUpdate = MarketInventory(
                    id = uuid_1,
                    ownerid = testOwnerId,
                    itemtype = "WaterTank"
                )

                val updateFuture = marketInventoryService.updateMarketInventoryEntity(
                    id = UUID_ZERO,
                    ownerid = testOwnerId,
                    "WaterTank",
                    origin = UUID_ZERO,
                    marketInventoryUpdate)

                val updateresult: Try[Option[MarketInventoryEntity]] = Await.ready(updateFuture, Duration.Inf).value.get
                updateresult match {
                    case Success(item) =>
                        item match {
                            case Some(it) =>
                                assert(it.id.equalsIgnoreCase(uuid_1))
                                assert(it.ownerid.equalsIgnoreCase(testOwnerId))
                                assert(it.itemtype == 1)
                            case None =>
                                fail("No MarketInventoryEntity found with id: " +
                                    uuid_1 + " - ownerid: " + testOwnerId
                                )
                        }
                    case Failure(e) => log.error("Failure in updateMarketInventoryEntity ", e)
                }

            Then("I should still get one MarketInventoryEntity with id: "+UUID_ZERO+" and ownerId: " +testOwnerId)
                val marketInventoryFuture = marketInventoryService.getMarketInventoryItem(
                    id = UUID_ZERO,
                    ownerid = testOwnerId,
                    itemType = ItemSpec.getIntOfKind("WaterTank")
                )
                val checkresult1: Try[Option[MarketInventoryEntity]] = Await.ready(marketInventoryFuture, Duration.Inf).value.get
                checkresult1 match {
                    case Success(mi) =>
                        mi match {
                            case Some(it) =>
                                assert(it.id.equalsIgnoreCase(UUID_ZERO))
                                assert(it.ownerid.equalsIgnoreCase(testOwnerId))
                                assert(it.itemtype == 1)
                            case None => fail("No MarketInventoryEntity found with ownerid: " +testOwnerId+" and id: " + UUID_ZERO)
                        }
                    case Failure(e) =>
                        log.error("Failure in getMarketInventoryEntityById ", e)
                }

            When("I update the second MarketInventoryEntity with id UUID_ZERO to a valid UUID")
                val marketInventoryUpdate2 = MarketInventory(
                    id = uuid_2,
                    ownerid = testOwnerId,
                    itemtype = "WaterTank"
                )

                val updateFuture2 = marketInventoryService.updateMarketInventoryEntity(
                    id = UUID_ZERO,
                    ownerid = testOwnerId,
                    "WaterTank",
                    origin = UUID_ZERO,
                    marketInventoryUpdate2)

                val updateresult2: Try[Option[MarketInventoryEntity]] = Await.ready(updateFuture2, Duration.Inf).value.get
                updateresult2 match {
                    case Success(item) =>
                        item match {
                            case Some(it) =>
                                assert(it.id.equalsIgnoreCase(uuid_2))
                                assert(it.ownerid.equalsIgnoreCase(testOwnerId))
                                assert(it.itemtype == 1)
                            case None =>
                                fail("No MarketInventoryEntity found with id: " +
                                    uuid_2 + " - ownerid: " + testOwnerId
                                )
                        }
                    case Failure(e) => log.error("Failure in updateMarketInventoryEntity ", e)
                }

            Then("I should still get no more MarketInventoryEntity with id: "+UUID_ZERO+" and ownerId: " +testOwnerId)
                val marketInventoryFuture2 = marketInventoryService.getMarketInventoryItem(
                    id = UUID_ZERO,
                    ownerid = testOwnerId,
                    itemType = ItemSpec.getIntOfKind("WaterTank")
                )
                val checkresult2: Try[Option[MarketInventoryEntity]] = Await.ready(marketInventoryFuture2, Duration.Inf).value.get
                checkresult2 match {
                    case Success(mi) =>
                        mi match {
                            case Some(_) =>
                                fail("A MarketInventoryEntity found with ownerid: " +testOwnerId+ " and id: " + UUID_ZERO)
                            case None =>
                                assert( true)
                    }
                    case Failure(e) =>
                        log.error("Failure in getMarketInventoryEntityById ", e)
                }

            Then("I delete the two MarketInventory Items")
                val deleteFuture1 = marketInventoryService.deleteMarketInventoryEntity(
                    id = uuid_1
                )
                val deleteresult1: Try[Int] = Await.ready(deleteFuture1, Duration.Inf).value.get
                deleteresult1 match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in deleteMarketInventoryEntity:", e)
                }

                val deleteFuture2 = marketInventoryService.deleteMarketInventoryEntity(
                    id = uuid_2
                )
                val deleteresult2: Try[Int] = Await.ready(deleteFuture2, Duration.Inf).value.get
                deleteresult2 match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in deleteMarketInventoryEntity:", e)
                }

        }

    }
}


