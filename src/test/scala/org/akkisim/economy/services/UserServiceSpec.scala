/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.StatusCodes
import org.akkisim.economy.dataproviders.db.{UserEntity, UserEntityUpdate}
import org.akkisim.economy.dataproviders.types.PushUrl
import org.akkisim.economy.utils.DummyActorSpec
import org.scalatest.{FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class UserServiceSpec extends FeatureSpec with GivenWhenThen {
    val log: Logger = LoggerFactory.getLogger("UserServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global
    val actorSystem = ActorSystem("BaseServiceTest")
    val dummyActor: ActorRef = actorSystem.actorOf(Props[DummyActorSpec], "CentralActor")
    
    val userService = new UserService(new DatabaseService())(dummyActor)


    info("As UserService UserActor")
    info("I want to be able to create, read, update, delete Users")
    info("in order to have a quick overview of each economy user")

    feature("A UserService to manage User") {

        scenario("insert a User") {

            Given("a UserEntity is available")
            val user = UserEntity(
                avataruuid = "99999999-4711-11e7-9598-0800200c9a66",
                avatarname = "Akira Sonoda",
                avatarpassword = "password",
                avatarhunger = Some(0),
                avatarthirst = Some(100),
                avatarlevel = Some(0)
            )

            When("a User is created with the UserService")
            val userEntityFuture = userService.createUser(user)

            Then("the result should be the number of created UserEntities")
            val result: Try[Int] = Await.ready(userEntityFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in createUser:", e)
            }
        }
    
        scenario("Deliver a pushUrl to the central Actor") {
            val user = UserEntity(
                avataruuid = "99999999-4711-11e7-9598-0800200c9a66",
                avatarname = "Akira Sonoda",
                avatarpassword = "password",
                avatarhunger = Some(0),
                avatarthirst = Some(100),
                avatarlevel = Some(0)
            )
    
            Given("A new URL to push is available")
            val pushurl = PushUrl("http//somewhere.com:8080")
            When("I push the Url to the User Service")
            val statusCode = userService.pushUrl(user, pushurl)
            Then("I should get a StatusCode of 200")
            assert(statusCode.equals(StatusCodes.OK))
        }
    
    
    
        scenario("Read all UserEntities") {
            Given("Some UserEntities are inserted")
            When("I read all user")
            val userFuture = userService.getUsers
            Then("I should get a List of UserEntities")
            val result: Try[Seq[UserEntity]] = Await.ready(userFuture, Duration.Inf).value.get
            result match {
                case Success(resultset) =>
                    assert(resultset.lengthCompare(1) == 0)
                    resultset.foreach(f = a => {
                        assert(a.avataruuid.equalsIgnoreCase("99999999-4711-11e7-9598-0800200c9a66"))
                        a.avatarthirst match {
                            case Some(thirst) => assert(thirst == 100)
                            case None => fail("UserActor without Thirst")
                        }

                    })
                case Failure(e) => fail("failure in getTasks:", e)
            }
        }

        scenario("Read specific User") {
            Given("Some Users are inserted")
            When("I read the User with id \"99999999-4711-11e7-9598-0800200c9a66\"")
            val userFuture = userService.getUserById("99999999-4711-11e7-9598-0800200c9a66")
            Then("I should get one User with the id \"99999999-4711-11e7-9598-0800200c9a66\" and some Kind of \"Thirst\"")
            val result: Try[Option[UserEntity]] = Await.ready(userFuture, Duration.Inf).value.get
            result match {
                case Success(answer) =>
                    answer match {
                        case Some(user) =>
                            assert(user.avataruuid.equalsIgnoreCase("99999999-4711-11e7-9598-0800200c9a66"))
                            user.avatarthirst match {
                                case Some(thirst) => assert(thirst == 100)
                                case None => fail("UserActor without Thirst")
                            }
                        case None => fail("No User found with id \"99999999-4711-11e7-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in getUserById ", e)
            }
        }

        scenario("Update a specific User") {
            Given("Some User are inserted")
            When("I update the User with id \"99999999-4711-11e7-9598-0800200c9a66\" to Hunger: 100")
            val userUpdate = UserEntityUpdate(
                avataruuid = "99999999-4711-11e7-9598-0800200c9a66",
                avatarname = "Akira Sonoda",
                avatarpassword = "password",
                avatarhunger = Some(100),
                avatarthirst = Some(100),
                avatarlevel = Some(0)
            )
            val userFuture = userService.updateUser("99999999-4711-11e7-9598-0800200c9a66",userUpdate)
            Then("I should get one User with id \"99999999-4711-11e7-9598-0800200c9a66\" and some avatarhunger of 100")
            val result: Try[Option[UserEntity]] = Await.ready(userFuture, Duration.Inf).value.get
            result match {
                case Success(answer) =>
                    answer match {
                        case Some(user) =>
                            assert(user.avataruuid.equalsIgnoreCase("99999999-4711-11e7-9598-0800200c9a66"))
                            user.avatarthirst match {
                                case Some(thirst) => assert(thirst == 100)
                                case None => fail("UserActor without Thirst")
                            }
                        case None => fail("No User found with id \"99999999-4711-11e7-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in updateUser ", e)
            }
        }

        scenario("delete a User") {
            Given("a User with the id \"99999999-4711-11e7-9598-0800200c9a66\" is inserted")
            When("I delete the User wit id \"99999999-4711-11e7-9598-0800200c9a66\"")
            val userFuture = userService.deleteUser(avatarUUID = "99999999-4711-11e7-9598-0800200c9a66")
            Then("should get the number of deleted Users")
            val result: Try[Int] = Await.ready(userFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in deleteUser:", e)
            }
        }
    }
}


