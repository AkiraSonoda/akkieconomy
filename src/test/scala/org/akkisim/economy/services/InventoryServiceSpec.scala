/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{InventoryEntity, InventoryEntityUpdate, ItemSpecEntity}
import org.scalatest.{BeforeAndAfterAll, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class InventoryServiceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll {
    val log: Logger = LoggerFactory.getLogger("InventoryServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global

    val databaseService = new DatabaseService()
    val inventoryService = new InventoryService(databaseService)

    override def beforeAll: Unit = {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20))
        ItemSpec.addToCache(ItemSpecEntity ("VENDOR",2,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("ESSEN",3,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("BASKET_COCONUT",5,50,20))
        ItemSpec.addToCache(ItemSpecEntity ("MARKET_BUYER",6,0,0))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_10",7,100,10))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_20",8,200,20))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_30",9,300,30))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_40",10,500,40))
        ItemSpec.addToCache(ItemSpecEntity ("STORE_50",11,800,50))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_BUSH",13,300,10,1800,1080,10))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_TREE",14,500,20,3600,1800,20))
        ItemSpec.addToCache(ItemSpecEntity ("PLANT_VEGETABLE",12,100,5,700,700,5))

        val result = inventoryService.deleteAllInventoryItems()
        Await.result(result, 10.seconds)
    }

    override def afterAll: Unit = {
        val result = inventoryService.deleteAllInventoryItems()
        Await.result(result, 10.seconds)
    }

    info("As InventoryService UserActor")
    info("I want to be able to create, read, update, delete InventoryItems")
    info("in order to maintain each Players Inventories")

    feature("A InventoryService to manage InventoryItems") {

        scenario("insert an InventoyItem") {

            Given("a Inventory is available")
            val inventoryItem = InventoryEntity(
                itemid = "102dba0-4711-11e7-9598-0800200c9a66",
                itemownerid = "102dba0-4bc7-4711-9598-0800200c9a66",
                itemname = "InventoryServiceSpecItem",
                itemprice = 100,
                itemtype = 1,
                itemstate = 1
            )
            val inventoryItem2 = InventoryEntity(
                itemid = "102dba0-4711-11e7-9598-0800200c9a66",
                itemownerid = "102dba0-4bc7-4711-9598-0800200c9a66",
                itemname = "InventoryServiceSpecItem",
                itemprice = 200,
                itemtype = 1,
                itemstate = 1
            )

            When("a InventoryItem is created with the InventoryService")
                val InventoryItemFuture = inventoryService.createInventoryItem(inventoryItem)

            Then("the result should be the number of created InventoryItems")
                val result: Try[Int] = Await.ready(InventoryItemFuture, Duration.Inf).value.get
                result match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in createInventoryItem:", e)
                }

            When("The same InventoryItem is created with the InventoryService")
                val InventoryItemFuture2 = inventoryService.createInventoryItem(inventoryItem2)
                val result2: Try[Int] = Await.ready(InventoryItemFuture2, Duration.Inf).value.get
                result2 match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in createInventoryItem:", e)
                }

            Then("the Item Price should remain the same because a new Item with the same ID will not be inserted")
                val InventoryItemsFuture2 = inventoryService.getInventoryItemById(inventoryItem2.itemid)
                val getResult: Try[Option[InventoryEntity]] = Await.ready(InventoryItemsFuture2, Duration.Inf).value.get
                getResult match {
                case Success(inventory) =>
                    inventory match {
                        case Some(inv) =>
                            assert(inv.itemid.equalsIgnoreCase(inventoryItem2.itemid))
                            inv.itemprice.equals(100)
                        case None => fail("No InventoryItem found with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in getInventoryItemById ", e)
            }


        }

        scenario("Read all InventoryItems") {
            Given("Some InventoryItems are inserted")
            When("I read all InventoyItems")
            val InventoryItemsFuture = inventoryService.getInventoryItems
            Then("I should get a List of Industries")
            val result: Try[Seq[InventoryEntity]] = Await.ready(InventoryItemsFuture, Duration.Inf).value.get
            result match {
                case Success(resultset) =>
                    assert(resultset.size == 1)
                    resultset.foreach(f = a => {
                        assert(a.itemid.equalsIgnoreCase("102dba0-4711-11e7-9598-0800200c9a66"))
                        a.itemprice.equals(100)
                    })
                case Failure(e) => fail("failure in getInventoryItems:", e)
            }
        }

        scenario("Read specific InventoryItem") {
            Given("Some InventoryItems are inserted")
            When("I read the InventoryItem wit id \"102dba0-4711-11e7-9598-0800200c9a66\"")
            val InventoryItemsFuture = inventoryService.getInventoryItemById("102dba0-4711-11e7-9598-0800200c9a66")
            Then("I should get one InventoryItem with the id \"102dba0-4711-11e7-9598-0800200c9a66\" and some Price of 100")
            val result: Try[Option[InventoryEntity]] = Await.ready(InventoryItemsFuture, Duration.Inf).value.get
            result match {
                case Success(industry) =>
                    industry match {
                        case Some(ind) =>
                            assert(ind.itemid.equalsIgnoreCase("102dba0-4711-11e7-9598-0800200c9a66"))
                            ind.itemprice.equals(100)
                        case None => fail("No InventoryItem found with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in getInventoryItemById ", e)
            }
        }

        scenario("Read specific InventoryItem with the OpenSim InventoryItemId") {
            Given("Some InventoryItems are inserted")
            When("I read the InventoryItem wit idOs \"102dba0-4bc7-4711-9598-0800200c9a66\"")
            val InventoryItemsFuture = inventoryService.getInventoryItemByOwnerId("102dba0-4bc7-4711-9598-0800200c9a66")
            Then("I should get one InventoryItem with the idOs \"102dba0-4bc7-4711-9598-0800200c9a66\" and some Price of 100")
            val result: Try[Option[InventoryEntity]] = Await.ready(InventoryItemsFuture, Duration.Inf).value.get
            result match {
                case Success(industry) =>
                    industry match {
                        case Some(ind) =>
                            assert(ind.itemownerid.equalsIgnoreCase("102dba0-4bc7-4711-9598-0800200c9a66"))
                            ind.itemprice.equals(100)
                        case None => fail("No InventoryItem found with id \"102dba0-4bc7-4711-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in getInventoryItemById ", e)
            }
        }


        scenario("Update a specific InventoryItem") {
            Given("Some InventoryItems are inserted")
            When("I update the InventoryItem with id \"102dba0-4711-11e7-9598-0800200c9a66\" to Price 999")
            val inventoryItemUpdate = InventoryEntityUpdate(
                itemid = "102dba0-4711-11e7-9598-0800200c9a66",
                itemownerid = "102dba0-4bc7-4711-9598-0800200c9a66",
                itemname = "InventoryServiceSpecItem",
                itemprice = 999,
                itemtype = 1,
                itemstate = 1
            )
            val InventoryItemFuture = inventoryService.updateInventoryItem("102dba0-4711-11e7-9598-0800200c9a66",inventoryItemUpdate)
            Then("I should get one Industry with id \"102dba0-4711-11e7-9598-0800200c9a66\" and some kind of \"Restaurant\"")
            val result: Try[Option[InventoryEntity]] = Await.ready(InventoryItemFuture, Duration.Inf).value.get
            result match {
                case Success(good) =>
                    good match {
                        case Some(g) =>
                            assert(g.itemid.equalsIgnoreCase("102dba0-4711-11e7-9598-0800200c9a66"))
                            g.itemprice.equals(999)
                        case None => fail("No InventoryItem found with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                    }
                case Failure(e) => log.error("Failure in updateInventoryItem ", e)
            }
        }

        scenario("manage some plants") {
            Given("Three Plants are inserted for User: " + BaseTestData.user1.avataruuid)
                val p1 = inventoryService.createInventoryItem(BaseTestData.inventoryItemPlant1)
                Await.result(p1, 10.seconds)
                val p2 = inventoryService.createInventoryItem(BaseTestData.inventoryItemPlant2)
                Await.result(p2, 10.seconds)
                val p3 = inventoryService.createInventoryItem(BaseTestData.inventoryItemPlant3)
                Await.result(p3, 10.seconds)
            When("I request all Plants for User: "+ BaseTestData.user1.avataruuid )
                val plantServiceInventoryFuture = inventoryService.getPlantsPerUser(BaseTestData.user1.avataruuid)
            Then("I should get a Sequence with 3 InventoryItems")
                val result = Await.ready(plantServiceInventoryFuture, Duration.Inf).value.get
                result match {
                    case Success(t) =>
                        assert(t.size == 3)
                    case Failure(_) => fail("Failure in getting 3 Plants")
                }
        }

        scenario("delete a InventoryItem") {
            Given("a InventoryItem with the id \"102dba0-4711-11e7-9598-0800200c9a66\" is inserted")
            When("I delete the InventoryItem with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
            val InventoryItemsFuture = inventoryService.deleteInventoryItem("102dba0-4711-11e7-9598-0800200c9a66")
            Then("should get the number of deleted Industries")
            val result: Try[Int] = Await.ready(InventoryItemsFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in deleteInventoryItem:", e)
            }
        }

    }
}
