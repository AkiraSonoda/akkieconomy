/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.BaseTestData.user1
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{InventoryEntity, ItemSpecEntity, StoreEntity}
import org.akkisim.economy.dataproviders.items.ItemState.NEW
import org.akkisim.economy.dataproviders.items.{StoreItem, StoreItemOsName}
import org.scalatest.{BeforeAndAfterAll, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class StoreServiceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll {
    val log: Logger = LoggerFactory.getLogger("StoreServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global

    val databaseService = new DatabaseService()
    val storeService = new StoreService(databaseService)
    val inventoryService = new InventoryService(databaseService)
    val marketInventoryService = new MarketInventoryService(databaseService)



    override def beforeAll ( ): Unit = {
        ItemSpec.addToCache(ItemSpecEntity ("WATERTANK",1,20,20,0,0,0,"AE Watertank"))
        ItemSpec.addToCache(ItemSpecEntity ("BASKET_COCONUT",5,50,20,0,0,0,"AE Basket Coconut"))
        Central.optMarketInventoryService = Some(marketInventoryService)

        // Create a Store Inventory Item

        val store = InventoryEntity (
            itemid = "c6d6ffc6-4ebd-4979-9208-3e12080a791e",
            itemname = "Store_10",
            itemtype = 7,
            itemstate = NEW.id,
            itemownerid = user1.avataruuid)
        val storeFuture = inventoryService.createInventoryItem(store)
        Await.result(storeFuture, 10.seconds )

        val storeItem1 = StoreItem(
            itemtype = "BASKET_COCONUT"
        )
        val storeEntityFuture = storeService.addStoreEntity(store.itemid, storeItem1)
        Await.result(storeEntityFuture, 10.seconds )

        val storeEntityFuture2 = storeService.addStoreEntity("ffffffff-4ebd-4979-9208-3e12080a791e", storeItem1)
        Await.result(storeEntityFuture2, 10.seconds )
    }

    override def afterAll ( ): Unit = {
        val miFuture = marketInventoryService.deleteAllMarketInventory()
        Await.result(miFuture, 10.seconds )

        val invFuture = inventoryService.deleteAllInventoryItems()
        Await.result(invFuture, 10.seconds )

        val storeFuture = storeService.deleteAllStores()
        Await.result(storeFuture, 10.seconds )
    }

    info("As StoreService User")
    info("I want to be able to create, read, update, delete StoreItems")
    info("in order to have a central location to manage all Storage of the economy")

    feature("Storage to manage the storage of several Items") {

        scenario("insert an Item into a Store") {

            Given("a Store Item is available")
                val storeItem = StoreItem(
                    itemtype = "Watertank"
                )

            When("a Store item is added to the Store")
                val storeEntityFuture = storeService.addStoreEntity("c6d6ffc6-4ebd-4979-9208-3e12080a791e", storeItem)

            Then("the result should be the number of created StoreEntities")
                val result: Try[Int] = Await.ready(storeEntityFuture, Duration.Inf).value.get
                result match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in addStoreEntity:", e)
                }
        }


        scenario("Read all items of a given Store") {
            Given("Some Store Items are inserted")
            When("I read all Items of a given Store")
                val storeFuture = storeService.getStoreById("c6d6ffc6-4ebd-4979-9208-3e12080a791e")
            Then("I should get a List of StoreItems")
                val result: Try[Seq[StoreEntity]] = Await.ready(storeFuture, Duration.Inf).value.get
                result match {
                    case Success(resultset) =>
                        assert(resultset.length == 2)
                        resultset.foreach(f = a => {
                            assert ( a.quantity == 20 )
                        })
                    case Failure(e) => fail("failure in StoreItems:", e)
                }
        }

        scenario("Read specific StoreItemOsName") {
            Given("Some StoreItemOsName are inserted")
            When("I read the StoreItemOsName with type \"BASKET_COCONUT\"")
                val storeFuture = storeService.getStoreEntity(
                    "c6d6ffc6-4ebd-4979-9208-3e12080a791e"
                    ,ItemSpec.getIntOfKind("BASKET_COCONUT")
                )
            Then("I should get one Price with price 50")
                val result: Try[Option[StoreEntity]] = Await.ready(storeFuture, Duration.Inf).value.get
                result match {
                    case Success(answer) =>
                        answer match {
                            case Some(storeItem) =>
                                assert(storeItem.quantity == 20)
                            case None => fail("No StoreItemOsName found with id \"BASKET_COCONUT\"")
                        }
                    case Failure(e) => log.error("Failure in getStoreEntity ", e)
                }
        }

        scenario("Update a specific StoreItemOsName") {
            Given("Some StoreItems are inserted")
            When("""I add 20 to the quantity of StoreItemOsName with kind "BASKET_COCONUT" """)
                val storeItem  = StoreItem(
                    itemtype = "WATERTANK"
                )
                val createFuture = storeService.addStoreEntity("c6d6ffc6-4ebd-4979-9208-3e12080a791e", storeItem)
            Then("""I should get the confirmation of 1 updated StoreItemOsName""")
                val createResult: Try[Int] = Await.ready(createFuture, Duration.Inf).value.get
                createResult match {
                    case Success(answer) => assert(answer == 1)
                    case Failure(e) => log.error("Failure in update specific StoreItemOsName ", e)
                }
            When("""I read the Store Item with type "WATERTANK" from the same Store""")
                val readFuture = storeService.getStoreEntity(
                    "c6d6ffc6-4ebd-4979-9208-3e12080a791e"
                    ,ItemSpec.getIntOfKind("WATERTANK")
                )

            Then("""I should get one Item with id "WATERTANK" and capacity of 40""")
                val getResult: Try[Option[StoreEntity]] = Await.ready(readFuture, Duration.Inf).value.get
                getResult match {
                    case Success(answer) =>
                        answer match {
                            case Some(storeItemEntity) =>
                                assert(storeItemEntity.quantity == 40)
                            case None => fail("No StoreItemOsName found with id \"WATERTANK\"")
                        }
                    case Failure(e) => log.error("Failure in getStoreEntity ", e)
                }
        }

        scenario("""Get an Item "BASKET_COCONUT" from the Store""") {
            Given("""a StoreEntity with kind "BASKET_COCONUT" is inserted into the Store with ID "c6d6ffc6-4ebd-4979-9208-3e12080a791e" """)
            When("""I get an Item from the store with kind "BASKET_COCONUT" """)
                val getFuture = storeService.getFromStore("c6d6ffc6-4ebd-4979-9208-3e12080a791e", ItemSpec.getIntOfKind("BASKET_COCONUT"), user1.avataruuid )
            Then("""should get the OpenSim Name of the corresponding Object. In this case "AE Basket Coconut" """)
                val result: Try[Option[StoreItemOsName]] = Await.ready(getFuture, Duration.Inf).value.get
                result match {
                    case Success(t) =>
                        t match {
                            case Some ( si ) =>
                                assert(si.osItem.equalsIgnoreCase("AE Basket Coconut"))
                            case None => fail(""""None" returned from getFromStore """)
                        }
                    case Failure(e) => fail("Failure in get StoreItems:", e)
                }
            Then("""verify no SoreEntity of type "BASKET_COCONUT" is available""")
                val verifyFuture = storeService.getStoreEntity("c6d6ffc6-4ebd-4979-9208-3e12080a791e", ItemSpec.getIntOfKind("BASKET_COCONUT"))
                val verifyResult: Try[Option[StoreEntity]] = Await.ready(verifyFuture, Duration.Inf).value.get
                verifyResult match {
                    case Success(t) =>
                        t match {
                            case Some ( _ ) =>
                                fail("""There should be no more "BASKET_COCONUT" in the Store """)
                            case None =>
                                assert(true)
                        }
                    case Failure(e) => fail("Failure in get StoreItems:", e)
                }

        }

        scenario("""Get an Item "WATERTANK" from the Store""") {
            Given("""a StoreEntity with kind "WATERTANK" is inserted into the Store with ID "c6d6ffc6-4ebd-4979-9208-3e12080a791e" """)
            When("""I get an Item from the store with kind "WATERTANK" """)
                val getFuture = storeService.getFromStore("c6d6ffc6-4ebd-4979-9208-3e12080a791e", ItemSpec.getIntOfKind("WATERTANK"), user1.avataruuid )
            Then("""should get the OpenSim Name of the corresponding Object. In this case "AE Watertank" """)
                val result: Try[Option[StoreItemOsName]] = Await.ready(getFuture, Duration.Inf).value.get
                result match {
                    case Success(t) =>
                        t match {
                            case Some ( si ) =>
                                assert(si.osItem.equalsIgnoreCase("AE Watertank"))
                            case None => fail(""""None" returned from getFromStore """)
                        }
                    case Failure(e) => fail("Failure in get StoreItems:", e)
                }
            Then("""verify SoreEntity of type "WATERTANK" has a quantity of 20""")
                val verifyFuture = storeService.getStoreEntity("c6d6ffc6-4ebd-4979-9208-3e12080a791e", ItemSpec.getIntOfKind("WATERTANK"))
                val verifyResult: Try[Option[StoreEntity]] = Await.ready(verifyFuture, Duration.Inf).value.get
                verifyResult match {
                    case Success(t) =>
                        t match {
                            case Some ( se ) =>
                                assert( se.quantity == 20)
                            case None =>
                                fail("""There should be a remaining "WATERTANK" in the Store """)
                    }
                    case Failure(e) => fail("Failure in get StoreItems:", e)
            }

        }


        scenario("delete all StoreEntities of a give Store") {
            Given("""some StoreEntities are inserted into the Store with ID "c6d6ffc6-4ebd-4979-9208-3e12080a791e" """)
            When("""I delete all StoreEntities from the Store with ID "c6d6ffc6-4ebd-4979-9208-3e12080a791e" """)
                val deleteFuture = storeService.deleteStore("c6d6ffc6-4ebd-4979-9208-3e12080a791e")
            Then("should get the number of deleted StoreItems")
                val result: Try[Int] = Await.ready(deleteFuture, Duration.Inf).value.get
                result match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in deleted StoreItems:", e)
                }
        }
    }
}


