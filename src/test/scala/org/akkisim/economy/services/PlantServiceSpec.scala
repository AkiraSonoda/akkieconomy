/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.BaseTestData
import org.akkisim.economy.dataproviders.db.{InventoryEntity, PlantEntity, PlantEntityUpdate}
import org.akkisim.economy.dataproviders.items.{PlantState, PlantUpdate}
import org.scalatest.{BeforeAndAfterAll, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.{Duration, _}
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class PlantServiceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll {
    val log: Logger = LoggerFactory.getLogger("PlantServiceSpec")

    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global
    val plant1: PlantEntity = BaseTestData.plantEntity1
    val plant2: PlantEntity = BaseTestData.plantEntity2
    val inventoryItemPlant1: InventoryEntity = BaseTestData.inventoryItemPlant1

    val plantsService = new PlantService(new DatabaseService())

    override def beforeAll ( ): Unit = {
        val deleteAllFuture = plantsService.deleteAllPlants()
        Await.result(deleteAllFuture, 10.seconds )
    }


    info("As PlantsService User")
    info("I want to be able to create, read, update, delete Plants")
    info("in order to maintain my Plants Inventry")

    feature("A PlantService to manage Plants") {

        scenario("insert a Plant") {

            Given("a Plant is available")
                // See BaseServiceTest
            When("a Plant is created with the PlantService")
                val plantFuture = plantsService.createPlant(plant1)

            Then("the result should be the number of created Plants")
                val result: Try[Int] = Await.ready(plantFuture, Duration.Inf).value.get
                result match {
                    case Success(t) =>
                        assert(t == 1)
                    case Failure(e) =>
                        fail("Failure in createPlant:", e)
                }

            When("the same PlantId with different Data is created again with the PlantService, only the pushurl may change")
                val plantFuture2 = plantsService.createPlant(plant2)
                val result2: Try[Int] = Await.ready(plantFuture2, Duration.Inf).value.get
                result2 match {
                    case Success(t) =>
                        assert(t == 1)
                    case Failure(e) =>
                        fail("Failure in createPlant:", e)
                }

            Then("the GrowthPercent should remain the same")
                val plantFuture3 = plantsService.getPlantById(plant1.id)
                val getResult: Try[Option[PlantEntity]] = Await.ready(plantFuture3, Duration.Inf).value.get
                getResult match {
                    case Success ( item ) =>
                        item match {
                            case Some ( g ) =>
                                assert ( g.id.equalsIgnoreCase ( plant1.id ) )
                                assert ( g.growthpercent.equals ( 100 ) )
                                assert ( g.productioncycles.equals ( 5 ) )
                                assert ( g.pushurl.equalsIgnoreCase(plant2.pushurl))
                            case None => fail ( "Failure reading Plant" )
                        }
                    case Failure ( e ) => log.error ( "Failure in getPlantById", e )
                }
        }
        
        scenario("Read specific Plant") {
            Given("Some Plants are inserted")
            When("I read the Plant with id \"11111111-1111-11e7-9598-0800200d9c66\"")
                val plantFuture = plantsService.getPlantById("11111111-1111-11e7-9598-0800200d9c66")
            Then("I should get one Plant with the id \"11111111-1111-11e7-9598-0800200d9c66\"")
                val result: Try[Option[PlantEntity]] = Await.ready(plantFuture, Duration.Inf).value.get
                result match {
                    case Success(item) =>
                        item match {
                            case Some(g) =>
                                assert(g.id.equalsIgnoreCase("11111111-1111-11e7-9598-0800200d9c66"))
                                assert(g.growthpercent.equals(100))
                                assert(g.productioncycles.equals(5))
                            case None => fail("Failure reading Plant")
                        }
                    case Failure(e) => log.error("Failure in getPlantById", e)
            }
        }

        scenario("Change GrowthPercent of the Plant (plantEntity1)") {
            Given("Some Plants are inserted")
                // See BaseServiceTest
            When("I reduce GrowthPercent by 10")
                val plantUpdate = PlantUpdate(
                    avatarUUID = inventoryItemPlant1.itemownerid,
                    itemType = "PLANT",
                    growthChange = 10,
                    modification = "Subtraction"
                )
                val growthUpdateFuture = plantsService.updatePlantGrowth(plant1.id, plantUpdate)
            Then("I schould get a plant with GrowthPercent 90")
                val result: Try[Option[PlantEntity]] = Await.ready(growthUpdateFuture, Duration.Inf).value.get
                result match {
                    case Success(item) =>
                        item match {
                            case Some(g) =>
                                assert(g.id.equals(plant1.id))
                                assert(g.growthpercent == 90)
                                assert(g.productioncycles == 5)
                                case None => fail("Failure reading Plant")
                        }
                    case Failure(e) => log.error("Failure in getPlantById", e)
                }

        }

        scenario("Change Number of Fruits of the Plant (plantEntity1)") {
            Given("Some Plants are inserted")
            // See BaseServiceTest
            When("I increase the Number of Fruits by 10")
            val plantUpdate = PlantUpdate(
                avatarUUID = inventoryItemPlant1.itemownerid,
                itemType = "PLANT",
                volumeChange = 10,
                modification = "Addition"
            )
            val fruitsUpdateFuture = plantsService.updatePlantVolume(plant1.id, plantUpdate)
            Then("I schould get a plant with GrowthPercent 90")
            val result: Try[Option[PlantEntity]] = Await.ready(fruitsUpdateFuture, Duration.Inf).value.get
            result match {
                case Success(item) =>
                    item match {
                        case Some(g) =>
                            assert(g.id.equals(plant1.id))
                            assert(g.growthpercent == 90)
                            assert(g.productioncycles == 15)
                        case None => fail("Failure reading Plant")
                    }
                case Failure(e) => log.error("Failure in getPlantById", e)
            }

        }

        scenario("Update the whole Plant") {
            Given("Some Plants are inserted")
            // See BaseServiceTest
            When("I chante the plantState to 'PLANT_DEAD'")
            val plantUpdate = PlantEntityUpdate(
                id = plant1.id
                ,planttype = plant1.planttype
                ,plantstate = PlantState.PLANT_DEAD.id
                ,plantstateack = plant1.plantstateack
                ,growthpercent = plant1.growthpercent
                ,maxnofruits = plant1.maxnofruits
                ,productioncycles = plant1.productioncycles
                ,heartbeatcycles = plant1.heartbeatcycles
                ,pushurl = plant1.pushurl
            )
            val plantUpdateFuture = plantsService.updatePlant(plantUpdate)
            Then("I schould get a plant with plantState 'PLANT_DEAD'")
            val result: Try[Option[PlantEntity]] = Await.ready(plantUpdateFuture, Duration.Inf).value.get
            result match {
                case Success(item) =>
                    item match {
                        case Some(g) =>
                            assert(g.id.equals(plant1.id))
                            assert(g.plantstate == PlantState.PLANT_DEAD.id)
                        case None => fail("Failure reading Plant")
                    }
                case Failure(e) => log.error("Failure in getPlantById", e)
            }

        }

        scenario("delete a Plant") {
            Given("a Plant with the ID:"+plant1.id+" is inserted")
            When("I delete the Plant with ID "+plant1.id)
                val plantDeleteFuture = plantsService.deletePlant(id = plant1.id)
            Then("should get the number of deleted Plants")
                val result: Try[Int] = Await.ready(plantDeleteFuture, Duration.Inf).value.get
                result match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in deletePlant:", e)
            }
        }

    }
}


