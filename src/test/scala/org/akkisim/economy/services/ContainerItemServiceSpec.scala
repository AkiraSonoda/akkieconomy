/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import cats.instances.unit
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.economy.dataproviders.db.ContainerItemEntity
import org.akkisim.economy.dataproviders.items.Container
import org.scalatest.{FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class ContainerItemServiceSpec extends FeatureSpec with GivenWhenThen {
    val log: Logger = LoggerFactory.getLogger("ItemServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global
    val itemsService = new ContainerItemsService(new DatabaseService())

    info("As ContainerItemService User")
    info("I want to be able to create, read, update, delete ContainerItems")
    info("in order to manage all my Containers")

    feature("A ContainerItemService to manage ContainerItems") {

        scenario("insert an ContainerItem") {

            Given("a ContainerItem is available")
                val containerItem = ContainerItemEntity(
                    id = "102dba0-4711-11e7-9598-0800200c9a66",
                    maxvolume = 100,
                    currentvolume = 100,
                    measureunit = 5
                )

                val containerItem2 = ContainerItemEntity(
                    id = "102dba0-4711-11e7-9598-0800200c9a66",
                    maxvolume = 500,
                    currentvolume = 100,
                    measureunit = 5
                )

            When("a Container is created with the ItemService")
                val ContainerItemEntityFuture = itemsService.createContainerItem(containerItem)

            Then("the result should be the number of created Container")
                val result: Try[Int] = Await.ready(ContainerItemEntityFuture, Duration.Inf).value.get
                result match {
                    case Success(t) =>
                        assert(t == 1)
                    case Failure(e) =>
                        fail("Failure in createContainerItem:", e)
                }

            When("the same Container is created with the ItemService")
                val ContainerItemEntityFuture2 = itemsService.createContainerItem(containerItem2)
                val result2: Try[Int] = Await.ready(ContainerItemEntityFuture2, Duration.Inf).value.get
                result2 match {
                    case Success(t) =>
                        assert(t == 1)
                    case Failure(e) =>
                        fail("Failure in createContainerItem:", e)
                }

            Then("the MaxVolume should remain the same")
                val ContainerItemFuture = itemsService.getContainerItemById(containerItem.id)
                val getResult: Try[Option[ContainerItemEntity]] = Await.ready(ContainerItemFuture, Duration.Inf).value.get
                getResult match {
                    case Success ( item ) =>
                        item match {
                            case Some ( g ) =>
                                assert ( g.id.equalsIgnoreCase ( containerItem.id ) )
                                assert ( g.maxvolume.equals ( 100 ) )
                                assert ( g.currentvolume.equals ( 100 ) )
                            case None => fail ( "Failure reading ContainerItem" )
                        }
                    case Failure ( e ) => log.error ( "Failure in getPlantById", e )
                }
        }
        
        scenario("Read specific ContainerItem") {
            Given("Some ContainerItems are inserted")
            When("I read the ContainerItem with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                val ContainerItemFuture = itemsService.getContainerItemById("102dba0-4711-11e7-9598-0800200c9a66")
            Then("I should get one ContainerItem with the id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                val result: Try[Option[ContainerItemEntity]] = Await.ready(ContainerItemFuture, Duration.Inf).value.get
                result match {
                    case Success(item) =>
                        item match {
                            case Some(g) =>
                                assert(g.id.equalsIgnoreCase("102dba0-4711-11e7-9598-0800200c9a66"))
                                assert(g.maxvolume.equals(100))
                                assert(g.currentvolume.equals(100))
                            case None => fail("Failure reading ContainerItem")
                        }
                    case Failure(e) => log.error("Failure in getPlantById", e)
            }
        }


        scenario("delete a ContainerItem") {
            Given("a ContainerItem of the id \"102dba0-4711-11e7-9598-0800200c9a66\" is inserted")
            When("I delete the Container with id \"102dba0-4711-11e7-9598-0800200c9a66\"")
                val ContainerItemFuture = itemsService.deleteContainerItem(id = "102dba0-4711-11e7-9598-0800200c9a66")
            Then("should get the number of deleted Cintainers")
                val result: Try[Int] = Await.ready(ContainerItemFuture, Duration.Inf).value.get
                result match {
                    case Success(t) => assert(t == 1)
                    case Failure(e) => fail("Failure in deleteContainerItem:", e)
            }
        }

    }
}


