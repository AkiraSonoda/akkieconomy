/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.db.{ItemSpecEntity, ItemSpecEntityUpdate}
import org.scalatest.{FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class ItemSpecServiceSpec extends FeatureSpec with GivenWhenThen {
    val log: Logger = LoggerFactory.getLogger("ItemSpecServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global

    val itemSpecService = new ItemSpecService(new DatabaseService())


    info("As ItemSpecService User")
    info("I want to be able to create, read, update, delete ItemSpecs")
    info("in order to have a central location to manage all prices of the economy")

    feature("ItemSpecs to manage goods- and service-prices") {

        scenario("insert a ItemSpec") {

            Given("a ItemSpec is available")
            val price = ItemSpecEntity(
                kind = "BASKET_COCONUT"
                , id = 5
                , price = 50
                , storageQuantity = 20
            )

            When("a ItemSpec is created with the ItemSpecService")
            val priceEntityFuture = itemSpecService.createItemSpec(price)

            Then("the result should be the number of created ItemSpecs")
            val result: Try[Int] = Await.ready(priceEntityFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in createItemSpec:", e)
            }
        }


        scenario("Read all ItemSpecs") {
            Given("Some ItemSpecs are inserted")
            When("I read all prices")
            val pricesFuture = itemSpecService.getItemSpec
            Then("I should get a List of ItemSpecs")
            val result: Try[Seq[ItemSpecEntity]] = Await.ready(pricesFuture, Duration.Inf).value.get
            result match {
                case Success(resultset) =>
                    assert(resultset.lengthCompare(1) == 0)
                    resultset.foreach(f = a => {
                        assert ( a.kind.equalsIgnoreCase ( "BASKET_COCONUt" ) )
                    })
                case Failure(e) => fail("failure in getItemSpec:", e)
            }
        }

        scenario("Read specific ItemSpec") {
            Given("Some ItemSpecs are inserted")
            When("I read the ItemSpec with kind \"BASKET_COCONUT\"")
            val pricesFuture = itemSpecService.getItemSpecByKind("BASKET_COCONUT")
            Then("I should get one ItemSpec with price 50")
            val result: Try[Option[ItemSpecEntity]] = Await.ready(pricesFuture, Duration.Inf).value.get
            result match {
                case Success(answer) =>
                    answer match {
                        case Some(price) =>
                            assert(price.price.equals(50))
                        case None => fail("No ItemSpec found with id \"BASKET_COCONUT\"")
                    }
                case Failure(e) => log.error("Failure in getItemSpecByKind ", e)
            }
        }

        scenario("Update a specific ItemSpec") {
            Given("Some ItemSpecs are inserted")
            When("""I update the ItemSpec with kind "BASKET_COCONUT" to price: 100""")
            val priceUpdate = ItemSpecEntityUpdate(
                kind = "BASKET_COCONUT"
                , id = 5
                , price = 100
                , storageQuantity = 20
            )
            val pricesFuture = itemSpecService.updateItemSpec("BASKET_COCONUT",priceUpdate)
            Then("""I should get one ItemSpec with id "BASKET_COCONUT" and price of 100""")
            val result: Try[Option[ItemSpecEntity]] = Await.ready(pricesFuture, Duration.Inf).value.get
            result match {
                case Success(answer) =>
                    answer match {
                        case Some(price) =>
                            assert(price.price.equals(100))
                        case None => fail("""No ItemSpec found with id "BASKET_COCONUT"""")
                    }
                case Failure(e) => log.error("Failure in updateItemSpec ", e)
            }
        }

        scenario("delete a ItemSpec") {
            Given("""a ItemSpec with kind "BASKET_COCONUT" is inserted""")
            When("""I delete the ItemSpec wit kind "BASKET_COCONUT"""")
            val pricesFuture = itemSpecService.deleteItemSpec("BASKET_COCONUT")
            Then("should get the number of deleted ItemSpecs")
            val result: Try[Int] = Await.ready(pricesFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in deleteItemSpec:", e)
            }
        }
    }
}


