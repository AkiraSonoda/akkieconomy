  CREATE TABLE `user` (
  `avatarUUID`       CHAR(36) NOT NULL,
  `avatarName`       VARCHAR(128) NOT NULL,
  `avatarPassword`   VARCHAR(128) NOT NULL,
  `avatarThirst`     INT DEFAULT 0,
  `avatarHunger`     INT DEFAULT 0,
  `avatarLevel`      INT DEFAULT 0,
  PRIMARY KEY (`avatarUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
