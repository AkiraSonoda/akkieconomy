ALTER TABLE `marketinventory`
DROP PRIMARY KEY,
ADD COLUMN `itemType` INT(10) NOT NULL AFTER `ownerId`,
ADD INDEX `index_Id` (`id` ASC),
ADD INDEX `index_OwnerId` (`ownerId` ASC),
ADD INDEX `index_ItemType` (`itemType` ASC);