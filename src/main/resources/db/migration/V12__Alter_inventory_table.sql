DROP INDEX `idx_inventory_itemIdOs` ON `inventory` ;
ALTER TABLE `inventory`
CHANGE COLUMN `itemIdOs` `itemOwnerId` CHAR(36) NOT NULL ;

CREATE INDEX `idx_inventory_itemOwnerId`  ON `inventory` (itemOwnerId) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT ;

