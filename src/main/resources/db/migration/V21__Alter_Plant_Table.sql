ALTER TABLE `plant`
ADD COLUMN `plantStateAck` INT(11) NOT NULL DEFAULT 0 AFTER `plantState`,
CHANGE COLUMN `currNoFruits` `productionCycles` INT(11) NOT NULL DEFAULT '0' ;
