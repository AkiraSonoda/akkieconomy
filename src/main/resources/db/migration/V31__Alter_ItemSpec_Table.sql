ALTER TABLE `itemspec`
ADD COLUMN `growthtime` INT(11) NOT NULL DEFAULT 0 AFTER `storage_quantity`,
ADD COLUMN `cycletime` VARCHAR(45) NOT NULL DEFAULT 0 AFTER `growthtime`,
ADD COLUMN `numcycles` VARCHAR(45) NOT NULL DEFAULT 0 AFTER `cycletime`;
