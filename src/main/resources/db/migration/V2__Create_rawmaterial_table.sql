  CREATE TABLE `rawmaterial` (
  `id` char(36) NOT NULL,
  `ownerId` char(36) NOT NULL,
  `owningRegion` char(36) NOT NULL,
  `kind` VARCHAR(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
