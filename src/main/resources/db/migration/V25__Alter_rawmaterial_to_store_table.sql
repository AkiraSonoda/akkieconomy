ALTER TABLE `rawmaterial`
DROP COLUMN `kind`,
CHANGE COLUMN `id` `storeid` CHAR(36) NOT NULL ,
CHANGE COLUMN `ownerId` `itemid` CHAR(36) NOT NULL ,
CHANGE COLUMN `owningRegion` `quantity` INT(11) NOT NULL , RENAME TO  `store` ;
