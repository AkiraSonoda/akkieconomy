ALTER TABLE `item`
CHANGE COLUMN `kind` `kind` VARCHAR(50) NOT NULL ,
ADD COLUMN `name` VARCHAR(60) NULL AFTER `kind`,
ADD COLUMN `trait` BLOB NULL AFTER `name` ;