  CREATE TABLE `landscape` (
  `id` CHAR(36) NOT NULL,
  `ownerId` CHAR(36) NOT NULL,
  `owningRegion` CHAR(36) NOT NULL,
  `sizeInSquareMeter` INT DEFAULT 62500,
  `kind` VARCHAR(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
