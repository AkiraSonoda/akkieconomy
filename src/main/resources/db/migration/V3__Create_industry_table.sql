  CREATE TABLE `industry` (
  `id` char(36) NOT NULL,
  `ownerId` char(36) NOT NULL,
  `owningRegion` char(36) NOT NULL,
  `kind` varchar(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
