ALTER TABLE `item`
DROP COLUMN `trait`,
DROP COLUMN `name`,
CHANGE COLUMN `ownerId` `maxVolume` INT NOT NULL DEFAULT 0 ,
CHANGE COLUMN `owningRegion` `currentVolume` INT NOT NULL DEFAULT 0 ,
CHANGE COLUMN `kind` `unit` VARCHAR(50) NOT NULL DEFAULT 0 ,
RENAME TO `container_item` ;