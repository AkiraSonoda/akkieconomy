ALTER TABLE `plant`
ADD COLUMN `plantType` INT NOT NULL AFTER `id`,
ADD COLUMN `plantState` INT NOT NULL AFTER `plantType`,
ADD COLUMN `heartbeatCycles` INT(11) NOT NULL DEFAULT 0 AFTER `currNoFruits`;
