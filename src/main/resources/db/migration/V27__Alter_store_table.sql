ALTER TABLE `store`
ADD INDEX `storeid_idx` (`storeid` ASC),
ADD INDEX `itemtype_idx` (`itemtype` ASC),
DROP PRIMARY KEY;
;
