  CREATE TABLE `inventory` (
  `itemId` CHAR(36) NOT NULL,
  `itemIdOs` CHAR(36) NOT NULL,
  `itemName` VARCHAR(80) NOT NULL,
  `itemType` INT DEFAULT 0,
  `itemPrice` INT DEFAULT 0,
  `itemState` INT DEFAULT 0,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
