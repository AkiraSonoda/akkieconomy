ALTER TABLE `inventory`
CHANGE COLUMN `itemType` `itemType` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `itemPrice` `itemPrice` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `itemState` `itemState` INT(11) NOT NULL DEFAULT '0' ;

CREATE UNIQUE INDEX `idx_inventory_itemIdOs`  ON `inventory` (itemIdOs) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
