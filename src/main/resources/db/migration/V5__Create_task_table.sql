  CREATE TABLE `task` (
  `id` CHAR(36) NOT NULL,
  `ownerId` CHAR(36) NOT NULL,
  `customer` CHAR(36) NOT NULL,
  `price` INT DEFAULT 0,
  `transactionId` CHAR(36),
  `daysToComplete` INT DEFAULT 0,
  `kind` VARCHAR(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
