ALTER TABLE `landscape`
DROP COLUMN `kind`,
CHANGE COLUMN `ownerId` `growthPercent` INT(11) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `owningRegion` `maxNoFruits` INT(11) NOT NULL DEFAULT 20 ,
CHANGE COLUMN `sizeInSquareMeter` `currNoFruits` INT(11) NOT NULL DEFAULT 0 , RENAME TO `plant` ;
