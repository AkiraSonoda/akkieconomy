ALTER TABLE `Prices`
ADD COLUMN `id` INT(11) NOT NULL AFTER `kind`,
ADD COLUMN `storage_quantity` INT(11) NOT NULL AFTER `price`, RENAME TO `itemspec` ;
