CREATE TABLE `token` (
  `token`   CHAR(36) NOT NULL,
  `avatarUUID` CHAR(36) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
