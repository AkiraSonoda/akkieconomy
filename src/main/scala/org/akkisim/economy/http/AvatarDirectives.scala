package org.akkisim.economy.http

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.directives.{BasicDirectives, FutureDirectives, HeaderDirectives, RouteDirectives}
import org.akkisim.economy.dataproviders.caches.Central

import scala.util.Success

/**
  * @author Akira Sonoda
  */
trait AvatarDirectives {
    import BasicDirectives._
    import FutureDirectives._
    import HeaderDirectives._
    import RouteDirectives._

    def checkAvatarOnlineState(avatarUUID: String): Directive1[Option[ActorRef]] = {
        Central.userActors.get(avatarUUID) match {
            case Some(actorRef) => provide(Some(actorRef))
            case None => provide(None)
        }
    }




}
