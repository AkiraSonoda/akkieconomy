package org.akkisim.economy.http

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import org.akkisim.economy.http.routes._
import org.akkisim.economy.services._

import scala.concurrent.ExecutionContext

class HttpService( inventoryService: InventoryService,
                   itemService: ContainerItemsService,
                   userService: UserService,
                   authService: AuthService,
                   marketInventoryService: MarketInventoryService,
                   plantService: PlantService,
                   itemSpecService: ItemSpecService,
                   storeService: StoreService
                 )(implicit executionContext: ExecutionContext) /* with AuthServiceRoute */  extends CorsSupport {

    val containerRouter = new ContainerRoute(itemService, inventoryService,marketInventoryService)
    val userServiceRouter = new UserServiceRoute(authService, userService)
    val authServiceRouter = new AuthServiceRoute(authService)
    val vendorRouter = new VendorRoute(inventoryService,marketInventoryService)
    val plantRouter = new PlantRoute(inventoryService, plantService, marketInventoryService)
    val buyingRouter = new BuyingRoute(inventoryService, marketInventoryService,itemSpecService)
    val storeRouter = new StoreRoute(inventoryService, marketInventoryService, storeService)
    val pingRouter = new PingRoute()


    val routes: Route =
        pathPrefix("rest") {
            corsHandler {
                containerRouter.route ~
                userServiceRouter.route ~
                authServiceRouter.route ~
                vendorRouter.route ~
                plantRouter.route ~
                buyingRouter.route ~
                storeRouter.route ~
                pingRouter.route
            }

        }
}
