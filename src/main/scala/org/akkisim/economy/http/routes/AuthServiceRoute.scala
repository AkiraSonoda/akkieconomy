/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.akkisim.economy.http.SecurityDirectives
import org.akkisim.economy.services.AuthService
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.economy.dataproviders.types.LoginPassword

import scala.concurrent.ExecutionContext

class AuthServiceRoute(val authService: AuthService)(implicit executionContext: ExecutionContext) extends FailFastCirceSupport with SecurityDirectives {
    
    import authService._
    
    val route: Route = pathPrefix("auth") {
        path("signIn") {
            pathEndOrSingleSlash {
                post {
                    headerValueByName("pushUrl") { pushUrl =>
                        entity(as[LoginPassword]) { loginPassword =>
                            complete(signIn(
                                avatarUUID = loginPassword.avatarUUID,
                                avatarName = loginPassword.avatarName,
                                password = loginPassword.password,
                                newPassword = loginPassword.newPassword,
                                pushUrl = pushUrl
                            ).map(_.asJson))
                        }
                    }
                }
            }
        }
    }
}
