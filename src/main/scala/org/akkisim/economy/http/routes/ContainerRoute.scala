/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.actor.ActorRef
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.{Directives, Route}
import akka.pattern.ask
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{ContainerItemEntity, InventoryEntity, MarketInventory, MarketInventoryEntity}
import org.akkisim.economy.dataproviders.items.{Container, ContainerUpdate, ItemState}
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.http.{AvatarDirectives, HandlerDirectives}
import org.akkisim.economy.services.{ContainerItemsService, InventoryService, MarketInventoryService}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

/**
  * The Service Route to Items
  *
  * @author Akira Sonoda
  * @param itemsService     The instance of the ContainerItemsService
  * @param inventoryService The instance of the InventoryService
  * @param executionContext The executuionContext
  */
class ContainerRoute ( val itemsService: ContainerItemsService,
                       val inventoryService: InventoryService,
                       val marketInventoryService: MarketInventoryService)
                     ( implicit executionContext: ExecutionContext )
    extends FailFastCirceSupport with Directives with AvatarDirectives with HandlerDirectives {
    val log: Logger = LoggerFactory.getLogger ( "ContainerRoute" )
    implicit val timeout: Timeout = Timeout(5 seconds)

    import inventoryService._
    import itemsService._

    val route: Route = pathPrefix ( "item" ) {
        pathPrefix ( "container" ) {
            post {
                pathEndOrSingleSlash {
                    entity ( as [Container] ) { container =>
                        val ci = createContainerItem ( ContainerItemEntity (
                            id = container.id,
                            maxvolume = container.maxVolume,
                            currentvolume = container.currentVolume,
                            measureunit = MeasureUnit.getMeasureUnit ( container.measureUnit )

                        ) )
                        val ii = createInventoryItem ( InventoryEntity (
                            itemid = container.id,
                            itemownerid = container.ownerid,
                            itemname = container.name,
                            itemtype = ItemSpec.getIntOfKind ( container.itemType ),
                            itemprice = container.price,
                            itemstate = ItemState.NEW.id
                        ) )

                        val mi = marketInventoryService.updateMarketInventoryEntity(
                            id = UUID_ZERO,
                            ownerid = container.ownerid,
                            itemtype = container.itemType,
                            origin = container.origin,
                            MarketInventory(container.id,container.ownerid,container.itemType, container.origin)
                        )

                        val combinedFuture = for (
                            a <- ci;
                            b <- ii;
                            c <- mi
                        ) yield createContainerInt ( a, b, c, container.id )
                        complete ( combinedFuture )
                    }
                }
            } ~
                path ( Segment ) { containerId =>
                    put {
                        entity ( as [ContainerUpdate] ) { containerUpdate =>
                            handleWithGeneric(marketInventoryService.getMarketInventoryItem(
                                    containerId, containerUpdate.avatarUUID, ItemSpec.getIntOfKind(containerUpdate.itemType)
                                )
                            ) {
                                case Some ( _ ) =>
                                    checkAvatarOnlineState ( containerUpdate.avatarUUID ) {
                                        case Some ( ar ) =>
                                            val sc = subtractContainerVolume ( containerId, containerUpdate )
                                            val uu = updateUser ( containerUpdate, ar )

                                            val combinedFuture = {
                                                sc.flatMap ( a => uu.map ( b => updateContainerInt ( a, b ) ) )
                                            }
                                            complete ( combinedFuture )
                                        case None => complete ( StatusCodes.NotFound )
                                    }

                                case None => complete ( StatusCodes.NotFound )
                            }
                        }
                    } ~
                        get {
                                val ci = getContainerItemById ( containerId )
                                val ii = getInventoryItemById ( containerId )

                                val combinedFuture = for (
                                    a <- ci;
                                    b <- ii
                                ) yield createContainerJson ( a, b )
                                complete ( combinedFuture )
                        } ~
                        delete {
                            handleWithGeneric(inventoryService.getInventoryItemById(containerId)) {
                                case Some(inventoryItem) =>
                                    handleWithGeneric(marketInventoryService.getMarketInventoryItem(
                                            id = containerId
                                            , ownerid = inventoryItem.itemownerid
                                            , itemType = inventoryItem.itemtype
                                        )
                                    ) {
                                        case Some( _ ) =>
                                            val ci = itemsService.deleteContainerItem ( containerId )
                                            val ii = inventoryService.deleteInventoryItem ( containerId )
                                            val mi = marketInventoryService.deleteMarketInventoryEntity(containerId)

                                            val combinedFuture = for (
                                                a <- ci;
                                                b <- ii;
                                                c <- mi
                                            ) yield deleteContainerInt ( a, b, c )
                                            complete ( combinedFuture )
                                        case None => complete(StatusCodes.NotFound)
                                    }
                                case None => complete(StatusCodes.NotFound)
                            }
                        }

                    }

        }
    }

    private def updateUser( containerUpdate: ContainerUpdate, userActor: ActorRef ): Future[AvatarStatus] = {
        log.debug("updateUser")
        val stateName = StateName.getStateByName(containerUpdate.stateAffected )
        val stateModification = Modification.getStateModificationByName(containerUpdate.stateModification)
        val userFuture = userActor ? StateUpdate(stateName, containerUpdate.volume, stateModification)
        val responseFuture: Future[AvatarStatus] = userFuture.mapTo[AvatarStatus]
        responseFuture
    }

    private def updateContainerInt( optContainerItem: Option[ContainerItemEntity], avatarState: AvatarStatus ): Json = {
        log.debug("updateContainerInt")
        optContainerItem match {
            case Some(containerItemEntity) => containerItemEntity.asJson
            case None => Json.Null
        }
    }


    /**
      *
      * @param optCi Optional ContainerItemEntity
      * @param optIi Optional InventoryEntity
      * @return
      */
    private def createContainerJson ( optCi: Option[ContainerItemEntity], optIi: Option[InventoryEntity] ): Json = {
        log.debug("createContainerJson")
        optCi match {
            case Some ( ci ) =>
                optIi match {
                    case Some ( ii ) =>
                        val container = Container (
                            id = ii.itemid,
                            ownerid = ii.itemownerid,
                            name = ii.itemname,
                            itemType = ItemSpec.getKindOfInt ( ii.itemtype ),
                            price = ii.itemprice,
                            maxVolume = ci.maxvolume,
                            currentVolume = ci.currentvolume,
                            measureUnit = MeasureUnit.getMeasureUnit ( ci.measureunit )
                        )
                        container.asJson
                    case None => Json.Null
                }
            case None => Json.Null
        }
    }

    /**
      * @param numCi Number of ContainerItemEntity inserted
      * @param numIi Number of InventoryEntity inserted
      * @return
      */
    private def createContainerInt ( numCi: Int, numIi: Int, optMi: Option[MarketInventoryEntity], containerId: String ): StatusCode = {
        log.debug("createContainerInt")
        optMi match {
            case Some( _ ) =>
                if ( numCi == 1 && numIi == 1 ) {
                    StatusCodes.OK
                } else {
                    checkCreateContainerResults(numCi, numIi, containerId)
                }

            case None =>
                checkCreateContainerResults(numCi, numIi, containerId)
        }

    }

    private def checkCreateContainerResults( numCi: Int, numIi: Int, containerId: String): StatusCode = {
        if ( numCi != 1 && numIi == 1 ) { // ContainerItemEntity insert unsuccessful delete InventoryItem
            deleteInventoryItem ( containerId )
            return StatusCodes.NotFound
        }
        if ( numCi == 1 && numIi != 1 ) { // InventoryItem insert unsuccessful delete ContainerItemEntity
            deleteContainerItem ( containerId )
            return StatusCodes.NotFound
        }
        StatusCodes.NotFound // Both inserts unsuccessful

    }

    /**
      * @param numCi Number of ContainerItemEntity deleted
      * @param numIi Number of InventoryEntity deleted
      * @return
      */
    private def deleteContainerInt ( numCi: Int, numIi: Int, numMi: Int): StatusCode = {
        log.debug("deleteVendorInt")
        if ( numCi != 1 || numIi != 1 || numMi != 1) { // ContainerItemEntity insert unsuccessful delete InventoryItem
            log.warn ( "ContainerItemEntity delete: " + numCi + "InventoryItem delete: " + numIi +"MarketInventory delete:" +numMi )
            StatusCodes.MultiStatus
        }
        StatusCodes.OK
    }

}
