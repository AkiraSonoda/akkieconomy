/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.akkisim.economy.http.SecurityDirectives
import org.akkisim.economy.services.{AuthService, UserService}
import org.akkisim.economy.dataproviders.types.PushUrl

import scala.concurrent.ExecutionContext

class UserServiceRoute(val authService: AuthService, userService: UserService)(implicit executionContext: ExecutionContext) extends FailFastCirceSupport with SecurityDirectives {

    import userService._
    
    val route: Route = pathPrefix("user") {
        pathPrefix("me") {
            pathEndOrSingleSlash {
                authenticate { loggedUser =>
                    get {
                        complete(loggedUser)
                    }
                }
            }
        } ~
        pathPrefix("pushurl") {
            pathEndOrSingleSlash {
                authenticate { loggedUser =>
                    put {
                        entity(as[PushUrl]) { url =>
                            complete(pushUrl(loggedUser, url))
                        }
                    }
                }
            }
        }
    }
}
