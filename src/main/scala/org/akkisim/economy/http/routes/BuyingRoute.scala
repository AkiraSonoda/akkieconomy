/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.{Directives, Route}
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{InventoryEntity, MarketInventory, MarketInventoryEntity}
import org.akkisim.economy.dataproviders.items.{ItemState, Trader}
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.http.{AvatarDirectives, HandlerDirectives}
import org.akkisim.economy.services.{InventoryService, ItemSpecService, MarketInventoryService}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * The Service Route to Vendors
  *
  * @param inventoryService       Inventory Service Instance
  * @param marketInventoryService Market Inventory Service Instance
  * @param executionContext       the Execution Context, the program runs in
  */
class BuyingRoute ( val inventoryService: InventoryService
                    , val marketInventoryService: MarketInventoryService
                    , val itemSpecService: ItemSpecService
                  )
                  ( implicit executionContext: ExecutionContext )
    extends FailFastCirceSupport with Directives with AvatarDirectives with HandlerDirectives {
    val log: Logger = LoggerFactory.getLogger ( "BuyingRoute" )
    implicit val timeout: Timeout = Timeout ( 5 seconds )

    import inventoryService._

    val route: Route = pathPrefix ( "item" ) {
        pathPrefix ( "buyer" ) {
            post {
                pathEndOrSingleSlash {
                    rejectEmptyResponse {
                        log.debug("Inserting new BUYER")
                        entity ( as [Trader] ) { buyer =>
                            val ii = createInventoryItem ( InventoryEntity (
                                itemid = buyer.id,
                                itemownerid = buyer.ownerid,
                                itemname = buyer.name,
                                itemtype = ItemSpec.getIntOfKind ( buyer.itemType ),
                                itemprice = buyer.price,
                                itemstate = ItemState.NEW.id
                            ) )

                            val mi = marketInventoryService.updateMarketInventoryEntity (
                                id = UUID_ZERO,
                                ownerid = buyer.ownerid,
                                itemtype = buyer.itemType,
                                origin = buyer.origin,
                                MarketInventory ( buyer.id, buyer.ownerid, buyer.itemType, buyer.origin )
                            )

                            val combinedFuture = for (
                                a <- ii;
                                b <- mi
                            ) yield createBuyerInt ( a, b, buyer.id )
                            complete ( combinedFuture )
                        }
                    }
                }
            } ~
            path ( Segment ) { id =>
                get {
                    log.debug("Getting Trader with Id: " + id)
                    handleWithGeneric ( inventoryService.getInventoryItemById ( id ) ) {
                        case Some ( inventoryItem ) =>
                            handleWithGeneric ( marketInventoryService.getMarketInventoryItem (
                                    id = id
                                    , ownerid = inventoryItem.itemownerid
                                    , itemType = inventoryItem.itemtype
                                )
                            ) {
                                case Some ( _ ) =>
                                    complete ( Trader (
                                        id = inventoryItem.itemid,
                                        ownerid = inventoryItem.itemownerid,
                                        name = inventoryItem.itemname,
                                        itemType = ItemSpec.getKindOfInt ( inventoryItem.itemtype ),
                                        price = inventoryItem.itemprice
                                    ) )
                                case None =>
                                    complete ( StatusCodes.NotFound )
                            }
                        case None => complete ( StatusCodes.NotFound )
                    }
                } ~
                delete {
                    log.debug("Deleting Trader with Id: " + id)
                    handleWithGeneric ( inventoryService.getInventoryItemById ( id ) ) {
                        case Some ( inventoryItem ) =>
                            handleWithGeneric ( marketInventoryService.getMarketInventoryItem (
                                    id = id
                                    , ownerid = inventoryItem.itemownerid
                                    , itemType = inventoryItem.itemtype
                                    )
                            ) {
                                case Some ( _ ) =>
                                    val ii = inventoryService.deleteInventoryItem ( id )
                                    val mi = marketInventoryService.deleteMarketInventoryEntity ( id )

                                    val combinedFuture = for (
                                        a <- ii;
                                        b <- mi
                                    ) yield deleteBuyerInt ( a, b )
                                    complete ( combinedFuture )
                                case None => complete ( StatusCodes.NotFound )
                            }
                        case None => complete ( StatusCodes.NotFound )
                     }
                }
            } ~
            pathPrefix("price") {
                path ( Segment ) { kind =>
                    get {
                        pathEndOrSingleSlash {
                            log.debug("Prices requestet for:" +kind )
                            rejectEmptyResponse {
                                complete(itemSpecService.getPriceByKind(kind))
                            }
                        }
                    }
                }
            } ~
            path ( Segments ( 3 ) ) { segs =>
                // Buying things
                // seg(0) = Id of the called MARKET_BUYER
                // seg(1) = OwnerID of the called MARKET_BUYER
                // seg(3) = origin of the called MARKET_BUYER
                log.debug("3 Segments seg(0): " + segs.head + " - seg(1): " + segs(1) + " - seg(2): " + segs(2) )
                put {
                    pathEndOrSingleSlash {
                        rejectEmptyResponse {
                            entity ( as [Trader] ) { trader =>
                                log.debug ( "Trader :" + trader.id )
                                handleWithGeneric (
                                    marketInventoryService.getUniqueMarketInventoryItem (
                                        id = UUID_ZERO
                                        , ownerid = trader.ownerid
                                        , itemType = ItemSpec.getIntOfKind( trader.itemType )
                                        , origin = trader.origin
                                    )
                                ) {
                                    case Some ( mi ) =>
                                        complete ( marketInventoryService.deleteMarketInventoryEntity ( mi.id ) )
                                    case None =>
                                        complete ( StatusCodes.NotFound )
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /**
      * @param numIi Number of InventoryEntity inserted
      * @param optMi Option of MarketInventoryEntity
      * @return
      */
    private def createBuyerInt ( numIi: Int, optMi: Option[MarketInventoryEntity], containerId: String ): Option[String] = {
        log.debug ( "createBuyerInt" )
        optMi match {
            case Some ( mi ) =>
                if ( numIi == 1 ) {
                    Some(mi.ownerid)
                } else {
                    None
                }

            case None =>
                if ( numIi == 1 ) {
                    deleteInventoryItem ( containerId )
                    None
                } else {
                    None
                }

        }

    }

    /**
      * @param numIi Number of InventoryEntity deleted has to be 1
      * @param numMi Number of MarketInventoryEntity deleted has to be 1
      * @return
      */
    private def deleteBuyerInt ( numIi: Int, numMi: Int ): StatusCode = {
        log.debug ( "deleteBuyerInt" )
        if ( numIi != 1 || numMi != 1 ) { // ContainerItemEntity insert unsuccessful delete InventoryItem
            log.warn ( "InventoryItem delete: " + numIi + "MarketInventory delete:" + numMi )
            StatusCodes.NotFound
        } else {
            StatusCodes.OK
        }
    }

}
