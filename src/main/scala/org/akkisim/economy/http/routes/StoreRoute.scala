/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{InventoryEntity, MarketInventory, MarketInventoryEntity, StoreEntity}
import org.akkisim.economy.dataproviders.items.{ItemState, StoreItem, Trader}
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.http.{AvatarDirectives, HandlerDirectives}
import org.akkisim.economy.services.{InventoryService, MarketInventoryService, StoreService}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps


/**
  * The Service Route to Vendors
  *
  * @param inventoryService       Inventory Service Instance
  * @param marketInventoryService Market Inventory Service Instance
  * @param executionContext       the Execution Context, the program runs in
  */
class StoreRoute ( val inventoryService: InventoryService
                   , val marketInventoryService: MarketInventoryService
                   , val storeService: StoreService
                  )
                 ( implicit executionContext: ExecutionContext )
    extends FailFastCirceSupport with Directives with AvatarDirectives with HandlerDirectives {
    val log: Logger = LoggerFactory.getLogger ( "StoreRoute" )
    implicit val timeout: Timeout = Timeout ( 5 seconds )

    import inventoryService._

    val route: Route = pathPrefix ( "item" ) {
        pathPrefix ( "store" ) {
            post {
                pathEndOrSingleSlash {
                    rejectEmptyResponse {
                        log.debug("Inserting new STORE")
                        entity ( as [Trader] ) { store =>
                            val ii = createInventoryItem ( InventoryEntity (
                                itemid = store.id,
                                itemownerid = store.ownerid,
                                itemname = store.name,
                                itemtype = ItemSpec.getIntOfKind( store.itemType ),
                                itemprice = store.price,
                                itemstate = ItemState.NEW.id
                            ) )

                            val mi = marketInventoryService.updateMarketInventoryEntity (
                                id = UUID_ZERO,
                                ownerid = store.ownerid,
                                itemtype = store.itemType,
                                origin = store.origin,
                                MarketInventory ( store.id, store.ownerid, store.itemType, store.origin )
                            )

                            val combinedFuture = for (
                                a <- ii;
                                b <- mi
                            ) yield createStoreInt ( a, b, store.id )
                            complete ( combinedFuture )
                        }
                    }
                }
            } ~
            path ( Segment ) { storeId =>
                put {
                    entity( as [StoreItem] ) { storeItem =>
                        log.debug ( "Inserting StoreItemOsName: "+ storeItem.itemtype+ " into Store: " + storeId )
                        handleWithGeneric ( inventoryService.getInventoryItemById ( storeId ) ) {
                            case Some ( inventoryItem ) =>
                                handleWithGeneric(marketInventoryService.getUniqueMarketInventoryItem( // check the existence of the Marketinventory
                                    UUID_ZERO
                                    , inventoryItem.itemownerid
                                    , ItemSpec.getIntOfKind(storeItem.itemtype)
                                    , UUID_ZERO)) {
                                    case Some(_) =>
                                        handleWithGeneric ( storeService.getStoreById ( storeId ) ) {
                                            items =>
                                                if ( checkStorage ( items, inventoryItem.itemtype, ItemSpec.getIntOfKind ( storeItem.itemtype ) ) ) {
                                                    handleWithGeneric(storeService.addStoreEntity ( storeId, storeItem )) {
                                                        case 1 =>
                                                            handleWithGeneric(marketInventoryService.deleteUniqueMarketInventoryItem(
                                                                UUID_ZERO, inventoryItem.itemownerid,
                                                                ItemSpec.getIntOfKind(storeItem.itemtype), UUID_ZERO)) {
                                                                    case 1 => complete(StatusCodes.OK)
                                                            }
                                                    }
                                                } else {
                                                    complete ( StatusCodes.InsufficientStorage )
                                                }
                                        }
                                    case None =>
                                        complete ( StatusCodes.NotFound )
                                }
                            case None =>
                                complete ( StatusCodes.NotFound )
                        }
                    }
                }
            } ~
            path ( Segments(3) ) { segs =>
                // segs.head = id of the store
                // segs(1) = itemType
                // segs(3) = uuid of owner
                get {
                    rejectEmptyResponse {
                        log.debug ( "Getting StoreItemOsName: " +  segs(1)  + " from Store: " + segs.head + " of owner: " + segs(2))
                        handleWithGeneric ( inventoryService.getInventoryItemById ( segs.head ) ) {
                            case Some ( inv ) =>
                                if (!inv.itemownerid.equalsIgnoreCase(segs(2))) {
                                    complete( StatusCodes.NotFound)
                                } else {
                                    complete ( storeService.getFromStore ( segs.head, ItemSpec.getIntOfKind( segs(1)), inv.itemownerid ) )
                                }
                            case None =>
                                complete ( StatusCodes.NotFound )
                        }
                    }
                }
            }
//                delete {
//                    log.debug("Deleting Trader with Id: " + id)
//                    handleWithGeneric ( inventoryService.getInventoryItemById ( id ) ) {
//                        case Some ( inventoryItem ) =>
//                            handleWithGeneric ( marketInventoryService.getMarketInventoryItem (
//                                    id = id
//                                    , ownerid = inventoryItem.itemownerid
//                                    , itemType = inventoryItem.itemtype
//                                    )
//                            ) {
//                                case Some ( _ ) =>
//                                    val ii = inventoryService.deleteInventoryItem ( id )
//                                    val mi = marketInventoryService.deleteMarketInventoryEntity ( id )
//
//                                    val combinedFuture = for (
//                                        a <- ii;
//                                        b <- mi
//                                    ) yield deleteBuyerInt ( a, b )
//                                    complete ( combinedFuture )
//                                case None => complete ( StatusCodes.NotFound )
//                            }
//                        case None => complete ( StatusCodes.NotFound )
//                     }
//                }
//            } ~
//            path ( Segments ( 3 ) ) { segs =>
//                // Buying things
//                // seg(0) = Id of the called MARKET_BUYER
//                // seg(1) = OwnerID of the called MARKET_BUYER
//                // seg(3) = origin of the called MARKET_BUYER
//                log.debug("3 Segments seg(0): " + segs.head + " - seg(1): " + segs(1) + " - seg(2): " + segs(2) )
//                put {
//                    pathEndOrSingleSlash {
//                        rejectEmptyResponse {
//                            entity ( as [Trader] ) { trader =>
//                                log.debug ( "Trader :" + trader.id )
//                                handleWithGeneric (
//                                    marketInventoryService.getUniqueMarketInventoryItem (
//                                        id = UUID_ZERO
//                                        , ownerid = trader.ownerid
//                                        , itemType = InventoryItemType.getInventoryItemType ( trader.itemType )
//                                        , origin = trader.origin
//                                    )
//                                ) {
//                                    case Some ( mi ) =>
//                                        complete ( marketInventoryService.deleteMarketInventoryEntity ( mi.id ) )
//                                    case None =>
//                                        complete ( StatusCodes.NotFound )
//                                }
//                            }
//                        }
//                    }
//                }
//            }
        }
    }


    /**
      * @param numIi Number of InventoryEntity inserted
      * @param optMi Option of MarketInventoryEntity
      * @return
      */
    private def createStoreInt ( numIi: Int, optMi: Option[MarketInventoryEntity], containerId: String ): Option[String] = {
        log.debug ( "createStoreInt" )
        optMi match {
            case Some ( mi ) =>
                if ( numIi == 1 ) {
                    Some(mi.ownerid)
                } else {
                    None
                }

            case None =>
                if ( numIi == 1 ) {
                    deleteInventoryItem ( containerId )
                    None
                } else {
                    None
                }

        }

    }

    /** Check the Store Size and Usage
      *
      * @param items the items of the store identified by storeID
      * @param storeItemType the itemType of the Store
      * @param storageItemType the itemType of the Item to be added to the Store
      * @return true -> item can be inserted or false -> item can not be inserted
      */
    private def checkStorage(items: Seq[StoreEntity], storeItemType: Int, storageItemType: Int): Boolean = {
        var enoughStorage = true
        var found = false

        items.foreach ( item =>
            if ( item.itemtype == storageItemType ) found = true
        )

        val capacity = ItemSpec.getCapacity ( storeItemType )
        if ( !found ) {
            if (capacity == -1) {
                enoughStorage = false
            } else {
                if (items.size == capacity) enoughStorage = false
            }
        }

        enoughStorage
    }

}
