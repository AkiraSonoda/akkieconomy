/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.{Directives, Route}
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.items._
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.http.{AvatarDirectives, HandlerDirectives}
import org.akkisim.economy.services.{InventoryService, MarketInventoryService, PlantService}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * The Service Route to Vendors
  *
  * @param inventoryService       Inventory Service Instance
  * @param marketInventoryService Market Inventory Service Instance
  * @param executionContext       the Execution Context, the program runs in
  */
class PlantRoute ( val inventoryService: InventoryService,
                   val plantService: PlantService,
                   val marketInventoryService: MarketInventoryService )
                 ( implicit executionContext: ExecutionContext )
    extends FailFastCirceSupport with Directives with AvatarDirectives with HandlerDirectives {
    val log: Logger = LoggerFactory.getLogger ( "PlantRoute" )
    implicit val timeout: Timeout = Timeout ( 5 seconds )

    import inventoryService._
    import plantService._

    val route: Route = pathPrefix ( "item" ) {
        pathPrefix ( "plant" ) {
            post {
                pathEndOrSingleSlash {
                    entity ( as [Plant] ) { plant =>
                        val plantEnt = createPlantEntity(plant)
                        val ii = createInventoryItem ( InventoryEntity (
                            itemid = plant.id,
                            itemownerid = plant.ownerid,
                            itemname = plant.name,
                            itemtype = ItemSpec.getIntOfKind ( plant.itemType ),
                            itemprice = plant.price,
                            itemstate = ItemState.NEW.id
                        ) )

                        val pi = createPlant( plantEnt )

                        val mi = marketInventoryService.updateMarketInventoryEntity (
                            id = UUID_ZERO,
                            ownerid = plant.ownerid,
                            itemtype = plant.itemType,
                            origin = plant.origin,
                            MarketInventory ( plant.id, plant.ownerid, plant.itemType, plant.origin )
                        )

                        val combinedFuture = for (
                            a <- ii;
                            b <- pi;
                            c <- mi
                        ) yield createPlantInt ( a, b, c, plant.id, plant.pushurl )
                        complete ( combinedFuture )
                    }
                }
            } ~
            path ( Segment ) { plantId =>
                get {
                    val pi = getPlantById( plantId )
                    val ii = getInventoryItemById ( plantId )

                    val combinedFuture = for (
                        a <- pi;
                        b <- ii
                    ) yield createPlantJson ( a, b )
                    complete ( combinedFuture )
                } ~
                delete {
                    log.debug("Delete Plant: " + plantId)
                    handleWithGeneric(inventoryService.getInventoryItemById(plantId)) {
                        case Some(inventoryItem) =>
                            handleWithGeneric(marketInventoryService.getMarketInventoryItem(
                                    id = plantId
                                    , ownerid = inventoryItem.itemownerid
                                    , itemType = inventoryItem.itemtype
                                )
                            ) {
                                    case Some( _ ) =>
                                        val pi = plantService.deletePlant( plantId )
                                        val ii = inventoryService.deleteInventoryItem ( plantId )
                                        val mi = marketInventoryService.deleteMarketInventoryEntity(plantId)

                                        val combinedFuture = for (
                                            a <- pi;
                                            b <- ii;
                                            c <- mi
                                        ) yield deletePlantInt( a, b, c )
                                        complete ( combinedFuture )
                                    case None => complete(StatusCodes.NotFound)
                                }
                        case None => complete(StatusCodes.NotFound)
                    }
                } ~
                put {
                    entity ( as [MarketInventory] ) { marketInventoryItem =>
                        log.debug ("PlantID: " + plantId)
                        handleWithGeneric ( inventoryService.getInventoryItemById(plantId) ) { // Check the Plants Status in the Inventory
                            case Some ( _ ) =>
                                complete ( marketInventoryService.createMarketInventoryEntity ( marketInventoryItem ) )
                            case None =>
                                complete ( StatusCodes.NotFound )
                        }
                    }
                }
            }
        }
    }

    private def createPlantEntity(plant: Plant): PlantEntity = {
        log.debug("createPlantEntity")
        val plantType = ItemSpec.getIntOfKind(plant.plantType)
        val plantState = PlantState.getPlantStateStateValueByString(plant.plantState)
        val maxFruits = ItemSpec.getCapacity(plant.plantType)
        var growth = 0

        plantState match {
            case PlantState.PLANT_GROWING => growth = 0
            case PlantState.PLANT_PRODUCING_FRUITS => growth = 100
            case _ => growth = 0
        }

        PlantEntity (
            id = plant.id
            , planttype = plantType
            , plantstate = plantState.id
            , growthpercent = growth
            , maxnofruits = maxFruits
            , pushurl = plant.pushurl
        )

    }


    /**
      *
      * @param optPi Optional PlantEntity
      * @param optIi Optional InventoryEntity
      * @return
      */
    private def createPlantJson ( optPi: Option[PlantEntity], optIi: Option[InventoryEntity] ): Json = {
        log.debug("createPlantJson")
        optPi match {
            case Some ( pi ) =>
                optIi match {
                    case Some ( ii ) =>
                        val plant = Plant (
                            id = ii.itemid,
                            ownerid = ii.itemownerid,
                            name = ii.itemname,
                            itemType = ItemSpec.getKindOfInt ( ii.itemtype ),
                            price = ii.itemprice,
                            plantState = PlantState.getPlantStateString(pi.plantstate),
                            plantType = ItemSpec.getKindOfInt(pi.planttype),
                            pushurl = pi.pushurl
                        )
                        plant.asJson
                    case None => Json.Null
                }
            case None => Json.Null
        }
    }

    /**
      * @param numIi Number of InventoryEntity inserted
      * @param optMi Option of MarketInventoryEntity
      * @return
      */
    private def createPlantInt ( numIi: Int, numPi: Int,optMi: Option[MarketInventoryEntity], plantId: String, pushUrl: String ): StatusCode = {
        log.debug ( "createPlantInt" )
        optMi match {
            case Some( mi ) =>
                if ( numPi == 1 && numIi == 1 ) {
                    // if there is already a PlantActor running, we send the new pushurl
                    val optPlantActorRef = Central.plantActors.get(plantId)
                    optPlantActorRef match {
                        case Some(plantActorRef) =>
                            plantActorRef ! PlantPushUrl(pushUrl)
                        case None => // Register a new PlantActor
                            Central.optPlantFactory match {
                                case Some(plantFactory) =>
                                    plantFactory ! RegisterPlantOfUser(mi.ownerid)
                                case None => log.error("No PlantFactory found to Register")
                            }
                    }
                    StatusCodes.OK
                } else {
                    checkCreatePlantResults(numPi, numIi, plantId)
                }
            case None =>
                checkCreatePlantResults(numPi, numIi, plantId)
        }

    }

    private def checkCreatePlantResults( numPi: Int, numIi: Int, plantId: String): StatusCode = {
        numPi match {
            case 1 => deletePlant( plantId )
            case num =>
                if (num != 0) {
                    log.error("Number of inserted Plants should actually be 0. Got antother number: " +numPi)
                }
        }
        numIi match  {
            case 1 => deleteInventoryItem( plantId )
            case num =>
                if (num != 0) {
                    log.error("Number of inserted Plants should actually be 0. Got antother number: " +numPi)
                }
        }

        StatusCodes.NotFound // Both inserts unsuccessful

    }

    /**
      * @param numPi Number of PlantEntity deleted has to be 1
      * @param numIi Number of InventoryEntity deleted has to be 1
      * @param numMi Number of MarketInventoryEntity deleted has to be 1
      * @return
      */
    private def deletePlantInt ( numPi: Int, numIi: Int, numMi: Int ): StatusCode = {
        log.debug ( "deletePlantInt" )
        if ( numPi != 1 || numIi != 1 || numMi != 1 ) { // ContainerItemEntity insert unsuccessful delete InventoryItem
            log.warn ( "Plant delete: " + numPi + " InventoryItem delete: " + numIi + " MarketInventory delete:" + numMi )
            StatusCodes.MultiStatus
        } else {
            StatusCodes.OK
        }
    }

}
