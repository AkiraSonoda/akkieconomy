/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.http.routes

import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.{Directives, Route}
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{InventoryEntity, MarketInventory, MarketInventoryEntity}
import org.akkisim.economy.dataproviders.items.{ItemState, Trader}
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.http.{AvatarDirectives, HandlerDirectives}
import org.akkisim.economy.services.{InventoryService, MarketInventoryService}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * The Service Route to Vendors
  *
  * @param inventoryService       Inventory Service Instance
  * @param marketInventoryService Market Inventory Service Instance
  * @param executionContext       the Execution Context, the program runs in
  */
class VendorRoute ( val inventoryService: InventoryService
                    , val marketInventoryService: MarketInventoryService
                  )
                  ( implicit executionContext: ExecutionContext )
    extends FailFastCirceSupport with Directives with AvatarDirectives with HandlerDirectives {
    val log: Logger = LoggerFactory.getLogger ( "VendorRoute" )
    implicit val timeout: Timeout = Timeout ( 5 seconds )

    import inventoryService._

    val route: Route = pathPrefix ( "item" ) {
        pathPrefix ( "vendor" ) {
            post {
                pathEndOrSingleSlash {
                    rejectEmptyResponse {
                        log.debug("Inserting new Trader")
                        entity ( as [Trader] ) { vendor =>
                            val ii = createInventoryItem ( InventoryEntity (
                                itemid = vendor.id,
                                itemownerid = vendor.ownerid,
                                itemname = vendor.name,
                                itemtype = ItemSpec.getIntOfKind ( vendor.itemType ),
                                itemprice = vendor.price,
                                itemstate = ItemState.NEW.id
                            ) )

                            val mi = marketInventoryService.updateMarketInventoryEntity (
                                id = UUID_ZERO,
                                ownerid = vendor.ownerid,
                                itemtype = vendor.itemType,
                                origin = vendor.origin,
                                MarketInventory ( vendor.id, vendor.ownerid, vendor.itemType, vendor.origin )
                            )

                            val combinedFuture = for (
                                a <- ii;
                                b <- mi
                            ) yield createVendorInt ( a, b, vendor.id )
                            complete ( combinedFuture )
                        }
                    }
                }
            } ~
            path ( Segment ) { id =>
                get {
                    log.debug("Getting Trader with Id: " + id)
                    handleWithGeneric ( inventoryService.getInventoryItemById ( id ) ) {
                        case Some ( inventoryItem ) =>
                            handleWithGeneric ( marketInventoryService.getMarketInventoryItem (
                                    id = id
                                    , ownerid = inventoryItem.itemownerid
                                    , itemType = inventoryItem.itemtype
                                )
                            ) {
                                case Some ( _ ) =>
                                    complete ( Trader (
                                        id = inventoryItem.itemid,
                                        ownerid = inventoryItem.itemownerid,
                                        name = inventoryItem.itemname,
                                        itemType = ItemSpec.getKindOfInt ( inventoryItem.itemtype ),
                                        price = inventoryItem.itemprice
                                    ) )
                                case None =>
                                    complete ( StatusCodes.NotFound )
                            }
                        case None => complete ( StatusCodes.NotFound )
                    }
                } ~
                delete {
                    log.debug("Deleting Trader with Id: " + id)
                    handleWithGeneric ( inventoryService.getInventoryItemById ( id ) ) {
                        case Some ( inventoryItem ) =>
                            handleWithGeneric ( marketInventoryService.getMarketInventoryItem (
                                    id = id
                                    , ownerid = inventoryItem.itemownerid
                                    , itemType = inventoryItem.itemtype
                                    )
                            ) {
                                case Some ( _ ) =>
                                    val ii = inventoryService.deleteInventoryItem ( id )
                                    val mi = marketInventoryService.deleteMarketInventoryEntity ( id )

                                    val combinedFuture = for (
                                        a <- ii;
                                        b <- mi
                                    ) yield deleteVendorInt ( a, b )
                                    complete ( combinedFuture )
                                case None => complete ( StatusCodes.NotFound )
                            }
                        case None => complete ( StatusCodes.NotFound )
                     }
                } ~
                post { // Only Logged Users can register new Vendors
                    log.debug("Registering a vendor for: " + id)
                    entity(as [MarketInventory]) { marketInventory =>
                        val optUser = Central.userActors.get(id) // Only logged in Users can register a new Trader
                        optUser match {
                            case Some( _ ) =>
                                complete(marketInventoryService.createMarketInventoryEntity(marketInventory))
                            case None =>
                                complete(StatusCodes.NotFound)
                        }
                    }
                }
            } ~
            path ( Segments ( 4 ) ) { segs =>
                // Inquiry about an Item
                // seg(0) = Id of the Item
                // seg(1) = OwnerID of the Item
                // seg(2) = ItemType of the Item
                // seg(3) = origin = id of the Items Vendor
                log.debug ( "4 Segments seg(0): " + segs.head + " - seg(1): " + segs ( 1 ) + " - seg(2): " + segs ( 2 ) + " - seg(3): " + segs(3) )
                get {
                    pathEndOrSingleSlash {
                        complete ( marketInventoryService.getUniqueMarketInventoryItem (
                                segs.head
                                , segs ( 1 )
                                , ItemSpec.getIntOfKind ( segs (2) )
                                , segs ( 3 )
                            )
                        )
                    }
                }
            } ~
            path ( Segments ( 3 ) ) { segs =>
                // Registered Vendors can Insert new Items or delete existing Items
                // seg(0) = Id of the called VENDOR
                // seg(1) = OwnerID of the called VENDOR
                // seg(3) = origin of the called VENDOR
                put {
                    log.debug("a Trader registers a new Item id: " + segs.head + " - ownerid: " + segs(1) + " - origin: " + segs(2) )
                    entity ( as [MarketInventory] ) { marketInventoryItem =>
                        handleWithGeneric ( marketInventoryService.getUniqueMarketInventoryItem ( // Check the Vendors Status in the MarketInventory
                            id = segs.head
                            , ownerid = segs(1)
                            , itemType = ItemSpec.getIntOfKind ( "VENDOR" )
                            , origin = segs(2)
                            )
                        ) {
                            case Some ( _ ) =>
                                complete ( marketInventoryService.createMarketInventoryEntity ( marketInventoryItem ) )
                            case None =>
                                complete ( StatusCodes.NotFound )
                        }
                    }
                } ~
                delete {
                    log.debug("a Trader is deleted id: " + segs.head + " - and informs> ItemType: " + segs(1) + " - ItemUUID: " + segs(2) )
                    val itemType = segs(1)
                    itemType match {
                        case "PLANT" =>
                            log.debug("Sending a Message to the PlantActor and Deleting the Trader")
                            Central.plantActors.get(segs(2)) match {
                                case Some(plantActor ) => plantActor ! StateChange
                                case None =>
                                    log.error("Actually we should have a Plant Actor. Check the log")
                            }
                            val ii = inventoryService.deleteInventoryItem ( segs.head )
                            val mi = marketInventoryService.deleteMarketInventoryEntity ( segs.head )

                            val combinedFuture = for (
                                a <- ii;
                                b <- mi
                            ) yield deleteVendorInt ( a, b )
                            complete ( combinedFuture )
                        case _ =>
                            log.error("Actually only ItemType Plant have something to do")
                            complete( StatusCodes.NotFound)
                    }
                }
            }
        }
    }


    /**
      * @param numIi Number of InventoryEntity inserted
      * @param optMi Option of MarketInventoryEntity
      * @return
      */
    private def createVendorInt ( numIi: Int, optMi: Option[MarketInventoryEntity], containerId: String ): Option[String] = {
        log.debug ( "createVendorInt" )
        optMi match {
            case Some ( mi ) =>
                if ( numIi == 1 ) {
                    Some(mi.ownerid)
                } else {
                    None
                }

            case None =>
                if ( numIi == 1 ) {
                    deleteInventoryItem ( containerId )
                    None
                } else {
                    None
                }

        }

    }

    /**
      * @param numIi Number of InventoryEntity deleted has to be 1
      * @param numMi Number of MarketInventoryEntity deleted has to be 1
      * @return
      */
    private def deleteVendorInt ( numIi: Int, numMi: Int ): StatusCode = {
        log.debug ( "deleteVendorInt" )
        if ( numIi != 1 || numMi != 1 ) { // ContainerItemEntity insert unsuccessful delete InventoryItem
            log.warn ( "InventoryItem delete: " + numIi + "MarketInventory delete:" + numMi )
            StatusCodes.NotFound
        } else {
            StatusCodes.OK
        }
    }

}
