/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.util.Timeout
import org.akkisim.economy.EconomyServer.networkCallTimeout
import org.akkisim.economy.actors.UserActor
import org.akkisim.economy.dataproviders.caches.Central
import org.akkisim.economy.dataproviders.db.{TokenEntity, TokenEntityTable, UserEntity}
import org.akkisim.economy.dataproviders.items.SignInResult
import org.akkisim.economy.dataproviders.types._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

/**
  * @author Akira Sonoda
  */
class AuthService(val databaseService: DatabaseService, userService: UserService, system: ActorSystem)(implicit executionContext: ExecutionContext) extends TokenEntityTable {
    val log: Logger = LoggerFactory.getLogger("MoneyServer")
    
    import databaseService.driver.api._
    implicit val timeout: Timeout = Timeout(networkCallTimeout seconds)
    
    def signIn(avatarUUID: String, avatarName: String, password: String, newPassword: String, pushUrl: String): Future[Option[SignInResult]] = {
        db.run(User.filter(u => u.avataruuid === avatarUUID).result).flatMap { users =>
            users.find(user => user.avatarpassword == password) match {
                case Some(user) =>
                    val userThirst = getAdjustedThirst(user.avatarthirst)
                    val userHunger = getAdjustedHunger(user.avatarhunger)
                    log.debug("Thirst: " +userThirst+ " Hunger:" + userHunger )
                    notifyCentral(userService = userService, avatarUUID = avatarUUID,pushUrl = pushUrl)
                    db.run(Token.filter(_.avataruuid === user.avataruuid).result.headOption).flatMap {
                        // TODO new password change handling
                        case Some(token) =>
                            Future.successful(Some(
                                SignInResult(token.token, token.avataruuid, userThirst, userHunger)
                            ))
                        case None        => createToken(user).flatMap {
                            case Some(token) =>
                                Future.successful(Some(
                                    SignInResult(token.token, token.avataruuid, userThirst, userHunger)
                                ))
                            case None => Future.successful(None)
                        }
                    }
                case None =>
                    userService.createUser(avatarUUID = avatarUUID, avatarName = avatarName, avatarPassword = password).flatMap {
                        case Some(user) =>
                            val userThirst = getAdjustedThirst(user.avatarthirst)
                            val userHunger = getAdjustedHunger(user.avatarhunger)
                            log.debug("Thirst: " +userThirst+ " Hunger:" + userHunger )
                            notifyCentral(userService = userService, avatarUUID = avatarUUID,pushUrl = pushUrl)
                            db.run(Token.filter(_.avataruuid === user.avataruuid).result.headOption).flatMap {
                                case Some(token) =>
                                    Future.successful(Some(
                                        SignInResult(token.token, token.avataruuid, userThirst, userHunger)
                                    ))
                                case None => createToken(user).flatMap {
                                    case Some(token) =>
                                        Future.successful(Some(
                                            SignInResult(token.token, token.avataruuid, userThirst, userHunger)
                                        ))
                                    case None => Future.successful(None)
                                }
                            }
                        case None =>
                            Future.successful(None)
                    }
            }
        }
    }

    def authenticate(token: String): Future[Option[UserEntity]] =
        db.run((for {
            token <- Token.filter(_.token === token)
            user <- User.filter(_.avataruuid === token.avataruuid)
        } yield user).result.headOption)

    def createToken(user: UserEntity): Future[Option[TokenEntity]] = {
        db.run(Token += TokenEntity(avataruuid = user.avataruuid)).flatMap {
            case 1 => db.run(Token.filter(_.avataruuid === user.avataruuid).result.headOption)
            case _ => Future.successful(None)
        }
    }
    def deleteToken(avatarUUID: String): Future[Int] = db.run(Token.filter(_.avataruuid === avatarUUID).delete)
 
    private def notifyCentral(userService: UserService, avatarUUID: String, pushUrl: String): Unit = {
        var optUserActor = Central.userActors.get(avatarUUID)
        optUserActor match {
            case Some ( userActorRef ) =>
                userActorRef ! RegisterPushUrl ( avatarUUID, pushUrl )
            case None =>
                Central.optCentral match {
                    case Some ( central ) =>
                        val register = system.actorOf ( Props ( new UserActor ( userService, avatarUUID, pushUrl, central ) ), avatarUUID )
                        central ! RegisterUser ( avatarUUID, register )
                        register ! SendHeartbeat
                    case None => log.error ( "No Central Actor to notify" )
                }
        }
    }

    private def getAdjustedThirst(int: Option[Int]): Int = {
        int match {
            case Some(thirst) => thirst/THiRST_DIVIDER
            case None => 0
        }
    }

    private def getAdjustedHunger(int: Option[Int]): Int = {
        int match {
            case Some(hunger) => hunger/HUNGER_DIVIDER
            case None => 0
        }
    }

}