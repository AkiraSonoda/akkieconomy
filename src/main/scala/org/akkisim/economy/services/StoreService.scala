/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.items.{StoreItem, StoreItemOsName}
import org.akkisim.economy.dataproviders.types.UUID_ZERO
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.postfixOps


/**
  * @author Akira Sonoda
  */
class StoreService ( val databaseService: DatabaseService)( implicit executionContext: ExecutionContext) extends StoreEntityTable {
    val log: Logger = LoggerFactory.getLogger ( "StoreService" )

    import databaseService.driver.api._

    /** Creates a new StoreItemOsName if it does not already exist. If it exists only the quantity will be added to the existing Store Item
      *
      * @param store the StoreItemOsName to insert
      * @return the number of inserted or updated StoreItems should always be 1
      */
    def addStoreEntity ( storeid: String, store: StoreItem): Future[Int] = {
        val intItemType = ItemSpec.getIntOfKind(store.itemtype)
        db.run(StoreTable.filter(_.storeid === storeid).filter(_.itemtype === intItemType).result.headOption).flatMap {
            case Some(storeEntity) =>
                val storeUpdate = StoreEntityUpdate(
                    storeEntity.storeid
                    , storeEntity.itemtype
                    , storeEntity.quantity
                )
                val updatedStore = storeUpdate.add(ItemSpec.getCapacity(intItemType))
                db.run(StoreTable.filter(_.storeid === storeid)
                    .filter(_.itemtype === intItemType)
                    .update(updatedStore)).flatMap {
                    case 1 => Future.successful(1)
                    case res =>
                        if(res > 0) {
                            log.error("More than 1 item updated. this should not happen - Store: " +
                                storeid + " - itemType: " + store.itemtype )
                        }
                        Future.successful(0)
                }
            case None =>
                val quantity = ItemSpec.getCapacity(intItemType)
                val storeEntity = StoreEntity(
                    storeid = storeid
                    , itemtype = intItemType
                    , quantity = quantity
                )
                db.run(StoreTable += storeEntity)
        }
    }

    def subtractStoreEntity(store: StoreEntity): Future[Int] =
        db.run(StoreTable.filter(_.storeid === store.storeid).filter(_.itemtype === store.itemtype).result.headOption).flatMap {
            case Some(storeEntity) =>
                val storeUpdate = StoreEntityUpdate(
                    storeEntity.storeid
                    , storeEntity.itemtype
                    , storeEntity.quantity
                )
                val updatedStore = storeUpdate.subtract(store.quantity)
                db.run(StoreTable.filter(_.storeid === store.storeid)
                    .filter(_.itemtype === store.itemtype)
                    .update(updatedStore)).flatMap {
                    case 1 => Future.successful(1)
                    case res =>
                        if(res > 0) {
                            log.error("More than 1 item updated. this should not happen - Store: " +
                                store.storeid + " - itemType: " + ItemSpec.getKindOfInt(store.itemtype) )
                        }
                        Future.successful(0)
                }
            case None => Future.successful(0)
        }


    /** Deletes the whole Store
      *
      * @param storeId the id of the store to delete
      * @return number of deleted StoreItems
      */
    def deleteStore(storeId: String): Future[Int] = {
        db.run(StoreTable.filter(_.storeid === storeId).delete)
    }

    /** Delete all Store Entities ( only for testing purposes )
      *
      * @return the number of deleted StoreEntities
      */
    def deleteAllStores(): Future[Int] = {
        db.run(StoreTable.delete)
    }

    /** Get all StoreItems of a given Store
      *
      * @param storeId the Id of the store to get all store Items from
      * @return
      */
    def getStoreById(storeId: String): Future[Seq[StoreEntity]] = {
        db.run(StoreTable.filter(_.storeid === storeId ).result)
    }

    /** Get exactly one StoreItemOsName out of a given Store
      *
      * @param storeId The Id of the Store to get the Store Item from
      * @param itemType The ItemType of the StoreItemOsName
      * @return The Store Entity
      */
    def getStoreEntity( storeId: String, itemType: Int ): Future[Option[StoreEntity]] = {
        db.run(StoreTable.filter(_.storeid === storeId )
            .filter(_.itemtype === itemType).result.headOption)
    }






    /** Gets a specific item from a given Store if the store Capacity is sufficient
      *
      * @param storeId the id of the store to delete
      * @param itemType the ItemType to delete
      * @return number of deleted StoreItems
      */
    def getFromStore ( storeId: String, itemType: Int, ownerId: String): Future[Option[StoreItemOsName]] = {
        log.debug("getFromStore - id: " + storeId + " - itemTyoe: " + ItemSpec.getKindOfInt(itemType) + " - ownerId: " + ownerId)
        db.run(StoreTable.filter(_.storeid === storeId).filter(_.itemtype === itemType).result.headOption).flatMap {
            case Some(storeEntity) =>
                val capacity = ItemSpec.getCapacity(itemType)
                if ( storeEntity.quantity < capacity ) {
                    // Store quantity less than Container Capacity we do not pass back anything
                    Future.successful(None)
                } else if ( storeEntity.quantity == capacity) {
                    // Store quantity equal than Container Capacity we return the Container
                    // create a MarketInventoryEntity
                    // and delete the StoreItemEntity
                    val si = deleteStoreEntity(storeId, itemType)
                    val mi = createMarketInventoryItem(itemType, ownerId)
                    val combinedFuture = for (
                        a <- si;
                        b <- mi
                    ) yield checkStoreEntity ( a, b, itemType, storeId )
                    combinedFuture
                } else {
                    // Store quantity bigger than Container Capacity we return the Container
                    // create a MarketInventoryEntity
                    // and update the StoreItemEntity
                    val si = updateStoreEntity(storeId, itemType, ItemSpec.getCapacity(itemType))
                    val mi = createMarketInventoryItem(itemType, ownerId)
                    val combinedFuture = for (
                        a <- si;
                        b <- mi
                    ) yield checkStoreEntity ( a, b, itemType, storeId )
                    combinedFuture
                }
            case None =>
                Future.successful(None)
        }
    }

    private def checkStoreEntity ( numSi: Int, optMi: Option[String], itemType: Int, storeId: String): Option[StoreItemOsName] = {
        if (numSi == 1) {
            optMi match {
                case Some(_ ) =>
                    val storeItem = StoreItemOsName(ItemSpec.getOsObjectName(itemType))
                    Some(storeItem)
                case None =>
                    // No MarketInventory could be created
                    // Restoring the Store
                    revertStoreEntity(storeId,itemType)
                    None
            }
        } else {
            // StoreItemEntity could not be deleted
            // Have to remove the MarketInventoryEntity
            optMi match {
                case Some(id) =>
                    deleteMarketInventory(id)
                    None
                case None =>
                    None
            }
        }
    }

    private def updateStoreEntity(storeId: String, itemType: Int, quantity: Int): Future[Int] = {
        val storeEntity = StoreEntity(
            storeId,itemType,quantity
        )
        subtractStoreEntity(storeEntity)
    }

    private def revertStoreEntity(storeId: String, itemType: Int): Unit = {
        val strgItemType = ItemSpec.getKindOfInt(itemType)
        val storeItem = StoreItem(strgItemType)
        val result: Future[Int] = addStoreEntity(storeId, storeItem)
        val success = Await.result(result, 5 seconds)
        success match {
            case 1 => None
            case _ =>
                log.error("Unable to re-creae StoreItemEntity. This should not happen. check log and database" +
                    "- Store: "+storeId +" - itemType: " + ItemSpec.getKindOfInt(itemType) )
                None
        }
    }

    /** Deletes a MarketInventoryEntity
      *
      * @param id the internal id of the MarketInventoryEntity
      */
    private def deleteMarketInventory(id: String): Unit = {
        Central.optMarketInventoryService match {
            case Some(ms) =>
                ms.deleteMarketInventoryEntity(id)
            case None =>
                log.error("No marketInventoryService available. This should not happen. Check Log")
                None
        }
    }

    /** Creates MarketItemEntity
      *
      * @param itemType the ItemType of the MarketItemEntity
      * @param ownerid the owner of the MarketItemEntity
      * @return Future[Int] which always should be 1
      */
    private def createMarketInventoryItem(itemType: Int, ownerid: String): Future[Option[String]] = {
        Central.optMarketInventoryService match {
            case Some(ms) =>
                val mi = MarketInventory(
                    UUID_ZERO
                    , ownerid
                    , ItemSpec.getKindOfInt(itemType)
                    , UUID_ZERO
                )
                ms.createMarketInventoryEntityReturningId(mi)
            case None =>
                Future.successful(None)
        }

    }

    /** Deletes a StoreItemOsName
      *
      * @param storeId the uuid of the store
      * @param itemType the itemType to delete
      * @return Future[Int] the number of deleted Items should be 1
      */
    private def deleteStoreEntity(storeId: String, itemType: Int): Future[Int] = {
        db.run(StoreTable.filter(_.storeid === storeId).filter(_.itemtype === itemType).delete)
    }


}