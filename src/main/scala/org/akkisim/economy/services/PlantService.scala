/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.db.{PlantEntity, PlantEntityTable, PlantEntityUpdate}
import org.akkisim.economy.dataproviders.items.PlantUpdate
import org.akkisim.economy.dataproviders.types.Modification
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Akira Sonoda
  */

class PlantService ( val databaseService: DatabaseService)( implicit executionContext: ExecutionContext) extends PlantEntityTable {
    val log: Logger = LoggerFactory.getLogger ( "PlantService" )

    import databaseService.driver.api._

    /**
      * Stores a Plant in the Plant Table if it's not already ther
      *
      * @param plantEntity the Plant to store
      * @return a Future containing an Int which should be 1 ( the amount of Plants stored in the database )
      */
    def createPlant(plantEntity: PlantEntity): Future[Int] = {
        log.debug("createPlant")
        db.run(PlantTable.filter(p => p.id === plantEntity.id).result.headOption).flatMap {
            case Some(plant) =>
                log.debug("PlantFound")
                val plantUpdate = PlantEntity (
                    id = plant.id
                    , plantstate = plant.plantstate
                    , planttype = plant.planttype
                    , growthpercent = plant.growthpercent
                    , maxnofruits = plant.maxnofruits
                    , productioncycles = plant.productioncycles
                    , heartbeatcycles = plantEntity.heartbeatcycles
                    , pushurl = plantEntity.pushurl
                )
                db.run ( PlantTable.filter ( _.id === plant.id ).update ( plantUpdate ) ).flatMap { result =>
                    Future.successful(result)
                }
            case None =>
                log.debug("PlantNotFound")
                db.run(PlantTable += plantEntity).flatMap { result =>
                    Future.successful(result)
                }
        }
    }

    /**
      * Deletes a Plant.
      *
      * @param id the Id of the Plant
      * @return a Future containing an Int which should be 1 ( the amount of Plants deleted in the database )
      */
    def deletePlant(id: String): Future[Int] = {
        log.debug("deletePlant")
        db.run(PlantTable.filter(_.id === id).delete)
    }

    /**
      * Delete all Plants ( should only be used for testing purposes )
      *
      * @return
      */
    def deleteAllPlants(): Future[Int] = {
        db.run(PlantTable.delete)
    }

    /**
      * We read the Plant with the given Id
      *
      * @param id the Id of the Plant
      * @return a Future with an Option of a PlantEntity
      */
    def getPlantById ( id: String): Future[Option[PlantEntity]] = {
        log.debug("getPlantById")
        db.run(PlantTable.filter(_.id === id).result.headOption)
    }

    /** Updates a plant
      *
      * @param plantEntityUpdate updated Plant
      * @return the updated Plant
      */
    def updatePlant(plantEntityUpdate: PlantEntityUpdate): Future[Option[PlantEntity]] = getPlantById(plantEntityUpdate.id).flatMap {
        case Some(plant) =>
            val updatedPlant = plantEntityUpdate.merge()
            db.run(PlantTable.filter(_.id === plantEntityUpdate.id).update(updatedPlant)).map(_ => Some(updatedPlant))
        case None => Future.successful(None)
    }

    /**
      * We modify the Volume of the Plant based on the instruction
      * TODO we currently do not consider the measure Unit it's only for documentation purposes
      *
      * @param id the id of the plantto be modified
      * @param plantUpdate the Modification
      * @return an optional PlantEntity
      */
    def updatePlantVolume(id: String, plantUpdate: PlantUpdate): Future[Option[PlantEntity]] = getPlantById(id).flatMap {
        case Some(plant) =>
            log.debug("updatePlantVolume")
            val newVolume = modifyVolume(plant.productioncycles, plant.maxnofruits, plantUpdate)
            val updatedPlant = PlantEntity(
                id = plant.id
                , plantstate = plant.plantstate
                , planttype = plant.planttype
                , growthpercent = plant.growthpercent
                , maxnofruits = plant.maxnofruits
                , productioncycles = newVolume
                , heartbeatcycles = plant.heartbeatcycles
                , pushurl = plant.pushurl
            )
            db.run(PlantTable.filter(_.id === id).update(updatedPlant)).map(_ => Some(updatedPlant))
        case None => Future.successful(None)
    }

    /** Modifies the volume of a plant. The volume can not decrease below 0
      * and can never be more than the maximum volume
      *
      * @param i the desired modification
      * @param maxVolume the maximum number of fruits the plant can produce
      * @param plantUpdate the update object
      * @return the adjusted volume
      */
    private def modifyVolume ( i: Int, maxVolume: Int ,plantUpdate: PlantUpdate): Int = {
        val modification = Modification.getStateModificationByName(plantUpdate.modification)
        if(modification == Modification.ADDITION) {
            val result = i + plantUpdate.volumeChange
            if ( result > maxVolume ) { maxVolume } else { result } // Volume can not be bigger then MaxVolume
        } else if(modification == Modification.SUBTRACTION) {
            val result = i - plantUpdate.volumeChange
            if ( result < 0 ) { 0 } else { result }
        } else { 0 }
    }

    /** Updates the plant growth value. if successful the modified plant will be returned otherwise None
      *
      * @param id the plantId to update
      * @param plantUpdate the update Object
      * @return Option of the updated Plant
      */
    def updatePlantGrowth(id: String, plantUpdate: PlantUpdate): Future[Option[PlantEntity]] = getPlantById(id).flatMap {
        case Some(plant) =>
            log.debug("updatePlantGrowth")

            val newGrowth = modifyGrowth(plant.growthpercent, plantUpdate)
            val updatedPlant = PlantEntity(
                id = plant.id
                , plantstate = plant.plantstate
                , planttype = plant.planttype
                , growthpercent = newGrowth
                , maxnofruits = plant.maxnofruits
                , productioncycles = plant.productioncycles
                , heartbeatcycles = plant.heartbeatcycles
                , pushurl = plant.pushurl
            )
            db.run(PlantTable.filter(_.id === id).update(updatedPlant)).map(_ => Some(updatedPlant))
        case None => Future.successful(None)
    }

    /** Modifies the growth Value. A plant can never grow more than 100
      *
      * @param i the desired modification
      * @param plantUpdate the update Object
      * @return the adjusted modification
      */
    private def modifyGrowth ( i: Int,plantUpdate: PlantUpdate): Int = {
        val modification = Modification.getStateModificationByName(plantUpdate.modification)
        if(modification == Modification.ADDITION) {
            val result = i + plantUpdate.growthChange
            if ( result > 100 ) { 100 } else { result } // Growth can not be bigger than 100%
        } else if(modification == Modification.SUBTRACTION) {
            val result = i - plantUpdate.growthChange
            if ( result < 0 ) { 0} else { result }
        } else { 0 }
    }


}