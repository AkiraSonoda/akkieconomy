/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */
package org.akkisim.economy.services
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db.{InventoryEntity, InventoryEntityTable, InventoryEntityUpdate}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Akira Sonoda
  */
class InventoryService(val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends InventoryEntityTable {

    import databaseService.driver.api._

    /**
      * Inventory Items will only be created if they do not already exist
      *
      * @param inventory the InventoyEntity to be created
      * @return the number of created InventoryEntities. Should be 1
      */
    def createInventoryItem(inventory: InventoryEntity): Future[Int] = {
        val query = Inventory.filter(_.itemid === inventory.itemid)

        val finalQuery = {
            query.exists.result.flatMap{ exitingRecord =>
                if (!exitingRecord) {
                    Inventory += inventory
                } else {
                    DBIO.successful(1)
                }
            }
        }

        val result: Future[Any] = db.run(finalQuery.transactionally)
        val responseFuture: Future[Int] = result.mapTo[Int]
        responseFuture

    }

    def deleteInventoryItem(itemId: String): Future[Int] = db.run(Inventory.filter(_.itemid === itemId).delete)

    def deleteAllInventoryItems(): Future[Int] = db.run(Inventory.delete)

    def getInventoryItems: Future[Seq[InventoryEntity]] = db.run(Inventory.result)

    def getPlantsPerUser(ownerId: String): Future[Seq[InventoryEntity]] = {
        val plant_tree = ItemSpec.getIntOfKind("PLANT_TREE")
        val plant_bush = ItemSpec.getIntOfKind("PLANT_BUSH")
        val plant_vegetable = ItemSpec.getIntOfKind("PLANT_VEGETABLE")
        val plantTypes = List(plant_bush, plant_vegetable, plant_tree)

        db.run(Inventory.filter(_.itemownerid === ownerId).filter(_.itemtype inSetBind plantTypes).result)
    }
    
    def getInventoryItemById(itemId: String): Future[Option[InventoryEntity]] = db.run(Inventory.filter(_.itemid === itemId).result.headOption)
    def getInventoryItemByOwnerId(itemOwnerId: String): Future[Option[InventoryEntity]] = db.run(Inventory.filter(_.itemownerid === itemOwnerId).result.headOption)

    def updateInventoryItem(itemId: String, inventoryUpdate: InventoryEntityUpdate): Future[Option[InventoryEntity]] = getInventoryItemById(itemId).flatMap {
        case Some(inventoryItem) =>
            val updatedIventoryItem = inventoryUpdate.merge(inventoryItem)
            db.run(Inventory.filter(_.itemid === itemId).update(updatedIventoryItem)).map(_ => Some(updatedIventoryItem))
        case None => Future.successful(None)
    }

}
