/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import akka.actor.ActorRef
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.items.Price
import org.akkisim.economy.dataproviders.types.{PushUrl, RegisterPushUrl}

import scala.concurrent.{ExecutionContext, Future}


/**
  * @author Akira Sonoda
  */
class ItemSpecService ( val databaseService: DatabaseService)( implicit executionContext: ExecutionContext) extends ItemSpecEntityTable {
    import databaseService.driver.api._

    /**
      *
      * @param itemSpecEntity
      * @return
      */
    def createItemSpec ( itemSpecEntity: ItemSpecEntity): Future[Int] = {
        db.run(ItemSpecTable += itemSpecEntity)
    }

    /**
      *
      * @param kind
      * @return
      */
    def deleteItemSpec ( kind: String): Future[Int] = {
        db.run(ItemSpecTable.filter(_.kind === kind).delete)
    }

    /** Delete all Prices (only used for testing purposes
      *
      * @return the number of deleted prices
      */
    def deleteAllItemSpec (): Future[Int] = {
        db.run(ItemSpecTable.delete)
    }

    /**
      *
      * @return
      */
    def getItemSpec: Future[Seq[ItemSpecEntity]] = {
        db.run(ItemSpecTable.result)
    }

    /**
      *
      * @param kind
      * @return
      */
    def getPriceByKind ( kind: String): Future[Option[Price]] = {
        db.run ( ItemSpecTable.filter ( _.kind === kind ).result.headOption ).flatMap {
            case Some(itemSpec) =>
                val price = Price(itemSpec.kind, itemSpec.price)
                Future.successful(Some(price))
            case None =>
                Future.successful(None)
        }
    }

    /**
      *
      * @param kind
      * @return
      */
    def getItemSpecByKind( kind: String ): Future[Option[ItemSpecEntity]] = {
        db.run ( ItemSpecTable.filter ( _.kind === kind ).result.headOption )
    }

    def updateItemSpec ( kind: String, itemSpecUpdate: ItemSpecEntityUpdate): Future[Option[ItemSpecEntity]] = getItemSpecByKind(kind).flatMap {
        case Some(price) =>
            val updatedPrice = itemSpecUpdate.merge()
            db.run(ItemSpecTable.filter(_.kind === kind).update(updatedPrice)).map( _ => Some(updatedPrice))
        case None => Future.successful(None)
    }
}