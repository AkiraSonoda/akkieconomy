/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import akka.actor.ActorRef
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import org.akkisim.economy.dataproviders.db._
import org.akkisim.economy.dataproviders.types.{PushUrl, RegisterPushUrl}

import scala.concurrent.{ExecutionContext, Future}


/**
  * @author Akira Sonoda
  */
class UserService(val databaseService: DatabaseService)(val central: ActorRef)(implicit executionContext: ExecutionContext) extends UserEntityTable {

    import databaseService.driver.api._

    def createUser(user: UserEntity): Future[Int] = {
        db.run(User += user)
    }

    def deleteUser(avatarUUID: String): Future[Int] = {
        db.run(User.filter(_.avataruuid === avatarUUID).delete)
    }

    def getUsers: Future[Seq[UserEntity]] = {
        db.run(User.result)
    }

    def getUserById(avatarUUID: String): Future[Option[UserEntity]] = {
        db.run(User.filter(_.avataruuid === avatarUUID).result.headOption)
    }

    def updateUser(avatarUUID: String, userUpdate: UserEntityUpdate): Future[Option[UserEntity]] = getUserById(avatarUUID).flatMap {
        case Some(user) =>
            val updatedUser = userUpdate.merge(user)
            db.run(User.filter(_.avataruuid === avatarUUID).update(updatedUser)).map(_ => Some(updatedUser))
        case None => Future.successful(None)
    }

    def createUser(avatarUUID: String, avatarName: String, avatarPassword: String): Future[Option[UserEntity]] = {
        db.run(User += UserEntity(avatarname = avatarName, avataruuid = avatarUUID, avatarpassword = avatarPassword)).flatMap {
            case 1 => getUserById(avatarUUID)
            case _ => Future.successful(None)
        }
    }

    def userStatusUpdate(avatarUUID: String, avatarhunger: Int, avatarthirst: Int): Future[Int] = {
        db.run(User.filter(_.avataruuid === avatarUUID)
            .map(x => (x.avatarhunger, x.avatarthirst))
            .update(Some(avatarhunger), Some(avatarthirst)))
    }
    
    
    def pushUrl(userEntity: UserEntity, pushUrl: PushUrl): StatusCode = {
        central ! RegisterPushUrl(userEntity.avataruuid, pushUrl.url)
        StatusCodes.OK
    }
}