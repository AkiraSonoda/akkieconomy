/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import org.akkisim.economy.dataproviders.db.{ContainerItemEntity, ContainerItemEntityTable}
import org.akkisim.economy.dataproviders.items.ContainerUpdate
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Akira Sonoda
  */

class ContainerItemsService(val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends ContainerItemEntityTable {
    val log: Logger = LoggerFactory.getLogger ( "ContainerItemsService" )

    import databaseService.driver.api._

    /**
      * Stores a ContainerItem in the container_item Table if it's not already ther
      *
      * @param containerItem the ContainerItem to store
      * @return a Future containing an Int which should be 1 ( the amount of ItemEntities stored in the database )
      */
    def createContainerItem(containerItem: ContainerItemEntity): Future[Int] = {
        log.debug("createContainerItem")
        val query = ContainerItem.filter(_.id === containerItem.id)

        val finalQuery = {
            query.exists.result.flatMap{ exitingRecord =>
                if (!exitingRecord) {
                    ContainerItem += containerItem
                } else {
                    DBIO.successful(1)
                }
            }
        }

        val result: Future[Any] = db.run(finalQuery.transactionally)
        val responseFuture: Future[Int] = result.mapTo[Int]
        responseFuture
    }

    /**
      * Deletes a ContainerItem.
      *
      * @param id the Id of the ContainetItem
      * @return a Future containing an Int which should be 1 ( the amount of ItemEntities deleted in the database )
      */
    def deleteContainerItem(id: String): Future[Int] = {
        log.debug("deleteContainerItem")
        db.run(ContainerItem.filter(_.id === id).delete)
    }


    /**
      * We read the ItemEntity with the given Id
      *
      * @param id the Id of the ContainerItem
      * @return a Future with an Option of a ContainerItem
      */
    def getContainerItemById(id: String): Future[Option[ContainerItemEntity]] = {
        log.debug("getPlantById")
        db.run(ContainerItem.filter(_.id === id).result.headOption)
    }

    /**
      * We subtract the updated Volume from the current Volume
      * TODO we currently do not consider the measure Unit it's only for documentation purposes
      *
      * @param id
      * @param containerUpdate
      * @return
      */
    def subtractContainerVolume(id: String, containerUpdate: ContainerUpdate): Future[Option[ContainerItemEntity]] = getContainerItemById(id).flatMap {
        case Some(ct) =>
            log.debug("subtractContainerVolume")
            val updatedContainer = new ContainerItemEntity(ct.id, ct.maxvolume, (ct.currentvolume - containerUpdate.volume), ct.measureunit )
            db.run(ContainerItem.filter(_.id === id).update(updatedContainer)).map(_ => Some(updatedContainer))
        case None => Future.successful(None)
    }

}