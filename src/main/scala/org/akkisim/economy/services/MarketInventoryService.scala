/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.services

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCode
import org.akkisim.economy.dataproviders.caches.ItemSpec
import org.akkisim.economy.dataproviders.db._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}


/**
  * @author Akira Sonoda
  */
class MarketInventoryService ( val databaseService: DatabaseService)( implicit executionContext: ExecutionContext) extends MarketInventoryEntityTable {

    import databaseService.driver.api._
    val log: Logger = LoggerFactory.getLogger("MarketInventoryService")

    /** Creates a MarketInventory
      *
      * @param marketInventory the MarketInventory to be created
      * @return the number of created MarketInventories
      */
    def createMarketInventoryEntity(marketInventory: MarketInventory): Future[Int] = {
        log.debug("create MarketInventoryEntity")
        val marketInventoryInsert = MarketInventoryEntity(
            intid = UUID.randomUUID().toString,
            id = marketInventory.id,
            ownerid = marketInventory.ownerid,
            itemtype = ItemSpec.getIntOfKind (marketInventory.itemtype),
            origin = marketInventory.origin)
        db.run(MarketInventoryTable += marketInventoryInsert)
    }


    /** Creates a MarketInventory that returns the internal representation. Should not be exposed to the outside
      *
      * @param marketInventory the MarketInventory to be created
      * @return Option of the internal representation of the MarketInventory
      */
    def createMarketInventoryEntityReturningId(marketInventory: MarketInventory): Future[Option[String]] = {
        log.debug("createMarketInventoryEntityReturningId")
        val internalId = UUID.randomUUID().toString
        val marketInventoryInsert = MarketInventoryEntity(
            intid = internalId,
            id = marketInventory.id,
            ownerid = marketInventory.ownerid,
            itemtype = ItemSpec.getIntOfKind (marketInventory.itemtype),
            origin = marketInventory.origin)
        db.run(MarketInventoryTable += marketInventoryInsert).flatMap {
            case 1 => Future.successful(Some(internalId))
            case _ => Future.successful(None)
        }
    }


    /**
      * In to delete a MarketInventory Item the UUID of the Item is sufficient,
      * becuse it is unique
      *
      * @param id the id of the Market Inventory to be deleted
      * @return the number of MarketInventories deleted. Should be 1
      */
    def deleteMarketInventoryEntity(id: String): Future[Int] = {
        log.debug ("delete MarketInventoryEntity")
        db.run (MarketInventoryTable.filter (_.id === id).delete)
    }

    /**
      * Delete all MarketInventory Items ( used mainly for testing purposes )
      *
      * @return
      */
    def deleteAllMarketInventory(): Future[Int] = {
        log.debug ("delete all MarketInventoryEntity")
        db.run(MarketInventoryTable.delete)

    }

    /**
      * Get all MerketInventory Entities
      *
      * @return a Sequence of all MarketInventoryEntities
      */
    def getMarketInventoryEntities: Future[Seq[MarketInventoryEntity]] = {
        log.debug ("get MarketInventoryEnties ")
        db.run(MarketInventoryTable.result)
    }

    /** The most precise approach to get a MarketInventory Item
      *
      * @param id the id of fhe Market Inventory Entity
      * @param ownerid the ownerid ( UUID ) of fhe Market Inventory Entity
      * @param itemType the Int ItemType of the MarketInventory Entity
      * @param origin the origin ( UUID ) of the MarketInventory Entity
      * @return Future Option of MarketInventoryEntity
      */
    def getUniqueMarketInventoryItem(id: String, ownerid: String, itemType: Int, origin: String): Future[Option[MarketInventoryEntity]] = {
        log.debug ("getUniqueMarketInventoryItem ")
        db.run(MarketInventoryTable
            .filter(_.id === id)
            .filter(_.ownerid === ownerid)
            .filter(_.itemtype === itemType)
            .filter(_.origin === origin)
            .result.headOption
        )
    }

    /** check the existence of a MarketInventoryItem
      *
      * @param id the id of fhe Market Inventory Entity
      * @param ownerid the ownerid ( UUID ) of fhe Market Inventory Entity
      * @param itemType the Int ItemType of the MarketInventory Entity
      * @param origin the origin ( UUID ) of the MarketInventory Entity
      * @return HttpStatus Code of the check
      */
    def checkUniqueMarketInventoryItem(id: String, ownerid: String, itemType: Int, origin: String): Future[StatusCode] = {
        log.debug ("getUniqueMarketInventoryItem ")
        db.run(MarketInventoryTable
            .filter(_.id === id)
            .filter(_.ownerid === ownerid)
            .filter(_.itemtype === itemType)
            .filter(_.origin === origin)
            .result.headOption
        ).flatMap {
            case Some(_) => Future.successful(StatusCodes.OK)
            case None => Future.successful(StatusCodes.NotFound)
        }
    }

    /** check the existence of a MarketInventoryItem
      *
      * @param id the id of fhe Market Inventory Entity
      * @param ownerid the ownerid ( UUID ) of fhe Market Inventory Entity
      * @param itemType the Int ItemType of the MarketInventory Entity
      * @param origin the origin ( UUID ) of the MarketInventory Entity
      * @return HttpStatus Code of the check
      */
    def deleteUniqueMarketInventoryItem(id: String, ownerid: String, itemType: Int, origin: String): Future[Int] = {
        log.debug ("getUniqueMarketInventoryItem ")
        db.run(MarketInventoryTable
            .filter(_.id === id)
            .filter(_.ownerid === ownerid)
            .filter(_.itemtype === itemType)
            .filter(_.origin === origin)
            .result.headOption
        ).flatMap {
            case Some(mi) =>
                deleteMarketInventoryEntityInt(mi.intid)
            case None => Future.successful(0)
        }
    }

    /** A sufficient precise approach to get a MarketInventory Item
      *
      * @param id the id of fhe Market Inventory Entity
      * @param ownerid the ownerid ( UUID ) of fhe Market Inventory Entity
      * @param itemType the Int ItemType of the MarketInventory Entity
      * @return Future Option of MarketInventoryEntity
      */
    def getMarketInventoryItem(id: String, ownerid: String, itemType: Int): Future[Option[MarketInventoryEntity]] = {
        log.debug ("getMarketInventoryItem id: "+id+" - ownerid: "+ownerid+ " - itemType: "+ itemType)
        db.run(MarketInventoryTable
            .filter(_.ownerid === ownerid)
            .filter(_.itemtype === itemType)
            .filter(_.id === id)
            .result.headOption
        )
    }

    /** A marketInventory to be updated
      *
      * @param id the id of fhe Market Inventory Entity
      * @param ownerid the ownerid ( UUID ) of fhe Market Inventory Entity
      * @param itemtype the String ItemType of the MarketInventory Entity
      * @param origin the origin ( UUID ) of the MarketInventory Entity
      * @param marketInventory the MarketInventory to be updated
      * @return Future Option of MarketInventoryEntity
      */
    def updateMarketInventoryEntity( id: String
                                     , ownerid: String
                                     , itemtype: String
                                     , origin: String
                                     , marketInventory: MarketInventory): Future[Option[MarketInventoryEntity]] =
        // First check if the MarketInventory to Update is already there
        getUniqueMarketInventoryItem(
            id = marketInventory.id
            ,ownerid = marketInventory.ownerid
            ,itemType = ItemSpec.getIntOfKind(marketInventory.itemtype)
            ,origin = marketInventory.origin
        ).flatMap {
            case Some(mi) =>
                Future.successful(Some(mi))
            case None =>
                getUniqueMarketInventoryItem(
                    id = id
                    ,ownerid = ownerid
                    ,itemType = ItemSpec.getIntOfKind(itemtype)
                    ,origin = origin
                ).flatMap {
                    case Some(mi) =>
                        val marketInventoryEntityUpdate = MarketInventoryEntityUpdate(
                            marketInventory.id
                            ,marketInventory.ownerid
                            ,ItemSpec.getIntOfKind(marketInventory.itemtype)
                            ,marketInventory.origin
                        )
                        val updatedMarketInventoryEntity = marketInventoryEntityUpdate.merge(mi)
                        db.run(MarketInventoryTable.filter(_.intid === mi.intid).update(updatedMarketInventoryEntity)).map(_ => Some(updatedMarketInventoryEntity) )
                    case None =>
                        Future.successful(None)
                }

        }

    /**
      * In to delete a MarketInventory Item the internal UUID of the Item is sufficient,
      * becuse it is unique
      *
      * @param intid the id of the Market Inventory to be deleted
      * @return the number of MarketInventories deleted. Should be 1
      */
    private def deleteMarketInventoryEntityInt(intid: String): Future[Int] = {
        log.debug ("delete MarketInventoryEntity")
        db.run (MarketInventoryTable.filter (_.intid === intid).delete)
    }

}