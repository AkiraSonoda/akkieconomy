/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.akkisim.economy.actors.{CentralActor, PlantFactoryActor}
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.types.SendHeartbeat
import org.akkisim.economy.http.HttpService
import org.akkisim.economy.services._
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * @author Akira Sonoda
  */
object EconomyServer extends App with Configuration with Directives  {
    val log = LoggerFactory.getLogger("EconomyServer")
    log.info("Starting App")

    // we need an ActorSystem to host our application in
    implicit val actorSystem: ActorSystem = ActorSystem("economyserver")
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val executor: ExecutionContext = actorSystem.dispatcher
    implicit val timeout: Timeout = Timeout(networkCallTimeout seconds)

    val flywayService = new FlywayService(dbJDBCUrl, dbUsername, dbPassword)
    val migrationResult = flywayService.migrateDatabaseSchema()
    log.info("Result of DatabaseMigration: %d".format(migrationResult))
    
    val centralActor = actorSystem.actorOf(Props[CentralActor], "CentralActor")
    Central.optCentral = Some(centralActor)

    //Starting the Scheduler after 60 Seconds
    actorSystem.scheduler.schedule(60 seconds,60 seconds, centralActor, SendHeartbeat)
    
    val databaseService = new DatabaseService()
    val inventoryService = new InventoryService(databaseService)
    val containerItemsService = new ContainerItemsService(databaseService)
    val userService = new UserService(databaseService)(centralActor)
    val authService = new AuthService(databaseService,userService,actorSystem)
    val marketInventoryService = new MarketInventoryService(databaseService)
    val plantService = new PlantService(databaseService)
    val itemSpecService = new ItemSpecService(databaseService)
    val storeService = new StoreService(databaseService)

    Central.optPlantService = Some(plantService)
    Central.optInventoryService = Some(inventoryService)
    Central.optMarketInventoryService = Some(marketInventoryService)
    Central.optUserService = Some(userService)

    ItemSpec.initialize(itemSpecService)

    val httpService = new HttpService(
        inventoryService,
        containerItemsService,
        userService,
        authService,
        marketInventoryService,
        plantService,
        itemSpecService,
        storeService
    )

    // We need a PlantFactoryActor
    val plantFactoryActor = actorSystem.actorOf(
        Props(new PlantFactoryActor(inventoryService,plantService, actorSystem)), name = "PlantFactoryActor"
    )
    Central.optPlantFactory = Some(plantFactoryActor)

    Http().bindAndHandle(httpService.routes, httpInterface, httpPort) map {
        binding =>
          log.info(s"REST interface bound to ${binding.localAddress}")
    } recover {
        case ex =>
          log.error(s"REST interface could not bind to interface: "+httpInterface+" Port: " + httpPort, ex.getMessage)
    }

}
