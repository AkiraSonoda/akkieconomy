/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{Actor, ActorSystem, Props}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import org.akkisim.economy.dataproviders.caches.Central
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.services.{InventoryService, PlantService}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

/** Purpose of this Actor is to create PlantActors and to remove them once no longer needed
  * This actor is also responsible to send the Heartbeats to the plants
  *
  * @author Akira Sonoda
  */
class PlantFactoryActor ( inventoryService: InventoryService, plantService: PlantService, actorSystem: ActorSystem ) (implicit executionContext: ExecutionContext) extends Actor {
    val log: Logger = LoggerFactory.getLogger ( "PlantFactoryActor" )

    final implicit val materializer: ActorMaterializer = ActorMaterializer ( ActorMaterializerSettings ( context.system ) )

    def receive: Receive = {
        case SendHeartbeat =>
            log.debug ( "SendHeartbeat for all Plants" )
            Central.plantActors.values.foreach ( actorRef => actorRef ! SendHeartbeat )
        case RegisterPlantOfUser ( avatarUUID: String ) =>
            log.debug ( "RegisterPlantOfUser: " + avatarUUID)
            registerPlantPerUser ( avatarUUID )
        case DeRegisterPlantOfUser ( avatarUUID: String ) =>
            log.debug ( "DeRegisterPlantOfUser: " + avatarUUID)
            deRegisterPlantPerUser ( avatarUUID )
    }

    /** Create and register PlantActors for the given User
      *
      * @param avatarUUID the UUID of the User we want to register the PlantActors for
      */
    private def registerPlantPerUser ( avatarUUID: String ): Unit = {
        log.debug ( "registerPlantOfUser" )
        val plantFuture = inventoryService.getPlantsPerUser ( avatarUUID )
        plantFuture onComplete {
            case Success ( inventories ) =>
                inventories.foreach {
                    invEntity =>
                        // We check if there is already a PlantActor running for the given Plant
                        val optRef = Central.plantActors.get ( invEntity.itemid )
                        optRef match {
                            case Some ( _ ) => // Nothing to do
                            case None =>
                                // Get the corresponding PlantEntity
                                val plantEntityFuture = plantService.getPlantById(invEntity.itemid)
                                plantEntityFuture onComplete {
                                    case Success(optPlantEntity) =>
                                        optPlantEntity match {
                                            case Some(plantEntity) =>
                                                val register = actorSystem.actorOf ( Props ( new PlantActor ( plantEntity ) ), name = invEntity.itemid )
                                                Central.plantActors += invEntity.itemid -> register
                                            case None =>
                                                // Shoule not happen
                                                log.error ( "No PlantEntity for ID: "
                                                    + invEntity.itemid
                                                    + " Name: "
                                                    + invEntity.itemname
                                                    + " Owner: "
                                                    + invEntity.itemownerid
                                                )
                                        }
                                    case Failure(ex) =>
                                        log.error ( "Failure on getting Plants from PlantService", ex )
                                }
                        }
                }
            case Failure ( exception ) =>
                log.error ( "Failure on getting Inventories from InventoryService", exception )
        }
    }

    /** Destroy all PlantActors of the Given User
      *
      * @param avatarUUID the UUID of the User we want to remove the PlantActors for
      */
    private def deRegisterPlantPerUser ( avatarUUID: String ): Unit = {
        log.debug ( "deRegisterPlantOfUser" )
        val plantFuture = inventoryService.getPlantsPerUser ( avatarUUID )
        plantFuture onComplete {
            case Success ( plants ) =>
                plants.foreach {
                    invEntity =>
                        // We check if there is already a PlantActor running for the given Plant
                        val optRef = Central.plantActors.get ( invEntity.itemid )
                        optRef match {
                            case Some ( actorRef ) =>
                                actorRef ! DeRegisterPlantOfUser(avatarUUID) // if this is the case, we delete it
                                Central.plantActors.remove(invEntity.itemid)
                            case None => // Nothing to do
                        }
                }
            case Failure ( exception ) =>
                log.error ( "Failure on getting Plants from InventoryService", exception )
        }

    }
}