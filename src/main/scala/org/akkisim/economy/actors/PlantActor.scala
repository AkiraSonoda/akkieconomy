/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{Actor, PoisonPill}
import akka.http.scaladsl.model._
import akka.http.scaladsl.{Http, HttpExt}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import akka.util.ByteString
import org.akkisim.economy.dataproviders.caches.{Central, ItemSpec}
import org.akkisim.economy.dataproviders.db.{PlantEntity, PlantEntityUpdate}
import org.akkisim.economy.dataproviders.items.{PlantState, PlantStateAck}
import org.akkisim.economy.dataproviders.types._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
  *
  * @author Akira Sonoda
  */
class PlantActor ( plantEntity: PlantEntity  ) extends Actor {
    val log: Logger = LoggerFactory.getLogger ( "PlantActor" )
    val http: HttpExt = Http(context.system)

    import akka.pattern.pipe
    import context.dispatcher
    final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))

    var myPushUrl: String = plantEntity.pushurl
    var myHeartbeatCount: Int = plantEntity.heartbeatcycles
    var myPlantStateVal: PlantState.Value = PlantState.getPlantStateStateValueByInt(plantEntity.plantstate)
    var myProductionCyles: Int = plantEntity.productioncycles
    var myPlantStateAck: PlantStateAck.Value = PlantStateAck.getPlantStateAckStateValueByInt(plantEntity.plantstateack)
    val myMaxCycles: Int = ItemSpec.getCycles(plantEntity.planttype)
    val myCycleTime: Int = ItemSpec.getCycleTime(plantEntity.planttype)

    val timeout: FiniteDuration = 3000.millis


    def receive: Receive = {
        case SendHeartbeat =>
            log.debug("SendHeartbeat")
            doHeartbeat()
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
            entity.dataBytes.runFold(ByteString(""))(_ ++ _).foreach {
                body =>
                    val strg = body.utf8String
                    log.debug("Request successful - entity: "+ strg )
                    strg match {
                        case "PLANT_HARVEST_OK" =>
                            // State change acknowledged we store it
                            myPlantStateAck = PlantStateAck.PLANT_PRODUCING_FRUITS_ACK
                            myPlantStateVal = PlantState.PLANT_HARVEST
                            myHeartbeatCount = 0
                            myProductionCyles += 1
                            updatePlantStates(myPlantStateAck, myPlantStateVal)
                        case "PLANT_DEAD_OK" =>
                            doDeletePlant()
                    }
            }

        case HttpResponse(StatusCodes.NotFound, _, entity, _) =>
            entity.discardBytes()
            log.debug("Request failed, response code: 404")

            // TODO Think about how to handle 404 There could be several reasons
        case PlantPushUrl(url) =>
            log.debug("PlantPushUrl")
            myPushUrl = url
        case StateChange =>
            log.debug("StateChange")
            updateStateChange()
        case DeRegisterPlantOfUser ( avatarUUID ) =>
            log.debug ( "deRegisterPlantOfUser: " +avatarUUID )
            persistPlantState()

    }

    /**
      *
      */
    private def doDeletePlant(): Unit = {
        // Delete this Plant from the databases
        val combinedFuture = for (
            a <- deleteFromInventory(plantEntity.id);
            b <- deleteFromPlant(plantEntity.id);
            c <- deleteFromMarketInventory(plantEntity.id)
        ) yield deletePlantInt ( a, b, c )

        combinedFuture.onComplete {
            case Success(result) =>
                if ( result ) {
                    removePlantFromCentral ( plantEntity.id )
                } else {
                    log.error ( "Something strange happened during the deletion of this plant check log" )
                    removePlantFromCentral ( plantEntity.id )
                }
            case Failure ( exception ) =>
                log.error("Failure during the removal of the Plant. Check the log")
                removePlantFromCentral(plantEntity.id)
        }
        // Delete this Actor from Central
    }

    /**
      *
      */
    private def doHeartbeat(): Unit = {
        log.debug("doHeartbeat for Plant: " + plantEntity.id)
        myHeartbeatCount += 1
        myPlantStateVal match {
            case PlantState.PLANT_GROWING => // TODO  PlantState.PLANT_GROWING
            case PlantState.PLANT_PRODUCING_FRUITS =>
                if ( isTerminatedPlantProducingFruits(myHeartbeatCount) ) {
                    // We set myPlantStateAck to PLANT_PRODUCING_FRUITS_WAIT_ACK
                    myPlantStateAck = PlantStateAck.PLANT_PRODUCING_FRUITS_WAIT_ACK
                    // We send the state-change "PLANT_HARVEST" to the plant
                    updatePlantStateAck(myPlantStateAck)
                }
            case PlantState.PLANT_HARVEST =>
                // We actually dont't do anything until we get a state change which will come from the OpenSim LSL Script
                myHeartbeatCount -= 1
            case PlantState.PLANT_SICK => // TODO PlantState.PLANT_SICK
            case PlantState.PLANT_DEAD => // TODO PlantState.PLANT_DEAD
        }
    }

    /**
      *
      */
    private def updateStateChange(): Unit = {
        myPlantStateVal match {
            case PlantState.PLANT_GROWING => // TODO PLANT_GROWING State change
            case PlantState.PLANT_PRODUCING_FRUITS => // TODO PLANT_PRODUCING_FRUITS State change
            case PlantState.PLANT_HARVEST =>
                if (myProductionCyles < myMaxCycles) {
                    myPlantStateVal = PlantState.PLANT_PRODUCING_FRUITS
                    myPlantStateAck = PlantStateAck.UNDEFINED
                    myHeartbeatCount = 0
                    updatePlantStates(myPlantStateAck, myPlantStateVal)
                } else {
                    // We set myPlantStateAck to PLANT_PRODUCING_FRUITS_WAIT_ACK
                    myPlantStateAck = PlantStateAck.PLANT_DEAD_WAIT_ACK
                    // We send the state-change "PLANT_HARVEST" to the plant
                    updatePlantStateAck(myPlantStateAck)
                }
            case PlantState.PLANT_SICK => // TODO PLANT_SICK State change
            case PlantState.PLANT_DEAD => // TODO PLANT_DEAD State change
        }
    }


    /**
      *
      */
    private def persistPlantState( ): Unit = {
        log.debug("updatePlantStates")

        val plantUpdate = PlantEntityUpdate (
            id = plantEntity.id
            ,planttype = plantEntity.planttype
            ,plantstate = plantEntity.plantstate
            ,plantstateack = plantEntity.plantstateack
            ,growthpercent = plantEntity.growthpercent
            ,maxnofruits = plantEntity.maxnofruits
            ,productioncycles = myProductionCyles
            ,heartbeatcycles = myHeartbeatCount
            ,pushurl = myPushUrl
        )
        Central.optPlantService match {
            case Some(plantService) =>
                plantService.updatePlant(plantUpdate).onComplete {
                    case Success(updPlant) => self ! PoisonPill
                    case Failure ( exception ) =>
                        log.error("Failed to update the Plant:", exception)
                        self ! PoisonPill
                    // TODO we never should get here therefore we need a better way to inform the Administrator
                }
            case None =>
                log.error("Failed to update States of the Plant after ack:" + plantEntity.id)
                self ! PoisonPill
            // TODO we never should get here therefore we need a better way to inform the Administrator
        }
    }


    /**
      *
      * @param ack
      * @param state
      */
    private def updatePlantStates(ack: PlantStateAck.Value, state: PlantState.Value ): Unit = {
        log.debug("updatePlantStates")

        val plantUpdate = PlantEntityUpdate (
            id = plantEntity.id
            ,planttype = plantEntity.planttype
            ,plantstate = state.id
            ,plantstateack = ack.id
            ,growthpercent = plantEntity.growthpercent
            ,maxnofruits = plantEntity.maxnofruits
            ,productioncycles = myProductionCyles
            ,heartbeatcycles = myHeartbeatCount
            ,pushurl = myPushUrl
        )
        Central.optPlantService match {
            case Some(plantService) =>
                plantService.updatePlant(plantUpdate).onComplete {
                    case Success(updPlant) =>
                    case Failure ( exception ) =>
                        log.error("Failed to update the Plant:", exception)
                        // TODO we never should get here therefore we need a better way to inform the Administrator
                }
            case None =>
                log.error("Failed to update States of the Plant after ack:" + plantEntity.id)
                // TODO we never should get here therefore we need a better way to inform the Administrator
        }
    }

    /**
      *
      * @param state
      */
    private def updatePlantStateAck(state: PlantStateAck.Value): Unit = {
        log.debug("updatePlantStateAck")

        val plantUpdate = PlantEntityUpdate (
            id = plantEntity.id
            ,planttype = plantEntity.planttype
            ,plantstate = plantEntity.plantstate
            ,plantstateack = state.id
            ,growthpercent = plantEntity.growthpercent
            ,maxnofruits = plantEntity.maxnofruits
            ,productioncycles = plantEntity.productioncycles
            ,heartbeatcycles = myHeartbeatCount
            ,pushurl = myPushUrl
        )
        Central.optPlantService match {
            case Some(plantService) =>
                plantService.updatePlant(plantUpdate).onComplete {
                    case Success(updPlant) =>
                        myPlantStateAck match {
                            case PlantStateAck.PLANT_PRODUCING_FRUITS_WAIT_ACK =>
                                doSingleRequest("PLANT_HARVEST") // We notify the Plant about the State Change
                                myHeartbeatCount -=1 // State is not yet Acknowledged
                            case PlantStateAck.PLANT_DEAD_WAIT_ACK =>
                                doSingleRequest("PLANT_DEAD") // We notify the Plant about the State Change
                                myHeartbeatCount -=1 // State is not yet Acknowledged
                        }
                    case Failure ( exception ) =>
                        log.error("Failed to update the Plant:", exception)
                        myHeartbeatCount -= 1 // We were not able to update the Plant we do not count this cycle
                }
            case None =>
                log.error("No PlantService")
                myHeartbeatCount -= 1 // No Plant Service we do not count this cycle
        }
    }

    /**
      *
      * @param heartbeatCount
      * @return
      */
    private def isTerminatedPlantProducingFruits(heartbeatCount: Int): Boolean = {
        if ((heartbeatCount % myCycleTime) == 0) true else false
    }

    /**
      *
      * @param message
      */
    private def doSingleRequest(message: String): Unit = {
        log.debug("doSingleRequest")
        http.singleRequest(HttpRequest(uri = myPushUrl
            , method = HttpMethods.PUT
        ).withEntity(ContentTypes.`text/plain(UTF-8)`, message)).pipeTo(self)

    }

    /** Delete this plant from the Inventory
      *
      * @param id the id of the plant to be deleted from inventory
      * @return 0 = nothing deleted 1 = delete successful
      */
    private def deleteFromInventory(id: String): Future[Int] = {
        log.debug("deleteFromInventory: id: " + id )
        Central.optInventoryService match {
            case Some(inventoryService) =>
                inventoryService.deleteInventoryItem(id)
            case None =>
                log.error("No Inventory Service")
                Future.successful(0)
        }
    }

    /** Delete this plant from the Plant Database
      *
      * @param id the id of the plant to be deleted from the PlantDatabase
      * @return 0 = nothing deleted 1 = delete successful
      */
    private def deleteFromPlant(id: String): Future[Int] = {
        log.debug("deleteFromPlant id:" + id )
        Central.optPlantService match {
            case Some(plantService) =>
                plantService.deletePlant(id)
            case None =>
                log.error("No Plant Service")
                Future.successful(0)
        }
    }

    /** Delete this plant from the MarketInventory
      *
      * @param id the id of the plant to be deleted from the MarketInventor
      * @return 0 = nothing deleted 1 = delete successful
      */
    private def deleteFromMarketInventory(id: String): Future[Int] = {
        log.debug("deleteFromMarketInventory id: " + id)
        Central.optMarketInventoryService match {
            case Some(marketInventoryService) =>
                marketInventoryService.deleteMarketInventoryEntity(id)
            case None =>
                log.error("No MarketInventory Service")
                Future.successful(0)
        }
    }

    /** Delete a plant from the Database
      *
      * @param a the number of InventoryItems deleted should be 1
      * @param b the number of PlantItems deleted should be 1
      * @param c the number of MarketInventoryItems deleted should be 1
      * @return true if all numbers are 1
      */
    private def deletePlantInt ( a: Int, b: Int, c:Int ): Boolean = {
        log.debug("deletePlant - a: " +a+ " b: " +b+ " c: " + c)
        if ( a == 1 && b == 1 && c == 1) {
            true
        } else {
            false
        }
    }

    /** Remove this actor from Central
      *
      * @param id the id of the actorRef
      */
    private def removePlantFromCentral(id: String): Unit = {
        log.debug("removePlantFromCentral id: " + id)
        val deletedPlant = Central.plantActors.remove(plantEntity.id)
        deletedPlant match {
            case Some(_) =>
                self ! PoisonPill
            case None =>
                log.error("No Plant found in Central, but there should be one")
                self ! PoisonPill
        }
    }
}
