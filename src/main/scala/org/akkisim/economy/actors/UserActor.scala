/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{Actor, ActorRef}
import akka.http.scaladsl.{Http, HttpExt}
import akka.http.scaladsl.model._
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.economy.dataproviders.types._
import org.akkisim.economy.services.UserService
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}

/**
  * @author Akira Sonoda
  */
class UserActor(userService: UserService, avatarUUID: String, pushURL: String, central: ActorRef) extends Actor {
    val log: Logger = LoggerFactory.getLogger ( "UserActor" )

    import akka.pattern.pipe
    import context.dispatcher
    
    final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
    val http: HttpExt = Http(context.system)
    
    // Avatar States
    private var hungerCount: Int = 0
    private var thirstCount: Int = 0

    private var myPushUrl = pushURL
    private var errorResonseCount = 0
    private var avatarGetStatus = 0
    
    def receive: Receive = {
        case SendHeartbeat =>
            log.debug("SendMessage for UserActor: " + avatarUUID)
            // Every 10 Minutes the Thirst is increased by 1
            // Every 70 Minutes the Hununger is increased by 1
            // Heartbeat comes every Minute
            doHeartbeat()
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
            log.debug("Request successful")
            entity.discardBytes()
            errorResonseCount = 0
        case resp@HttpResponse(code, _, _, _) =>
            log.debug("Request failed, response code: " + code)
            errorResonseCount += 1
            if (errorResonseCount == 3) {
                log.debug("Updating Status")
                userService.userStatusUpdate(avatarUUID, hungerCount, thirstCount).onComplete {
                    case Success( _ ) => log.debug("UserUpdate Successful")
                    case Failure(ex) => log.error("User Update failed", ex)
                }
                resp.discardEntityBytes()
                central ! DeRegisterUser(avatarUUID)
            }
        case RegisterPushUrl(`avatarUUID`, pushUrl) =>
            log.debug("RegisterPushUrl pushUrl: " + pushUrl)
            myPushUrl = pushUrl
            evaluateNumbers()
        case StateUpdate(stateName,stateValue,stateModification) =>
            log.debug("Updating State: "+stateName+" with Value: "+stateValue+" with direction: "+stateModification )
            sender ! setUserState(stateName, stateValue, stateModification)
    }

    private def setUserState(statName: StateName.Value, stateValue: Int, stateModification: Modification.Value): AvatarStatus = {
        log.debug("setUserState")
        statName match {
            case StateName.HUNGER =>
                stateModification match {
                    case Modification.ADDITION =>
                        hungerCount += stateValue
                    case Modification.SUBTRACTION =>
                        hungerCount = subtractNotBelowZero(hungerCount,stateValue)
                }
            case StateName.THIRST =>
                stateModification match {
                    case Modification.ADDITION =>
                        thirstCount += stateValue
                    case Modification.SUBTRACTION =>
                        thirstCount = subtractNotBelowZero(thirstCount,stateValue)
                }
        }
        evaluateNumbers()
        AvatarStatus(hungerCount,thirstCount)
    }

    private def subtractNotBelowZero(currentValue: Int , modification: Int ): Int = {
        val newValue = currentValue - modification
        if (newValue<0) {
            0
        } else {
            newValue
        }
    }

    private def doHeartbeat(): Unit = {
        log.debug("doHeartbeat")
        if (avatarGetStatus == 0) {
            userService.getUserById(avatarUUID).onComplete {
                case Success(optUser) => optUser match {
                    case Some(user) =>
                        log.debug("getUserById(" + avatarUUID + ") successful")
                        user.avatarhunger match {
                            case Some(hunger) =>
                                hungerCount = hunger
                                log.debug("Hunger set to:" + hungerCount)
                            case None =>
                                hungerCount = 0
                                log.debug("Hunger set to:" + hungerCount)
                        }
                        user.avatarthirst match {
                            case Some(thirst) =>
                                thirstCount = thirst
                                log.debug("Thirst set to:" + thirstCount)
                            case None =>
                                thirstCount = 0
                                log.debug("Thirst set to:" + thirstCount)
                        }
                        evaluateNumbers()
                        avatarGetStatus += 1
                    case None =>
                }
                case Failure(ex) => log.error("getUserById failed", ex)
            }
        } else {
            if ((thirstCount % 10) == 0) {
                evaluateNumbers()
                thirstCount += 1
                hungerCount += 1
            } else {
                http.singleRequest(HttpRequest(uri = myPushUrl)).pipeTo(self)
                thirstCount += 1
                hungerCount += 1
            }
            
        }
        
    }
    
    private def evaluateNumbers(): Unit = {
        log.debug("ThirstCount Evaluation")
        val adjThirst = thirstCount / THiRST_DIVIDER
        val adjHunger = hungerCount / HUNGER_DIVIDER
        log.debug("Statistics: Thirst("+adjThirst+"/"+thirstCount+") - Hunger("+adjHunger+"/"+hungerCount+")")
        val avatarStatus: AvatarStatus = AvatarStatus(adjHunger, adjThirst)
        val requestEntity = HttpEntity(MediaTypes.`application/json`, avatarStatus.asJson.toString())
        http.singleRequest(HttpRequest(method = HttpMethods.POST, uri = myPushUrl, entity = requestEntity)).pipeTo(self)
    }
}
