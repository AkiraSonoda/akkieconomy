/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.actors

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import org.akkisim.economy.dataproviders.caches.Central
import org.akkisim.economy.dataproviders.types._

/**
  * @author Akira Sonoda
  */
class CentralActor extends Actor with ActorLogging {
    
    def receive: Receive = {
        case SendHeartbeat =>
            log.debug("SendHeartbeat")
            Central.userActors.values.foreach( actorRef => actorRef ! SendHeartbeat)
            Central.optPlantFactory match {
                case Some(plantFactoryActor) => plantFactoryActor ! SendHeartbeat
                case None => log.error("No Plant Factory Available")
            }
        case RegisterUser(avatarUUID: String, userRef: ActorRef) =>
            log.debug("RegisterUser - avatarUUID:"+avatarUUID)
            Central.userActors.get(avatarUUID) match {
                case Some(actorRef) =>
                    actorRef ! PoisonPill // We still have an active UserActor for the given User we remove it
                    Central.userActors(avatarUUID) = userRef
                    Central.optPlantFactory match {
                        case Some(plantFactoryActor) => plantFactoryActor ! RegisterPlantOfUser(avatarUUID)
                        case None => log.error("No Plant Factory Available")
                    }
                case None =>
                    Central.userActors += avatarUUID -> userRef
                    Central.optPlantFactory match {
                        case Some(plantFactoryActor) => plantFactoryActor ! RegisterPlantOfUser(avatarUUID)
                        case None => log.error("No Plant Factory Available")
                    }
            }
        case DeRegisterUser(avatarUUID) =>
            log.debug("DeRegisterUser - avatarUUID:"+avatarUUID)
            Central.userActors.get(avatarUUID) match {
                case Some(actorRef) =>
                    actorRef ! PoisonPill
                    Central.userActors.remove(avatarUUID)
                    Central.optPlantFactory match {
                        case Some(plantFactoryActor) => plantFactoryActor ! DeRegisterPlantOfUser(avatarUUID)
                        case None => log.error("No Plant Factory Available")
                    }
                case None => // Nothing to do
            }
        case RegisterPushUrl(avatarUUID: String, pushUrl: String) =>
            log.debug("RegisterPushUrl - avatarUUID:"+avatarUUID+" pushUrl: "+pushUrl)
            Central.userActors.get(avatarUUID) match {
                case Some(actorRef) =>
                    actorRef ! RegisterPushUrl(avatarUUID, pushUrl)
                case None => // Nothing to do
            }
    }
}
