package org.akkisim.economy.dataproviders

import akka.actor.ActorRef

/**
  * @author Akira Sonoda
  */
package object types {

    val UUID_ZERO = "00000000-0000-0000-0000-000000000000"
    val THiRST_DIVIDER = 10
    val HUNGER_DIVIDER = 70


    /**
      * Case Class to Update the User State... or possibly other States
      *
      * @param stateName enumeration StateName
      * @param stateValue integer value how much the state is modified
      * @param stateModification enumaration describing the state modification
      */
    final case class StateUpdate (
                                     stateName: StateName.Value,
                                     stateValue: Int,
                                     stateModification: Modification.Value
                                 )

    /**
      * TODO factor out
      */
    final object Modification extends Enumeration {
        val ADDITION: Modification.Value = Value ( 0 )
        val SUBTRACTION: Modification.Value = Value ( 1 )

        private def withNameOpt( s: String ): Option[Value] = values.find ( _.toString == s.toUpperCase )

        private def withIntOpt( i: Int ): Option[Value] = values.find ( _.id == i )

        def getStateModificationName( intStateModificationName: Int ): String = {
            val optStateModificationName = Modification.withIntOpt ( intStateModificationName )
            optStateModificationName match {
                case Some ( sn ) => sn.toString
                case None => "UNDEFINED"
            }
        }

        def getStateModificationName( stringStateModificationName: String ): Int = {
            val optStateModificationName = Modification.withNameOpt ( stringStateModificationName )
            optStateModificationName match {
                case Some ( sn ) => sn.id
                case None => -1
            }
        }

        def getStateModificationByName( stringStateModificationName: String): Value = {
            val optStateModificationValue = Modification.withNameOpt ( stringStateModificationName )
            optStateModificationValue match {
                case Some(value) => value
                case None => Value("UNDEFINED")
            }
        }

    }

    /**
      *
      */
    final object StateName extends Enumeration {
        val HUNGER: StateName.Value = Value ( 0 )
        val THIRST: StateName.Value = Value ( 1 )

        private def withNameOpt( s: String ): Option[Value] = values.find ( _.toString == s.toUpperCase )

        private def withIntOpt( i: Int ): Option[Value] = values.find ( _.id == i )

        def getStateName( intStateName: Int ): String = {
            val optStateName = StateName.withIntOpt ( intStateName )
            optStateName match {
                case Some ( sn ) => sn.toString
                case None => "UNDEFINED"
            }
        }

        def getStateName( stringStateName: String ): Int = {
            val optStateName = StateName.withNameOpt ( stringStateName )
            optStateName match {
                case Some ( sn ) => sn.id
                case None => -1
            }
        }

        def getStateByName( stringStateName: String): Value = {
            val optStateValue = StateName.withNameOpt ( stringStateName )
            optStateValue match {
                case Some(value) => value
                case None => Value("UNDEFINED")
            }
        }

    }

    /**
      * Case Clas describing the User Login Data
      *
      * @param avatarUUID
      * @param avatarName
      * @param password
      * @param newPassword
      */
    final case class LoginPassword(
                                      avatarUUID: String,
                                      avatarName: String,
                                      password: String,
                                      newPassword: String
                                  )

    /**
      *
      * @param url
      */
    final case class PushUrl(
                                url: String
                            )

    /**
      *
      * @param avatarhunger
      * @param avatarthirst
      */
    final case class AvatarStatus(
                                     avatarhunger: Int,
                                     avatarthirst: Int
                                 )

    /**
      *
      * @param avatarUUID
      * @param avatarhunger
      * @param avatarthirst
      */
    final case class AvatarStatusUpdate(
                                           avatarUUID: String,
                                           avatarhunger: Int,
                                           avatarthirst: Int
                                       )


    /**
      *
      */
    sealed trait HeartbeatMessage

    /**
      *
      */
    case object SendHeartbeat extends HeartbeatMessage

    // Message Sends a new UserActorref to CentralActor in order
    // to store the reference in its internal UserActorMap
    final case class RegisterUser( avatarUUID: String, actorRef: ActorRef )

    // Message Requests the CentralActor Actor to remove the Actor with the given
    // UUID from the UserActorMap
    final case class DeRegisterUser( avatarUUID: String )

    // Message Requests the CentralActor Actor to send a new pushUrl to
    // the UserActor identified by avatarUUID
    final case class RegisterPushUrl( avatarUUID: String, pushUrl: String )

    // Message Requests the PlantActor to register the Plants for a given User
    // the User is identified by avatarUUID
    final case class RegisterPlantOfUser ( avatarUUID: String )

    // Message Requests the PlantActor to deregister the Plants for a given User
    // the User is identified by avatarUUID
    final case class DeRegisterPlantOfUser( avatarUUID: String )

    // Message Requests the PlantActor to when the pushUrl changes
    final case class PlantPushUrl( url: String )

    // Message Requests the PlantActor to when the state has to change
    final case class StateChange()



    final object MeasureUnit extends Enumeration {
        val CENTILITER: MeasureUnit.Value = Value ( 0 )
        val DECILITER: MeasureUnit.Value = Value ( 1 )
        val LITER: MeasureUnit.Value = Value ( 2 )
        val GRAM: MeasureUnit.Value = Value ( 3 )
        val KILOGRAM: MeasureUnit.Value = Value ( 4 )
        val UNIT: MeasureUnit.Value = Value ( 999 )

        private def withNameOpt( s: String ): Option[Value] = values.find ( _.toString == s.toUpperCase )

        private def withIntOpt( i: Int ): Option[Value] = values.find ( _.id == i )

        def getMeasureUnit( intMeasureUnit: Int ): String = {
            val optMeasureUnit = MeasureUnit.withIntOpt ( intMeasureUnit )
            optMeasureUnit match {
                case Some ( mu ) => mu.toString
                case None => "UNDEFINED"
            }
        }

        def getMeasureUnit( stringMeasureUnit: String ): Int = {
            val optMeasureUnit = MeasureUnit.withNameOpt ( stringMeasureUnit )
            optMeasureUnit match {
                case Some ( mu ) => mu.id
                case None => -1
            }
        }

    }

}
