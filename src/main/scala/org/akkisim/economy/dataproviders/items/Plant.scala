/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.items

import org.akkisim.economy.dataproviders.types.UUID_ZERO

/**
  * Plant Case Class. used to create new Plants
  *
  * @author Akira Sonoda
  *
  * @param id uuid if the plant
  * @param ownerid uuid of the owner of the plant
  * @param name name of the plant
  * @param itemType inventory Item Type currently only PLANT
  * @param price price of the plant as integer
  * @param pushurl the url to send state changes to
  * @param plantType the type of plant: PLANT_VEGETABLE, PLANT_BUSH etc in stringified representation
  * @param plantState the state of the Plant can be PLANT_PRODUCING_FRUITS or PLANT_GROWING
  */
case class Plant (
                       id: String
                       , ownerid: String
                       , name: String
                       , itemType: String
                       , price: Int = 0
                       , pushurl: String
                       , plantType: String
                       , plantState: String
                       , origin: String = UUID_ZERO
                   )

