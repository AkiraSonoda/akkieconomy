package org.akkisim.economy.dataproviders.items

/** Entity class storing rows of table Prices
  *
  *  @author Akira Sonoda
  *
  *  @param kind Database column kind SqlType(CHAR), PrimaryKey, Length(50,false)
  *  @param price Database column price SqlType(INT)
  */
final case class Price(
                    kind: String,
                    price: Int,
                )


