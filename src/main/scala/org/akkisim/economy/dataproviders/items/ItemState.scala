package org.akkisim.economy.dataproviders.items

/**
  * @author Akira Sonoda
  */
object ItemState extends Enumeration {
    val UNDEFINED: ItemState.Value = Value ( 0 )
    val NEW: ItemState.Value = Value ( 1 )
    val ACTIVE: ItemState.Value = Value ( 2 )
    val INACTIVE: ItemState.Value = Value ( 3 )

    private def withNameOpt( s: String ): Option[Value] = values.find ( _.toString == s.toUpperCase )

    private def withIntOpt( i: Int ): Option[Value] = values.find ( _.id == i )

    def getItemState( intItemState: Int ): String = {
        val optItemState = ItemState.withIntOpt ( intItemState )
        optItemState match {
            case Some ( is ) => is.toString
            case None => "UNDEFINED"
        }
    }

    def getItemState( stringItemState: String ): Int = {
        val optItemState = ItemState.withNameOpt ( stringItemState )
        optItemState match {
            case Some ( is ) => is.id
            case None => 0
        }
    }


}
