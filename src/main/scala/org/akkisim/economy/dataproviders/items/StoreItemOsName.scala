package org.akkisim.economy.dataproviders.items

/** The OS representation of the StoreObject
  *
  * @author Akira Sonoda
  *
  * @param osItem the String of the OS Prim to give to the User
  */
final case class StoreItemOsName (
                            osItem: String
                           )

