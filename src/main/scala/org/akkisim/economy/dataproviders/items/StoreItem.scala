package org.akkisim.economy.dataproviders.items

/** The externa representation of the StoreEntity
  *
  * @author Akira Sonoda
  *
  * @param itemtype the string representation of the itemtype
  */
final case class StoreItem (
                           itemtype: String
                           )

