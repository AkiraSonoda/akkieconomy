package org.akkisim.economy.dataproviders.items

/**
  * @author Akira Sonoda
  */
object PlantState extends Enumeration {
    val UNDEFINED: PlantState.Value = Value ( 0 )
    val PLANT_GROWING: PlantState.Value = Value ( 1 )
    val PLANT_PRODUCING_FRUITS: PlantState.Value = Value ( 2 )
    val PLANT_DEAD: PlantState.Value = Value ( 3 )
    val PLANT_SICK: PlantState.Value = Value ( 4 )
    val PLANT_HARVEST: PlantState.Value = Value ( 5 )

    private def withNameOpt( s: String ): Option[Value] = values.find ( _.toString == s.toUpperCase )

    private def withIntOpt( i: Int ): Option[Value] = values.find ( _.id == i )

    def getPlantStateString( intPlantState: Int ): String = {
        val optPlantState = PlantState.withIntOpt ( intPlantState )
        optPlantState match {
            case Some ( is ) => is.toString
            case None => "UNDEFINED"
        }
    }

    def getPlantStateInt ( stringPlantState: String ): Int = {
        val optPlantState = PlantState.withNameOpt ( stringPlantState )
        optPlantState match {
            case Some ( is ) => is.id
            case None => UNDEFINED.id
        }
    }

    def getPlantStateStateValueByString ( stringPlantState: String ): PlantState.Value = {
        val optPlantState = PlantState.withNameOpt(stringPlantState)
        optPlantState match {
            case Some ( is ) => is
            case None => UNDEFINED
        }
    }
    def getPlantStateStateValueByInt ( intPlantState: Int ): PlantState.Value = {
        val optPlantState = PlantState.withIntOpt(intPlantState)
        optPlantState match {
            case Some ( is ) => is
            case None => UNDEFINED
        }
    }

}
