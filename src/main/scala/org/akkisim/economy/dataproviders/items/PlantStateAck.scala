package org.akkisim.economy.dataproviders.items

/**
  * @author Akira Sonoda
  */
object PlantStateAck extends Enumeration {
    val UNDEFINED:                       PlantStateAck.Value = Value ( 0 )
    val PLANT_GROWING_WAIT_ACK:          PlantStateAck.Value = Value ( 1 )
    val PLANT_GROWING_ACK:               PlantStateAck.Value = Value ( 2 )
    val PLANT_PRODUCING_FRUITS_WAIT_ACK: PlantStateAck.Value = Value ( 3 )
    val PLANT_PRODUCING_FRUITS_ACK:      PlantStateAck.Value = Value ( 4 )
    val PLANT_DEAD_WAIT_ACK:             PlantStateAck.Value = Value ( 5 )
    val PLANT_DEAD_ACK:                  PlantStateAck.Value = Value ( 6 )
    val PLANT_SICK_WAIT_ACK:             PlantStateAck.Value = Value ( 7 )
    val PLANT_SICK_ACK:                  PlantStateAck.Value = Value ( 8 )
    val PLANT_HARVEST_WAIT_ACK:          PlantStateAck.Value = Value ( 9 )
    val PLANT_HARVEST_ACK:               PlantStateAck.Value = Value ( 10 )

    private def withNameOpt( s: String ): Option[Value] = values.find ( _.toString == s.toUpperCase )

    private def withIntOpt( i: Int ): Option[Value] = values.find ( _.id == i )

    def getPlantStateAckString( intPlantStateAck: Int ): String = {
        val optPlantStateAck = PlantStateAck.withIntOpt ( intPlantStateAck )
        optPlantStateAck match {
            case Some ( is ) => is.toString
            case None => "UNDEFINED"
        }
    }

    def getPlantStateAckInt ( stringPlantStateAck: String ): Int = {
        val optPlantStateAck = PlantStateAck.withNameOpt ( stringPlantStateAck )
        optPlantStateAck match {
            case Some ( is ) => is.id
            case None => UNDEFINED.id
        }
    }

    def getPlantStateAckStateValueByString ( stringPlantStateAck: String ): PlantStateAck.Value = {
        val optPlantStateAck = PlantStateAck.withNameOpt(stringPlantStateAck)
        optPlantStateAck match {
            case Some ( is ) => is
            case None => UNDEFINED
        }
    }
    def getPlantStateAckStateValueByInt ( intPlantStateAck: Int ): PlantStateAck.Value = {
        val optPlantStateAck = PlantStateAck.withIntOpt(intPlantStateAck)
        optPlantStateAck match {
            case Some ( is ) => is
            case None => UNDEFINED
        }
    }

}
