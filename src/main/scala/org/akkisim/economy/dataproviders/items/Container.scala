/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.items

import org.akkisim.economy.dataproviders.types.UUID_ZERO

/**
  * Container Case Class. The unit of the volume is interpreted by the caller.
  * Can be litres, gallons whatever
  *
  * @author Akira Sonoda
  *
  * @param id
  * @param ownerid
  * @param name
  * @param itemType
  * @param price
  * @param maxVolume
  * @param currentVolume
  * @param measureUnit
  */
case class Container (
                       id: String
                       , ownerid: String
                       , name: String
                       , itemType: String
                       , price: Int = 0
                       , maxVolume: Int = 0
                       , currentVolume: Int = 0
                       , measureUnit: String
                       , origin: String = UUID_ZERO
                   )

