package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService

/**
  * @author Akira Sonoda
  */
class StoreEntityTable extends DatabaseService {
    import driver.api._


    /** Table description of table store. Objects of this class serve as prototypes for rows in queries. */
    class StoreTable(_tableTag: Tag) extends Table[StoreEntity](_tableTag, "store") {
        def * = (storeid, itemtype, quantity) <> (StoreEntity.tupled, StoreEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(storeid), Rep.Some(itemtype), Rep.Some(quantity)).shaped.<>({r=>import r._; _1.map(_=> StoreEntity.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column storeid SqlType(CHAR), Length(36,false) */
        val storeid: Rep[String] = column[String]("storeid", O.Length(36,varying=false))
        /** Database column itemtype SqlType(INT) */
        val itemtype: Rep[Int] = column[Int]("itemtype")
        /** Database column quantity SqlType(INT) */
        val quantity: Rep[Int] = column[Int]("quantity")

        /** Index over (itemtype) (database name itemtype_idx) */
        val index1 = index("itemtype_idx", itemtype)
        /** Index over (storeid) (database name storeid_idx) */
        val index2 = index("storeid_idx", storeid)
    }
    /** Collection-like TableQuery object for table Store */
    lazy val StoreTable = new TableQuery(tag => new StoreTable(tag))

}
