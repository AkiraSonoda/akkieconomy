/*
 * opyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService

/**
  * @author Akira Sonoda
  */
class UserEntityTable extends DatabaseService {
    import driver.api._

    /** Table description of table user. Objects of this class serve as prototypes for rows in queries. */
    class User(tag: Tag) extends Table[UserEntity](tag, "user") {
        def * = (avataruuid, avatarname, avatarpassword, avatarthirst, avatarhunger, avatarlevel) <> (UserEntity.tupled, UserEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(avataruuid), Rep.Some(avatarname), Rep.Some(avatarpassword), avatarthirst, avatarhunger, avatarlevel).shaped.<>({r=>import r._; _1.map(_=> UserEntity.tupled((_1.get, _2.get, _3.get, _4, _5, _6)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column avatarUUID SqlType(CHAR), PrimaryKey, Length(36,false) */
        val avataruuid: Rep[String] = column[String]("avatarUUID", O.PrimaryKey, O.Length(36,varying=false))
        /** Database column avatarName SqlType(VARCHAR), Length(128,true) */
        val avatarname: Rep[String] = column[String]("avatarName", O.Length(128,varying=true))
        /** Database column avatarPassword SqlType(VARCHAR), Length(128,true) */
        val avatarpassword: Rep[String] = column[String]("avatarPassword", O.Length(128,varying=true))
        /** Database column avatarThirst SqlType(INT), Default(Some(0)) */
        val avatarthirst: Rep[Option[Int]] = column[Option[Int]]("avatarThirst", O.Default(Some(0)))
        /** Database column avatarHunger SqlType(INT), Default(Some(0)) */
        val avatarhunger: Rep[Option[Int]] = column[Option[Int]]("avatarHunger", O.Default(Some(0)))
        /** Database column avatarLevel SqlType(INT), Default(Some(0)) */
        val avatarlevel: Rep[Option[Int]] = column[Option[Int]]("avatarLevel", O.Default(Some(0)))
    }
    /** Collection-like TableQuery object for table UserActor */
    lazy val User = new TableQuery(tag => new User(tag))

}
