/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

/** Entity class storing rows of table Inventory
  *  @param itemid Database column itemId SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param itemownerid Database column itemOwnerId SqlType(CHAR), Length(36,false)
  *  @param itemname Database column itemName SqlType(VARCHAR), Length(80,true)
  *  @param itemtype Database column itemType SqlType(INT), Default(0)
  *  @param itemprice Database column itemPrice SqlType(INT), Default(0)
  *  @param itemstate Database column itemState SqlType(INT), Default(0) */
case class InventoryEntity(itemid: String,
                           itemownerid: String,
                           itemname: String,
                           itemtype: Int = 0,
                           itemprice: Int = 0,
                           itemstate: Int = 0
                          )

final case class InventoryEntityUpdate(itemid: String,
                                       itemownerid: String,
                                       itemname: String,
                                       itemtype: Int = 0,
                                       itemprice: Int = 0,
                                       itemstate: Int = 0
                                      ) {
    def merge(inventoryEntity: InventoryEntity): InventoryEntity = {
        InventoryEntity(itemid, itemownerid, itemname, itemtype, itemprice, itemstate)
    }
}
