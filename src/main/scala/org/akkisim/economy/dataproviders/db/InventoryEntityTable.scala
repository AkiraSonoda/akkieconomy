/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService

/**
  * @author Akira Sonoda
  */
class InventoryEntityTable extends DatabaseService {
    import driver.api._

    /** Table description of table inventory. Objects of this class serve as prototypes for rows in queries. */
    class Inventory(_tableTag: Tag) extends Table[InventoryEntity](_tableTag, "inventory") {
        def * = (itemid, itemownerid, itemname, itemtype, itemprice, itemstate) <>
            (InventoryEntity.tupled, InventoryEntity.unapply)

        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(itemid),
            Rep.Some(itemownerid), Rep.Some(itemname),
            Rep.Some(itemtype), Rep.Some(itemprice),
            Rep.Some(itemstate)).shaped.<>(
            {r=>import r._; _1.map(_=> InventoryEntity.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))},
            (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column itemId SqlType(CHAR), PrimaryKey, Length(36,false) */
        val itemid: Rep[String] = column[String]("itemId", O.PrimaryKey, O.Length(36,varying=false))
        /** Database column itemOwnerId SqlType(CHAR), Length(36,false) */
        val itemownerid: Rep[String] = column[String]("itemOwnerId", O.Length(36,varying=false))
        /** Database column itemName SqlType(VARCHAR), Length(80,true) */
        val itemname: Rep[String] = column[String]("itemName", O.Length(80,varying=true))
        /** Database column itemType SqlType(INT), Default(0) */
        val itemtype: Rep[Int] = column[Int]("itemType", O.Default(0))
        /** Database column itemPrice SqlType(INT), Default(0) */
        val itemprice: Rep[Int] = column[Int]("itemPrice", O.Default(0))
        /** Database column itemState SqlType(INT), Default(0) */
        val itemstate: Rep[Int] = column[Int]("itemState", O.Default(0))

        /** Index over (itemownerid) (database name idx_inventory_itemOwnerId) */
        val index1 = index("idx_inventory_itemOwnerId", itemownerid)
    }
    /** Collection-like TableQuery object for table Inventory */
    lazy val Inventory = new TableQuery(tag => new Inventory(tag))


}
