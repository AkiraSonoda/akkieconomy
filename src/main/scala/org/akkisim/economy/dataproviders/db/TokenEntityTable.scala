/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService


/** Table description of table token. Objects of this class serve as prototypes for rows in queries.
  *
  * @author Akira Sonoda
  */
trait TokenEntityTable extends UserEntityTable {

  protected val databaseService: DatabaseService
  import driver.api._

  class Token(tag: Tag) extends Table[TokenEntity](tag, "token") {
    def * = (token, avataruuid) <> (TokenEntity.tupled, TokenEntity.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(token), Rep.Some(avataruuid)).shaped.<>({ r => import r._; _1.map(_ => TokenEntity.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column token SqlType(CHAR), PrimaryKey, Length(36,false) */
    val token: Rep[String] = column[String]("token", O.PrimaryKey, O.Length(36, varying = false))
    /** Database column avatarUUID SqlType(CHAR), Length(36,false) */
    val avataruuid: Rep[String] = column[String]("avatarUUID", O.Length(36, varying = false))
  }

  /** Collection-like TableQuery object for table Token */
  lazy val Token = new TableQuery(tag => new Token(tag))
}
