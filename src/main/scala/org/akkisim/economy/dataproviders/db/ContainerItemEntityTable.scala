/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService
import slick.lifted.{MappedProjection, ProvenShape}

/**
  * @author Akira Sonoda
  */
class ContainerItemEntityTable  extends DatabaseService {
    import driver.api._

    /** Table description of table container_item. Objects of this class serve as prototypes for rows in queries. */
    class ContainerItem(_tableTag: Tag) extends Table[ContainerItemEntity](_tableTag, "container_item") {
        def * = (id, maxvolume, currentvolume, measureunit) <>
            (ContainerItemEntity.tupled, ContainerItemEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(id), Rep.Some(maxvolume), Rep.Some(currentvolume), Rep.Some(measureunit))
            .shaped.<>({r=>import r._; _1.map(_=> ContainerItemEntity.tupled((_1.get, _2.get, _3.get, _4.get)))},
            (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column id SqlType(CHAR), PrimaryKey, Length(36,false) */
        val id: Rep[String] = column[String]("id", O.PrimaryKey, O.Length(36,varying=false))
        /** Database column maxVolume SqlType(INT), Default(0) */
        val maxvolume: Rep[Int] = column[Int]("maxVolume", O.Default(0))
        /** Database column currentVolume SqlType(INT), Default(0) */
        val currentvolume: Rep[Int] = column[Int]("currentVolume", O.Default(0))
        /** Database column measureUnit SqlType(INT), Default(0) */
        val measureunit: Rep[Int] = column[Int]("measureUnit", O.Default(0))
    }
    /** Collection-like TableQuery object for table ContainerItem */
    lazy val ContainerItem = new TableQuery(tag => new ContainerItem(tag))

}
