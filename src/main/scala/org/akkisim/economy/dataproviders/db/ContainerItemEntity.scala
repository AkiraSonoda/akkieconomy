/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.dataproviders.items.ContainerUpdate

import scala.concurrent.Future


/** Entity class storing rows of table ContainerItem
  *
  *  @param id Database column id SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param maxvolume Database column maxVolume SqlType(INT), Default(0)
  *  @param currentvolume Database column currentVolume SqlType(INT), Default(0)
  *  @param measureunit Database column measureUnit SqlType(INT), Default(0) */
final case class ContainerItemEntity(id: String, maxvolume: Int = 0, currentvolume: Int = 0, measureunit: Int = 0)

