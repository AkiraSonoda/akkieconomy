/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

/**
  * @author Akira Sonoda
  */

/** Entity class storing rows of table UserActor
  *
  *  @author Akira Sonoda
  *
  *  @param avataruuid Database column avatarUUID SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param avatarname Database column avatarName SqlType(VARCHAR), Length(128,true)
  *  @param avatarpassword Database column avatarPassword SqlType(VARCHAR), Length(128,true)
  *  @param avatarthirst Database column avatarThirst SqlType(INT), Default(Some(0))
  *  @param avatarhunger Database column avatarHunger SqlType(INT), Default(Some(0))
  *  @param avatarlevel Database column avatarLevel SqlType(INT), Default(Some(0))
  *
  */
final case class UserEntity(
                             avataruuid: String,
                             avatarname: String,
                             avatarpassword: String,
                             avatarthirst: Option[Int] = Some(0),
                             avatarhunger: Option[Int] = Some(0),
                             avatarlevel: Option[Int] = Some(0)
                           )

final case class UserEntityUpdate(
                             avataruuid: String,
                             avatarname: String,
                             avatarpassword: String,
                             avatarthirst: Option[Int] = Some(0),
                             avatarhunger: Option[Int] = Some(0),
                             avatarlevel: Option[Int] = Some(0)
                           ) {
  def merge(userEntity: UserEntity): UserEntity = {
      UserEntity(avataruuid,avatarname,avatarpassword,avatarthirst,avatarhunger,avatarlevel)
  }
}
