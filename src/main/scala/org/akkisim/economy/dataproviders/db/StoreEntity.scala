package org.akkisim.economy.dataproviders.db

/** Entity class storing rows of table Store
  *
  *  @author Akira Sonoda
  *
  *  @param storeid Database column storeid SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param itemtype Database column itemtype SqlType(INT)
  *  @param quantity Database column quantity SqlType(INT)
  */
final  case class StoreEntity(
                              storeid: String
                              , itemtype: Int
                              , quantity: Int)


/** StoreEntity Update only adds the quantity
  *
  *  @param storeid Database column storeid SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param itemtype Database column itemtype SqlType(INT)
  *  @param quantity Database column quantity SqlType(INT)
  */
final case class StoreEntityUpdate(
                                      storeid: String
                                      , itemtype: Int
                                      , quantity: Int
                                 ) {
    def add ( newquantity: Int): StoreEntity = {
        StoreEntity(
            storeid = storeid
            , itemtype = itemtype
            , quantity = quantity + newquantity
        )
    }
    def subtract ( newquantity: Int): StoreEntity = {
        StoreEntity(
            storeid = storeid
            , itemtype = itemtype
            , quantity = quantity - newquantity
        )
    }
}

