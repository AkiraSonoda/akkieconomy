package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.dataproviders.types.UUID_ZERO

/**
  * @author Akira Sonoda
  *
  * Entity class storing rows of table Marketinventory
  *
  * @param id Database column id SqlType(CHAR), PrimaryKey, Length(36,false)
  * @param ownerid Database column ownerId SqlType(CHAR), Length(36,false)
  * @param itemType the Item Type
  * @param origin the originating Prim UUID
  */
final case class MarketInventory(
                                    id: String
                                    , ownerid: String
                                    , itemtype: String
                                    , origin: String = UUID_ZERO
                                )
