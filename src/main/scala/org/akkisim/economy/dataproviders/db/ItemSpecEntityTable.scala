package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService

/**
  * @author Akira Sonoda
  */
class ItemSpecEntityTable extends DatabaseService {
    import driver.api._

    /** Table description of table itemspec. Objects of this class serve as prototypes for rows in queries. */
    class ItemSpecTable(_tableTag: Tag) extends Table[ItemSpecEntity](_tableTag, "itemspec") {
        def * = (kind, id, price, storageQuantity, growthtime, cycletime, numcycles, osObjectname) <> (ItemSpecEntity.tupled, ItemSpecEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(kind), Rep.Some(id), Rep.Some(price), Rep.Some(storageQuantity), Rep.Some(growthtime), Rep.Some(cycletime), Rep.Some(numcycles), Rep.Some(osObjectname)).shaped.<>({r=>import r._; _1.map(_=> ItemSpecEntity.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column kind SqlType(CHAR), PrimaryKey, Length(50,false) */
        val kind: Rep[String] = column[String]("kind", O.PrimaryKey, O.Length(50,varying=false))
        /** Database column id SqlType(INT) */
        val id: Rep[Int] = column[Int]("id")
        /** Database column price SqlType(INT) */
        val price: Rep[Int] = column[Int]("price")
        /** Database column storage_quantity SqlType(INT) */
        val storageQuantity: Rep[Int] = column[Int]("storage_quantity")
        /** Database column growthtime SqlType(INT), Default(0) */
        val growthtime: Rep[Int] = column[Int]("growthtime", O.Default(0))
        /** Database column cycletime SqlType(INT), Default(0) */
        val cycletime: Rep[Int] = column[Int]("cycletime", O.Default(0))
        /** Database column numcycles SqlType(INT), Default(0) */
        val numcycles: Rep[Int] = column[Int]("numcycles", O.Default(0))
        /** Database column os_objectname SqlType(VARCHAR), Length(50,true), Default() */
        val osObjectname: Rep[String] = column[String]("os_objectname", O.Length(50,varying=true), O.Default(""))
    }
    /** Collection-like TableQuery object for table Itemspec */
    lazy val ItemSpecTable = new TableQuery(tag => new ItemSpecTable(tag))

}
