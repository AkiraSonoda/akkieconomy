/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.economy.dataproviders.db

import java.util.UUID

/** Entity class storing rows of table Token
  *
  *  @author Akira Sonoda
  *  @param token Database column token SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param avataruuid Database column avatarUUID SqlType(CHAR), Length(36,false)
  *
  */
case class TokenEntity(
                        token: String = UUID.randomUUID().toString.replaceAll("-", ""),
                        avataruuid: String
                      )
