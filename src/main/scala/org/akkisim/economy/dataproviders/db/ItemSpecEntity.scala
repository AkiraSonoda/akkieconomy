package org.akkisim.economy.dataproviders.db

/** Entity class storing rows of table Prices
  *
  *  @author Akira Sonoda
  *
  *  @param kind Database column kind SqlType(CHAR), PrimaryKey, Length(50,false)
  *  @param id Database column id SqlType(INT)
  *  @param price Database column price SqlType(INT)
  *  @param storageQuantity Database column storage_quantity SqlType(INT)
  *  @param growthtime Database column growthtime SqlType(INT), Default(0)
  *  @param cycletime Database column cycletime SqlType(INT), Default(0)
  *  @param numcycles Database column numcycles SqlType(INT), Default(0)
  *  @param osObjectname Database column os_objectname SqlType(VARCHAR), Length(50,true), Default() */
final case class ItemSpecEntity(
                          kind: String
                          , id: Int
                          , price: Int
                          , storageQuantity: Int
                          , growthtime: Int = 0
                          , cycletime: Int = 0
                          , numcycles: Int = 0
                          , osObjectname: String = ""
                         )


final case class ItemSpecEntityUpdate (
                                          kind: String
                                          , id: Int
                                          , price: Int
                                          , storageQuantity: Int
                                          , growthtime: Int = 0
                                          , cycletime: Int = 0
                                          , numcycles: Int = 0
                                          , osObjectname: String = ""
                                      ) {
    def merge( ): ItemSpecEntity = {
        ItemSpecEntity(
            kind
            , id
            , price
            , storageQuantity
            , growthtime
            , cycletime
            , numcycles
            , osObjectname
        )
    }
}