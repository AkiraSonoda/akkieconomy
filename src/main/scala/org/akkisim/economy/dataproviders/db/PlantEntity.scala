package org.akkisim.economy.dataproviders.db

/**
  * Entity class storing rows of table Plant
  *
  *  @author Akira Sonoda
  *
  *  @param id Database column id SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param planttype Database column plantType SqlType(INT)
  *  @param plantstate Database column plantState SqlType(INT)
  *  @param plantstateack Database column plantStateAck SqlType(INT), Default(0)
  *  @param growthpercent Database column growthPercent SqlType(INT), Default(0)
  *  @param maxnofruits Database column maxNoFruits SqlType(INT), Default(20)
  *  @param productioncycles Database column productionCycles SqlType(INT), Default(0)
  *  @param heartbeatcycles Database column heartbeatCycles SqlType(INT), Default(0)
  *  @param pushurl Database column pushUrl SqlType(VARCHAR), Length(100,true) */
final case class PlantEntity(
                          id: String
                          , planttype: Int
                          , plantstate: Int
                          , plantstateack: Int = 0
                          , growthpercent: Int = 0
                          , maxnofruits: Int = 20
                          , productioncycles: Int = 0
                          , heartbeatcycles: Int = 0
                          , pushurl: String
                      )

final case class PlantEntityUpdate(
                                      id: String
                                      , planttype: Int
                                      , plantstate: Int
                                      , plantstateack: Int = 0
                                      , growthpercent: Int = 0
                                      , maxnofruits: Int = 20
                                      , productioncycles: Int = 0
                                      , heartbeatcycles: Int = 0
                                      , pushurl: String

                                  ) {
    def merge(): PlantEntity = {
        PlantEntity(
            id
            ,planttype
            ,plantstate
            ,plantstateack
            ,growthpercent
            ,maxnofruits
            ,productioncycles
            ,heartbeatcycles
            ,pushurl
        )
    }
}
