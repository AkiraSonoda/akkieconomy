package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService

/**
  * @author Akira Sonoda
  */
class MarketInventoryEntityTable extends DatabaseService {
    import driver.api._

    /** Table description of table marketinventory. Objects of this class serve as prototypes for rows in queries. */
    class MarketInventoryTable(_tableTag: Tag) extends Table[MarketInventoryEntity](_tableTag, "marketinventory") {
        def * = (intid, id, ownerid, itemtype, origin) <> (MarketInventoryEntity.tupled, MarketInventoryEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(intid), Rep.Some(id), Rep.Some(ownerid), Rep.Some(itemtype), Rep.Some(origin)).shaped.<>({r=>import r._; _1.map(_=> MarketInventoryEntity.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column intid SqlType(CHAR), PrimaryKey, Length(36,false) */
        val intid: Rep[String] = column[String]("intid", O.PrimaryKey, O.Length(36,varying=false))
        /** Database column id SqlType(CHAR), Length(36,false) */
        val id: Rep[String] = column[String]("id", O.Length(36,varying=false))
        /** Database column ownerId SqlType(CHAR), Length(36,false) */
        val ownerid: Rep[String] = column[String]("ownerId", O.Length(36,varying=false))
        /** Database column itemType SqlType(INT) */
        val itemtype: Rep[Int] = column[Int]("itemType")
        /** Database column origin SqlType(CHAR), Length(36,false), Default(00000000-0000-0000-0000-000000000000) */
        val origin: Rep[String] = column[String]("origin", O.Length(36,varying=false), O.Default("00000000-0000-0000-0000-000000000000"))

        /** Index over (id) (database name index_Id) */
        val index1 = index("index_Id", id)
        /** Index over (itemtype) (database name index_ItemType) */
        val index2 = index("index_ItemType", itemtype)
        /** Index over (ownerid) (database name index_OwnerId) */
        val index3 = index("index_OwnerId", ownerid)
    }
    /** Collection-like TableQuery object for table Marketinventory */
    lazy val MarketInventoryTable = new TableQuery(tag => new MarketInventoryTable(tag))

}
