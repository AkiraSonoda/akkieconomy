package org.akkisim.economy.dataproviders.db

import org.akkisim.economy.services.DatabaseService
/**
  * @author Akira Sonoda
  */
class PlantEntityTable extends DatabaseService {
    import driver.api._

    /** Table description of table plant. Objects of this class serve as prototypes for rows in queries. */
    class PlantTable(_tableTag: Tag) extends Table[PlantEntity](_tableTag,"plant") {
        def * = (id, planttype, plantstate, plantstateack, growthpercent, maxnofruits, productioncycles, heartbeatcycles, pushurl) <> (PlantEntity.tupled, PlantEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? = (Rep.Some(id), Rep.Some(planttype), Rep.Some(plantstate), Rep.Some(plantstateack), Rep.Some(growthpercent), Rep.Some(maxnofruits), Rep.Some(productioncycles), Rep.Some(heartbeatcycles), Rep.Some(pushurl)).shaped.<>({r=>import r._; _1.map(_=> PlantEntity.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column id SqlType(CHAR), PrimaryKey, Length(36,false) */
        val id: Rep[String] = column[String]("id", O.PrimaryKey, O.Length(36,varying=false))
        /** Database column plantType SqlType(INT) */
        val planttype: Rep[Int] = column[Int]("plantType")
        /** Database column plantState SqlType(INT) */
        val plantstate: Rep[Int] = column[Int]("plantState")
        /** Database column plantStateAck SqlType(INT), Default(0) */
        val plantstateack: Rep[Int] = column[Int]("plantStateAck", O.Default(0))
        /** Database column growthPercent SqlType(INT), Default(0) */
        val growthpercent: Rep[Int] = column[Int]("growthPercent", O.Default(0))
        /** Database column maxNoFruits SqlType(INT), Default(20) */
        val maxnofruits: Rep[Int] = column[Int]("maxNoFruits", O.Default(20))
        /** Database column productionCycles SqlType(INT), Default(0) */
        val productioncycles: Rep[Int] = column[Int]("productionCycles", O.Default(0))
        /** Database column heartbeatCycles SqlType(INT), Default(0) */
        val heartbeatcycles: Rep[Int] = column[Int]("heartbeatCycles", O.Default(0))
        /** Database column pushUrl SqlType(VARCHAR), Length(100,true) */
        val pushurl: Rep[String] = column[String]("pushUrl", O.Length(100,varying=true))
    }
    /** Collection-like TableQuery object for table Plant */
    lazy val PlantTable = new TableQuery(tag => new PlantTable(tag))
}
