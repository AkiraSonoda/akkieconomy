package org.akkisim.economy.dataproviders.db

/**
  *  Entity class storing rows of table Marketinventory
  *  @author Akira Sonoda
  *
  *  @param intid Database column intid SqlType(CHAR), PrimaryKey, Length(36,false)
  *  @param id Database column id SqlType(CHAR), Length(36,false)
  *  @param ownerid Database column ownerId SqlType(CHAR), Length(36,false)
  *  @param itemtype Database column itemType SqlType(INT)
  *  @param origin Database column origin SqlType(CHAR), Length(36,false), Default(00000000-0000-0000-0000-000000000000) */
case class MarketInventoryEntity(
                                 intid: String
                                 , id: String
                                 , ownerid: String
                                 , itemtype: Int
                                 , origin: String = "00000000-0000-0000-0000-000000000000"
                                )

final case class MarketInventoryEntityUpdate(
                                                id: String
                                                , ownerid: String
                                                , itemtype: Int
                                                , origin: String = "00000000-0000-0000-0000-000000000000"
                                            ) {
    def merge(marketInventoryEntity: MarketInventoryEntity): MarketInventoryEntity = {
        MarketInventoryEntity(marketInventoryEntity.intid, id, ownerid,itemtype, origin)
    }
}