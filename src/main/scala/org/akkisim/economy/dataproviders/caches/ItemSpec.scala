package org.akkisim.economy.dataproviders.caches
import akka.util.Timeout
import org.akkisim.economy.dataproviders.db.ItemSpecEntity
import org.akkisim.economy.services.ItemSpecService
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
object ItemSpec {
    val log: Logger = LoggerFactory.getLogger("ItemSpec")

    private val itemSpecByString: mutable.Map[String, ItemSpecEntity] = mutable.Map[String, ItemSpecEntity]()
    private val itemSpecByInt: mutable.Map[Int, ItemSpecEntity] = mutable.Map[Int, ItemSpecEntity]()

    implicit val timeout: Timeout = Timeout(5 seconds)
    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global

    /** Initializes the Cache. Will read all ItemSpecEntities from the Database
      *
      * @param itemSpecService the ItemSpecService to get the ItemSpecEntities from
      * @return true if everything is ok.
      */
    def initialize(itemSpecService: ItemSpecService): Boolean = {
        log.debug("initialize")
        val itemSpecFuture = itemSpecService.getItemSpec
        val result: Try[Seq[ItemSpecEntity]] = Await.ready(itemSpecFuture, 10 seconds).value.get
        result match {
            case Success(items) =>
                items.foreach(
                    item => {
                        itemSpecByString += item.kind -> item
                        itemSpecByInt += item.id -> item
                    }
                )
                true
            case Failure ( exception ) =>
                log.error("Failed to initialize", exception)
                false
        }
    }

    /** Adds an ItemSpecEntity to the cache
      *
      * @param itemSpec the ItemSpec Entity
      */
    def addToCache(itemSpec: ItemSpecEntity): Unit = {
        // Check if the item is already there
        val result: Option[ItemSpecEntity] = itemSpecByString.get(itemSpec.kind.toUpperCase())
        result match {
            case Some(_) => //
                itemSpecByInt.remove(itemSpec.id)
                itemSpecByString.remove(itemSpec.kind)
                itemSpecByString += itemSpec.kind -> itemSpec
                itemSpecByInt += itemSpec.id -> itemSpec
            case None =>
                itemSpecByString += itemSpec.kind -> itemSpec
                itemSpecByInt += itemSpec.id -> itemSpec
        }
    }

    /** Returns the id of the ItemSpecEntity
      *
      * @param kind String kind of ItemSpec
      * @return the Integer id of the ItemSpecEntity
      */
    def getIntOfKind(kind:String): Int = {
        val result = itemSpecByString.get(kind.toUpperCase())
        result match {
            case Some(is) => is.id
            case None => 0
        }
    }

    /** Returns the kind of the ItemSpecEntity
      *
      * @param id Integer Id of ItemSpec
      * @return the kind String of the ItemSpecEntity
      */
    def getKindOfInt(id: Int): String = {
        val result = itemSpecByInt.get(id)
        result match {
            case Some(is) => is.kind.toUpperCase
            case None => "UNDEFINED"
        }
    }

    /** Returns the capacity of the ItemSpecEntity
      *
      * @param id Integer Id of ItemSpec
      * @return the capacity of the ItemSpecEntity. -1 if not found
      */
    def getCapacity(id: Int): Int = {
        val result = itemSpecByInt.get(id)
        result match {
            case Some(is) => is.storageQuantity
            case None => -1
        }
    }

    /** Returns the capacity of the ItemSpecEntity
      *
      * @param kind kind of ItemSpec
      * @return the capacity of the ItemSpecEntity. -1 if not found
      */
    def getCapacity(kind: String): Int = {
        val result = itemSpecByString.get(kind)
        result match {
            case Some(is) => is.storageQuantity
            case None => -1
        }
    }

    /** Returns the OpenSim Name of the Object that represents the ItemSpecEntity
      *
      * @param id the id of the ItemSpec
      * @return the OpenSim Name of the ItemSpecEntity
      */
    def getOsObjectName(id: Int): String = {
        val result = itemSpecByInt.get(id)
        result match {
            case Some(is) => is.osObjectname
            case None => ""
        }
    }

    /** Returns the OpenSim Name of the Object that represents the ItemSpecEntity
      *
      * @param kind the kind of the ItemSpec
      * @return the OpenSim Name of the ItemSpecEntity
      */
    def getOsObjectName(kind: String): String = {
        val result = itemSpecByString.get(kind)
        result match {
            case Some(is) => is.osObjectname
            case None => ""
        }
    }

    /** Returns the number of cycles for the given kind of the ItemSpecEntity
      *
      * @param kind the kind of the ItemSpec
      * @return the number cof cycles for the given ItemSpecEntity kind
      */
    def getCycles(kind: String): Int = {
        val result = itemSpecByString.get(kind)
        result match {
            case Some(is) => is.numcycles
            case None => 0
        }
    }

    /** Returns the number of cycles for the given id of the ItemSpecEntity
      *
      * @param id the id of the ItemSpec
      * @return the number cof cycles for the given ItemSpecEntity kind
      */
    def getCycles(id: Int): Int = {
        val result = itemSpecByInt.get(id)
        result match {
            case Some(is) => is.numcycles
            case None => 0
        }
    }

    /** Returns the cycletime for the given id of the ItemSpecEntity
      *
      * @param id the id of the ItemSpec
      * @return the cycletime for the given ItemSpecEntity kind
      */
    def getCycleTime(id: Int): Int = {
        val result = itemSpecByInt.get(id)
        result match {
            case Some(is) => is.cycletime
            case None => 0
        }
    }

}
