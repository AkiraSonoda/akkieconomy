package org.akkisim.economy.dataproviders.caches

import akka.actor.ActorRef
import org.akkisim.economy.services.{InventoryService, MarketInventoryService, PlantService, UserService}

import scala.collection.mutable.Map

/**
  * @author Akira Sonoda
  */
object Central {
    // Service References
    var optPlantService: Option[PlantService] = None
    var optInventoryService: Option[InventoryService] = None
    var optMarketInventoryService: Option[MarketInventoryService] = None
    var optUserService: Option[UserService] = None

    // Actor References
    var optCentral: Option[ActorRef] = None
    var optPlantFactory: Option[ActorRef] = None

    // ActorCollections
    val userActors: Map[String, ActorRef] = Map[String, ActorRef]()
    val plantActors: Map[String, ActorRef] = Map[String, ActorRef]()
}
