# akkieconomy
An Economy for virtual Worlds

## Description
This system will give your Avatar some more aspects of life. 
Your Avatar will suddenly be thirsty and hungry he even can get sick.
In order to make the feeling of hunger disappear, you will need food.
You will also need to consume enough liquid in order to stay healthy. 

You certainly can drink water from a nearby river but are you sure it is not
polluted somehow? Yes you can cook water before you drink it. Or you just buy 
fresh water on the market. The same applies to food. You can get the ingredients
from the market or eat fully prepared meals in a nearby restaurant. 

But as long as you don't plant vegetables in your garden and grow chicken on your fields,
you possibly need some money to purchase items from the market, restaurants and other locations. 

And this is where the circle begins. How to make some money. You can do it by:

* being a farmer
* being a shop owner
* being a producer of items
* working somewhere
* etc.

There will be many many possibilites to create an income and it possibly will increas over time 
allowing you to even get more ressources

## Getting Started
Actually there is not more available than a broad idea of the system

## Installing
Actually there is not more available than a broad idea of the system

## Built With

The system will be built with

* scala - programming language
* sbt - build system
* akka-http - transport
* akka - actor system
* slick - database layer
* mysql - database

The runtime environment will be a container like docker.

There will also some 

* lsl - (linden lab scripting language) scripts be part of the solution and 
* plugin - possibly an OpenSim/Arriba Plugin

## Versioning

We use [Bitbucket](https://bitbucket.org/AkiraSonoda/akkieconomy/src/develop/) for versioning.

## RoadMap
Milestones will be listed here:

* V0.001 - DONE: UserAccounts for the Players
* V0.002 - DONE: HUD and Avatar Tag displaying the health of the Avatar
* V0.003 - DONE: Chilli con Carne Menu, Bottle of Singha Beer, Palm Tree producing Coconuts, Sorage (for Coconuts), Buyer (which buys e.g. baskets of Coconuts)
* V0.004 - DONE: Implement Heart as a Display for the current Avatar State
* V0.005 - DONE: Plant Growth and Production Specification have to be migrated from Const to Database Fields [Issue 3](https://bitbucket.org/AkiraSonoda/akkieconomy/issues/3/plant-growth-and-production-specification)
* V0.006 - Production (value adding task ) Baking Coconut Cookies uses Coconuts and other Goods
* ...


## Authors

* **Akira Sonoda** - *Initial work* - [Akira Sonoda](https://github.com/AkiraSonoda)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the GNU Affero General Public License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* École Polytechnique Fédérale Lausanne (EPFL) and Martin Odersky - Scala Programming Language
* Lightbend - Scala, Akka, Slick and oder frameworks
* Jetbrains - Intellij Development Environment with an excellent Scala Plugin
* Oracle - MySql Database
* OpenSimulator.org - Virtual World where the System takes place
* LyAvain Swansong - ideas and feedback
* Victor Clary - all kinds of Prim-, Sculpt-Objects
* Freaky Tech - Arriba Simulator ( improved OpenSim Simulator )
* Dereos Grid and all the habitants of this grid.

