/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Drink Consumer Script
// Container Description
string  CONTAINER_NAME="Flasche";
string  CONTAINER_CONTENT="Bier";
string  CONTAINER_ITEM_TYPE="WATERTANK";
string  STATE_AFFECTED="THIRST";
integer CONTAINER_PRICE=3;

integer CONTAINER_VOLUME=30;
integer CONTAINER_CURRENT_VOLUME=30;
integer PORTION_VOLUME=10;

integer FLAG_DEBUG = FALSE; // see debug messages?

integer LISTEN_CHANNEL = 1001000003;
integer LISTEN_HANDLE = 0;


// KEYS for the HTTP Requests
key REST_REGISTER = NULL_KEY;
key REST_UPDATE = NULL_KEY;
key REST_DELETE = NULL_KEY;

key BUYER_ID = NULL_KEY;
key ITEM_OWNER = NULL_KEY;
key ITEM_ORIGIN = NULL_KEY;

string ECONOMYURL="http://akitest.dyndns.info:8010";

string CONTAINER_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"@jNAME\",\"itemType\":\"@jTYPE\",\"price\":@jPRICE,\"maxVolume\":@jCV,\"currentVolume\":@jCCV,\"measureUnit\":\"Deciliter\",\"origin\":\"@jORIGIN\"}";
string CONTAINER_UPDATE_JSON = "{\"avatarUUID\":\"@jOWNERID\",\"itemType\":\"@jTYPE\",\"volume\":\"@jPV\",\"stateAffected\":\"@jUSERSTATE\",\"stateModification\":\"subtraction\"}";

// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

userMessage(string umessage) {
    if(BUYER_ID == llGetOwner()) {
        llOwnerSay(umessage);
    } else {
        llInstantMessage(BUYER_ID, umessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

list json2List(string jsonString) {
    string aString = jsonString;
    aString = searchAndReplace(aString,"\"", "");
    aString = searchAndReplace(aString," ", "");
    aString = searchAndReplace(aString,"\n", "");
    aString = searchAndReplace(aString,"{","");
    aString = searchAndReplace(aString,"}","");
    aString = searchAndReplace(aString,",",":");
    debug("Json without Boilerplate: " + aString);
    return llParseString2List(aString, [":"], []);
}

//
// REST Calls
//
registerContainer(string whoClicked, string origin) {
    debug("registerInventory");
    string jsonString = CONTAINER_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) whoClicked);                 
    jsonString = searchAndReplace(jsonString,"@jCV",(string) CONTAINER_VOLUME);                 
    jsonString = searchAndReplace(jsonString,"@jCCV",(string) CONTAINER_CURRENT_VOLUME);                 
    jsonString = searchAndReplace(jsonString,"@jORIGIN",origin);                 
    jsonString = searchAndReplace(jsonString,"@jNAME",CONTAINER_NAME);
    jsonString = searchAndReplace(jsonString,"@jTYPE",CONTAINER_ITEM_TYPE);
    jsonString = searchAndReplace(jsonString,"@jPRICE",(string)CONTAINER_PRICE);                
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/container", POST_PARAMS, jsonString);
}

updateContainer(key whoClicked) {
    debug("updateContainer");
    string jsonString = CONTAINER_UPDATE_JSON;
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) whoClicked);
    jsonString = searchAndReplace(jsonString,"@jPV",(string) PORTION_VOLUME);                 
    jsonString = searchAndReplace(jsonString,"@jTYPE",CONTAINER_ITEM_TYPE);
    jsonString = searchAndReplace(jsonString,"@jUSERSTATE",STATE_AFFECTED);
    debug("JSON-String to update Container: "+jsonString);
    list PUT_PARAMS =[HTTP_METHOD,"PUT",HTTP_MIMETYPE,"application/json"];
    REST_UPDATE = llHTTPRequest(ECONOMYURL+"/rest/item/container/"+(string) llGetKey(), PUT_PARAMS, jsonString);
}

deleteContainer() {
    debug("deleteContainer");
    list DELETE_PARAMS =[HTTP_METHOD,"DELETE"];
    REST_DELETE = llHTTPRequest(ECONOMYURL+"/rest/item/container/"+(string)llGetKey(), DELETE_PARAMS,"");
}


default {
    state_entry() {
        LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");
        llSay(LISTEN_CHANNEL, "GET_OWNER");
    }

    on_rez(integer start_param){
        llResetScript();
    }

    listen ( integer channel, string name , key id, string message ) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        list params = llParseString2List(message, ["::"], []); // BUYER-ID and ORIGIN 
        ITEM_OWNER = (key) llList2String(params, 0);
        ITEM_ORIGIN = (key) llList2String(params, 1);
        registerContainer( ITEM_OWNER, ITEM_ORIGIN );
        llListenRemove(LISTEN_HANDLE);
    }


    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
                userMessage(CONTAINER_NAME+" ist serviert und in einigen Sekunden bereit");
                state serving;
            } else {
                debug("Registering Container failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    } 

    changed(integer change) {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }
}


state serving {
    state_entry() {
        debug("The script entered state 'serving' registering Container");
    }


    touch_end(integer num_detected) {
        BUYER_ID  = llDetectedKey(0);
        debug("User "+llKey2Name(BUYER_ID)+" clicked Updating the Container");
        if ( ( BUYER_ID != NULL_KEY ) && ( ITEM_OWNER != NULL_KEY ) ) {
            if ( BUYER_ID == ITEM_OWNER ) {
                updateContainer(BUYER_ID);
            } else {
                llInstantMessage(BUYER_ID, "Das Essen kann nur vom Käufer konsumiert werden");
            }
        }        
    }


    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_UPDATE) {
            if (status == 200) {
              userMessage("1/3 Portion "+CONTAINER_CONTENT+" konsumiert");
              list jsonList = json2List(body);
              // {0}{1}{2}{3}{4}{5}{6}{7}
              integer remainingVolume = llList2Integer(jsonList, 5);
              if(remainingVolume <= 0) {
                state deleting;
              } 
            } else if(status == 404) {
                debug("Unable to get UserData Status: "+(string) status);
            } else  {
                debug("Unable to get UserData Status: "+(string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    } 
 
    on_rez(integer start_param){
        debug("Reset Change from 'serving' to 'default'");
        llResetScript();
    }
 
    state_exit() {
        debug("The script leaves state 'serving'");
    }
}

state deleting {
    state_entry() {
        userMessage(CONTAINER_NAME+" ist leer wird in einigen Sekunden recycled");
        debug("The script entered state 'deleting' deleting Container");
        llSleep(1.0);
        deleteContainer();
    }

    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id == REST_DELETE) {
            debug("request_id = REST_DELETE"); 
            if(status == 200) {
                llOwnerSay(CONTAINER_NAME+" ist leer wird nun recycled");
                llSay(LISTEN_CHANNEL, "DELETE::"+(string)ITEM_ORIGIN);
                llDie();                
            } else {
                llOwnerSay("Something went wrong with delete: "+(string)status);
                llSleep(5.0);
                deleteContainer();
            }           
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
            llSleep(5.0);
            deleteContainer();
            
        } else  {
            debug("Unknown Request received"+(string)request_id);
            llSleep(5.0);
            deleteContainer();
        }
    } 

    on_rez(integer start_param){
        debug("Reset Change from 'deleting' to 'default'");
        llResetScript();
    }
}


