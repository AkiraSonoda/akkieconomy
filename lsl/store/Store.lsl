/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

// Store
string  STORE_NAME = "Kiste";
string  OBJECT_TYPE = "STORE_10";
integer OBJECT_PRICE = 0;

integer FLAG_DEBUG = FALSE; // see debug messages?

// KEYS for the HTTP Requests
key REST_REGISTER = NULL_KEY;
key REST_ADD_ITEM = NULL_KEY;

key ITEM_SELLER = NULL_KEY;
key ITEM_ID = NULL_KEY;
string ITEM_TYPE = NULL_KEY;

integer NEGOTIATION_CHANNEL = 1002000001;
integer TRANSACTION_CHANNEL = 1002000003;
integer LISTEN_HANDLE = 0;

string ECONOMYURL="http://akitest.dyndns.info:8010";

string STORE_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"@jNAME\",\"itemType\":\"@jTYPE\",\"price\":@jPRICE,\"origin\":\"00000000-0000-0000-0000-000000000000\"}";
string STORE_ITEM_JSON = "{\"itemtype\":\"@jITEMTYPE\"}";

// Helper
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

userMessage(string umessage) {
    if(ITEM_SELLER == llGetOwner()) {
        llOwnerSay(umessage);
    } else {
        llInstantMessage(ITEM_SELLER, umessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}


list json2List(string jsonString) {
    string aString = jsonString;
    // aString = searchAndReplace(aString,"\"", "");
    aString = searchAndReplace(aString," ", "");
    aString = searchAndReplace(aString,"\n", "");
    aString = searchAndReplace(aString,"{","");
    aString = searchAndReplace(aString,"}","");
    aString = searchAndReplace(aString,",",":");
    debug("Json without Boilerplate: " + aString);
    return llParseString2List(aString, [":"], []);
}

//
// REST Calls
//
registerStore() {
    debug("registerStore");
    string jsonString = STORE_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) llGetOwner());
    jsonString = searchAndReplace(jsonString,"@jNAME",STORE_NAME);
    jsonString = searchAndReplace(jsonString,"@jPRICE", (string) OBJECT_PRICE);
    jsonString = searchAndReplace(jsonString,"@jTYPE",OBJECT_TYPE);               
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/store", POST_PARAMS, jsonString);
}

addToStore() {
    debug("addToStore");
    string jsonString = STORE_ITEM_JSON;
    jsonString = searchAndReplace(jsonString,"@jITEMTYPE",ITEM_TYPE);               
    debug("JSON-String to register Container: "+jsonString);
    list PUT_PARAMS =[HTTP_METHOD,"PUT",HTTP_MIMETYPE,"application/json"];
    REST_ADD_ITEM = llHTTPRequest(ECONOMYURL+"/rest/item/store/"+(string)llGetKey(), PUT_PARAMS, jsonString);
}


default {

    on_rez( integer Parameter ) { llResetScript(); }
    
    state_entry() {
        debug("The script entered state 'default' registering Container");
        registerStore();
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
                llOwnerSay("Vendor registriert und in einigen Sekunden bereit");
                state negotiating;
            } else {
                debug("Registering Vendor failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }
} 

state negotiating {

    on_rez( integer Parameter ) { llResetScript(); }

    state_entry() {
        debug("The script entered state 'buying' opening negitiation channel");
        LISTEN_HANDLE = llListen(NEGOTIATION_CHANNEL, "", "", "GET_CHANNEL");

    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        if (message == "GET_CHANNEL") {
            llWhisper(NEGOTIATION_CHANNEL,(string)TRANSACTION_CHANNEL);
            llListenRemove(LISTEN_HANDLE);
            state trading;
        }
    }
}

state trading { 
    state_entry() {
        LISTEN_HANDLE = llListen(TRANSACTION_CHANNEL, "", "", "");
    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);

        list params = llParseString2List(message, ["::"], []); // BUYER-ID and ORIGIN 
        string command = (string) llList2String(params, 0);
        ITEM_ID  = (key) llList2String(params, 1);
        ITEM_SELLER = (key) llList2String(params, 2);
        ITEM_TYPE = llList2String(params, 3);
        if(command == "ITEM_TO_SELL") {
            // First we have to check if the seller is the same as the store owner
            if (ITEM_SELLER != llGetOwner()) {
                userMessage("Nur des Besitzer des Lagers darf Gegenstände einlagern");
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            } else {
                // Then we can add the Object to the Store
                addToStore();
            }
        }
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_ADD_ITEM) {
            if (status == 200) {
                llWhisper(TRANSACTION_CHANNEL, "TRANSACTION_OK");
                userMessage(ITEM_TYPE +" erfolgreich zum Lager " +STORE_NAME+ " hinzugefügt");
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            } else if (status == 404) {
                userMessage(" Nicht registrierte Objekte können nicht zum Lager " +STORE_NAME+ " hinzugefügt werden");
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            } else {
                debug("Price Request failed with Code: " + (string) status);
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }
}


// END //
