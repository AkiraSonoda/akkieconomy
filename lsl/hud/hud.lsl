/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

// The actual HUD Sceipt
//
// The purpose of this script is: TBD

// CONSTANTS - DO NOT CHANGE DURING RUN
// string soundscream = "hitmale.wav" ;
// string soundhit = "treffer.wav" ;
// string sounddie = "sterbenmale.wav" ;
// string soundstrike = "Fausthieb.wav" ;
 
// RUNTIME GLOBALS - CAN CHANGE DURING RUN
integer FLAG_DEBUG = FALSE; // see debug messages?
string CARD = "AkkiEconomyHudConfig"; // Configuration Notecar
string DIV = "|"; // message field divider
integer LINE = 0; // reading line number
key QUERY = NULL_KEY;      // track notecard queries
string TOKEN = "";
string LAST_URL = "";


// Login Information
key PLAYERID = NULL_KEY; // cached player UUID
string PLAYERNAME = "";  // cached player name
string PASSWORD = "";    // Do not set the password here. It will be read from the Config-file
string NEWPASSWORD ="";  // Do not set the new password here. It will be read from the Config-File
string ECONOMYURL = "";  // Di not set the url from the Economy Server here. It will be read from the Config-File

// Channels
integer CHANOBJECT = 0;    // dynamic channel to one object's UUID
integer CHANATTACH;        // dynamic channel for player attachments 
integer HANDATTACH;        // attach channel handle for llRemove 
string CHAN_PREFIX = "0x"; // channel prefix for calculating dynamic channels

// Stats for the Meter
integer METERWORN = FALSE; // using meter?
integer HUNGER = 0;
integer THIRST = 0;
integer HEALTH = 0;

// JSON String Templates
string  LOGIN_JSON = "{\"avatarUUID\":\"@jAUUID\",\"avatarName\":\"@jANAME\",\"password\":\"@jAPW\",\"newPassword\":\"@jANPW\"}";

// KEYS for the HTTP Requests
key REST_LOGIN = NULL_KEY;
//
// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

// ERROR - show errors on debug channel with wearer name for sorting
error(string emessage) {
    llSay(DEBUG_CHANNEL,"ERROR ("+llKey2Name(PLAYERID)+"): "+emessage);
}

// evaluateCommand - process chat and link message commands together
evaluateCommand(string command) {
    if ( command == "credits" ) { credits(); return;} // show the credits including version number
    if ( command == "reset" ) { reset(); return;} // reset HUD
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

// CREDITS comply with Myriad RPG Creative Common-Attribution legal requirement
credits() {
    llOwnerSay("The AkkiEconomy System was designed, written, and illustrated by Akira Sonoda.");
    llOwnerSay("AkkiEconomy is licensed under the GNU Affero General Public License V3");
}
 
// RESET - shut down running animations then reset the script to reload character sheet
reset() {
    llOwnerSay("Resetting AkkiEconomy. Please wait...");
    // stop all running animations
    llReleaseURL(LAST_URL);
    // llMessageLinked(LINK_THIS,0,"MODULERESET",NULL_KEY); // tell modules to reset too TODO function this
    // TODO can we do this with an attachment and have it do the attachment setup correctly?
    llResetScript(); // now reset

}

//
// Really bad idea.. but JSON Support is not implemented in ArribaSim
// Thereofre we remove all the JSON boilerplate and parse the List with the colon separator
// Nested JSON will not work with this mechanism but I guess the whole AkkiEconomy System 
// will only require very basic JSON filse
//
list json2List(string jsonString) {
	string aString = jsonString;
	//aString = searchAndReplace(aString,"\"", "");
	aString = searchAndReplace(aString," ", "");
	aString = searchAndReplace(aString,"\n", "");
	aString = searchAndReplace(aString,"{","");
	aString = searchAndReplace(aString,"}","");
	aString = searchAndReplace(aString,",",":");
	debug("Json without Boilerplate: " + aString);
	return llParseString2List(aString, [":"], []);
}

//
// Another awful function
//
string findInList(list searchlist, string search) {
	integer i;
    integer length = llGetListLength(searchlist);
    do 
    	if (llList2String(searchlist, i) == search ) {
    		return llList2String(searchlist, i+1);
    	}
    while(++i < length);
    return "";
}


//
// Business Function 
// 
// SETUP - begin bringing the HUD online
setup() {
    debug("Setup()");
    credits(); // show AkkiEconomy credits as required by the Creative Commons - Attribution license
    PLAYERID = llGetOwner(); // remember the owner's UUID
    PLAYERNAME = llKey2Name(PLAYERID); // remember the owner's legacy name
    llSetText("",<0,0,0>,0); // clear any previous hovertext
    llOwnerSay("Loading Configuration Notecard. Please wait..."); // tell player we're waiting for data server
    if ( llGetInventoryName(INVENTORY_NOTECARD,0) == CARD ) { // check inventory for notecard
        QUERY = llGetNotecardLine(CARD,LINE++); // ask for line from notecard and advance to next line
    } else {
        // error("Cannot locate character sheet from notecard: "+CARD); // TODO: what next? choose defaults? fall over?
    }
}
 
 
// METER - update a hovertext health meter or HUD bar graph
meter() {
    debug("METER()");
    if ( METERWORN == FALSE ) return;
    // create a meter message packet
    string message = "METER"+DIV+(string)HUNGER+DIV+(string)THIRST+DIV+(string)HEALTH;
    llWhisper(CHANATTACH,message); // whisper to the wearer's actual meter
    debug("AvatarState: Hunger: "+(string)HUNGER+" Thirst: "+(string)THIRST+" Health: "+(string)HEALTH);
}

sendToken() {
    debug("sendToken()");
    // create a meter message packet
    string message = "TOKEN"+DIV+TOKEN;
    llSay(CHANATTACH,message); 
}


//
// REST Calls
//
loginToEconomyServer(string pushurl) {
	debug("loginToEconomyServer");
	string jsonString = LOGIN_JSON;
	jsonString = searchAndReplace(jsonString,"@jAUUID",(string)PLAYERID); 
    jsonString = searchAndReplace(jsonString,"@jANAME",PLAYERNAME);                 
    jsonString = searchAndReplace(jsonString,"@jAPW",PASSWORD);
    jsonString = searchAndReplace(jsonString,"@jANPW",NEWPASSWORD);
    debug("JSON-String for Login: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json",HTTP_CUSTOM_HEADER,"pushurl",pushurl];
    REST_LOGIN = llHTTPRequest(ECONOMYURL+"/rest/auth/signIn", POST_PARAMS, jsonString);
}


 
// DEFAULT STATE - load character sheet
default {
 
    state_entry() {
        setup(); // show credits and start character sheet load
    }
 
    on_rez(integer params) {
        reset(); // force to go through state entry
    }
 
    attach(key id) {
        reset(); // force to go through state entry
    }
 
    // dataserver called for each line of notecard requested - process character sheet
    dataserver(key queryid,string data) {
        debug("Dataserver - QueryId: " +(string)queryid+" Data: " +data);
        if ( queryid == QUERY ) { // ataserver gave us line we asked for?
            if ( data != EOF ) { // we're not at end of notecard file?
                if ( llGetSubString(data,0,0) == "#" ) {    // does this line start with comment mark?
                    QUERY = llGetNotecardLine(CARD,LINE++); // ignore comment and ask for the next line
                    return;
                }
                // Parse non-comment lines in keyword = value[,value,...] format
                list FIELDS  = llParseString2List(data,["="],[]); // break line of text into = delimited fields
                string KEY   = llStringTrim(llList2String(FIELDS,0),STRING_TRIM); // field zero is the "command"
                string VALUE = llStringTrim(llList2String(FIELDS,1),STRING_TRIM); // field one is the data
                if ( KEY == "PASSWORD" ) { PASSWORD = VALUE; }
                if ( KEY == "NEWPASSWORD" ) { NEWPASSWORD = VALUE; }
				if ( KEY == "ECONOMYURL" ) { ECONOMYURL = VALUE; }
                QUERY = llGetNotecardLine(CARD,LINE++);  // finished with known keywords, get next line
            } else { 
                // TODO how to verify entire character sheet was completed and loaded?
                state running; // we're out of notecard, so character sheet is loaded - start playing
            } 
        } 
    } 
} 
 
state running {
 
    // STATE ENTRY
    state_entry() {
        CHANATTACH = (integer)("0x"+llGetSubString((string)PLAYERID,1,7)); // attachment-specific channel
        HANDATTACH = llListen(CHANATTACH,"",NULL_KEY,""); // listen for messages from attachments
        llOwnerSay("Registering any AkkiEconomy compatible attachments...");
        llWhisper(CHANATTACH,"REGISTERATTACHMENTS"); // ask for attachments on their dynamic channel
        meter(); // update hovertext
        llRequestURL();
    }
 
    on_rez(integer rezparam) {
        reset(); 
    }
 
    attach(key id) {
        reset(); 
    }
 
    // CHANGED - triggered for many changes to the avatar
    // TODO reload sim-specific settings on region change
    changed(integer changes) {
        if ( changes & CHANGED_INVENTORY ) { // inventory changed somehow?
            llOwnerSay("Inventory changed. Reloading.");
            reset(); // saved a new character sheet? - reset and re-read it.
        }
        if ( changes & CHANGED_REGION || changes & CHANGED_TELEPORT ) {
            reset();
        }
    }
 
 
    touch_start(integer total_number) {
        llResetScript();
    }
 
   
    link_message(integer sender,integer num,string cmd, key id) {
        evaluateCommand(cmd); // send to shared command processor for chat and link messages
        return;
    }
 
    // LISTEN - the main AkkiEconomy message processor 
    listen(integer channel, string speakername, key speakerid, string message) {
        debug("listen - channel: "+(string)channel+" speakername: "+speakername+" speakerid: "+(string)speakerid+" message: "+message);
        // calculate the dynamic channel of who is speaking in case we need to return commands
        CHANOBJECT = (integer)(CHAN_PREFIX+llGetSubString((string)speakerid,0,6));
 
        // break down the commands and messages into units we can work with
        list fields = llParseString2List(message,[DIV],[]); // break into list of fields based on DIVider
        string command = llList2String(fields,0);           // assume the first field is a AkkiEconomy command
 
 
        // --- ATTACHMENT CHANNEL
        if ( channel == CHANATTACH ) { // handle the attachment commands
            if ( command == "ATTACHMETER" ) {
                METERWORN = TRUE; // we need to send meter events
                meter(); // send update
                return;
            }
            if ( command == "DETACHMETER" ) {
                METERWORN = FALSE;
                return;
            }
            if ( command == "GETTOKEN" ) {
                sendToken();
                return;
            }

        }
    }

    http_request(key id, string method, string body) {
        debug("Url Key: " + (string) id);
        if (method == URL_REQUEST_DENIED) {
            llOwnerSay("The following error occurred while attempting to get a free URL for this device:\n \n" + body);
        } else if (method == URL_REQUEST_GRANTED) {
            LAST_URL = body;
        	llOwnerSay("Going To Login to the Economy Server.");
        	loginToEconomyServer(LAST_URL);
        } else if (method == "GET") {
            llHTTPResponse(id,200,"");
        } else if (method == "POST") {
        	debug("Got User Data: "+body);
        	list jsonList = json2List(body);
        	THIRST = (integer)findInList(jsonList, "avatarthirst");
        	HUNGER = (integer)findInList(jsonList, "avatarhunger");
			meter();
            llHTTPResponse(id,200,"");
        }
    }


    http_response(key request_id, integer status, list metadata, string body) {
    	debug("http_response"); 
        if (request_id == REST_LOGIN) {
        	if (status == 200) {
        		llOwnerSay("Login to Economy Server successful");
        		list jsonList = json2List(body);
        		TOKEN = findInList(jsonList, "token");
                THIRST = (integer)findInList(jsonList, "avatarthirst");
                HUNGER = (integer)findInList(jsonList, "avatarhunger");                
        		debug("Token found: " + TOKEN + "Thirst: " +(string)THIRST+ "HUNGER:" + (string)HUNGER);
                meter();
        	} else {
        		llOwnerSay("Login to Economy Server failed with Code: " + (string)status);
        	}

        } else  {
        	return;// exit if unknown
 		}
    } 

} 


// END