/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

// MarketGiver Item Giver

// name of the item in object's inventory, to vend


string REGISTER_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"MarketGiver\",\"itemType\":\"VENDOR\",\"price\":0,\"origin\":\"00000000-0000-0000-0000-000000000000\"}";

integer FLAG_DEBUG = FALSE; // see debug messages?
// string DIV = "|";
key REST_REGISTER = NULL_KEY;
key REST_CHECK = NULL_KEY;

string ECONOMYURL="http://akitest.dyndns.info:8010";
integer LISTEN_CHANNEL = -1001999999;
integer LISTEN_HANDLE = 0;

string COMMAND = "";
string ITEM_NAME = "";
string ITEM_ID = "";
key    ITEM_OWNER = "";
string ITEM_TYPE = "";
key    ITEM_ORIGIN = "";


// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

// give the item to a customer
registerGiver(key toWhom) {
	string jsonString = REGISTER_JSON;
	jsonString = searchAndReplace(jsonString,"@jOWNERID",(string)toWhom);
    jsonString = searchAndReplace(jsonString,"@jID",(string)llGetKey());
    debug("JSON-String to insert Plant: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
	REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/vendor", POST_PARAMS, jsonString);
}

checkItem(string id, string owner, string type, string origin) {
    debug("checkItem");
    list GET_PARAMS =[HTTP_METHOD,"GET"];
    REST_CHECK = llHTTPRequest(ECONOMYURL+"/rest/item/vendor/"+id+"/"+owner+"/"+type+"/"+origin, GET_PARAMS, "");
}



default {
	
    on_rez(integer start_param) {
        debug("Reseting");
        llResetScript();
    }

    state_entry() {
        // Register this giver
        registerGiver(llGetOwner());
    }


    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
                llOwnerSay("Registrierung des Market_Giver erfolgreich"); 
                state serving;
            } else {
               	debug("Registering Vendor failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    } 
  
}

state serving {

    state_entry() {
        // Opening Message Channel
        debug("Entering State Serving");
        LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");
    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        // Messages have the following format
        //
        // - command: "GIVE_ITEM"
        // - itemname: the name of the item to give
        // - itemId: the uuid of the item
        // - itemOwner: the uuid of the item owner
        // - itemType: the type of the item
        // - itemorigin: the origin of the items vendor
        //
        list params = llParseString2List(message, ["::"], []); 
        COMMAND = llList2String(params,0);
        ITEM_NAME = llList2String(params,1);
        ITEM_ID = (key) llList2String(params,2);
        ITEM_OWNER = (key) llList2String(params,3);
        ITEM_TYPE = llList2String(params,4);
        ITEM_ORIGIN = (key) llList2String(params,5);

        if (COMMAND == "GIVE_ITEM") {
            checkItem(
                (string) ITEM_ID
                , (string) ITEM_OWNER
                , ITEM_TYPE
                , (string) ITEM_ORIGIN
            );

        }
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_CHECK) {
            if (status == 200) {
                llGiveInventory(ITEM_OWNER, ITEM_NAME);
            } else {
                debug("Check Item failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }

    on_rez(integer start_param) {
        debug("Reseting");
        llResetScript();
    }
}

// END //
