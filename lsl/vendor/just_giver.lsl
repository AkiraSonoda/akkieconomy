/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

// Single Item Giver

// name of the item in object's inventory, to vend
string ITEM_NAME = "AEWaterTank";

integer FLAG_DEBUG = FALSE; // see debug messages?
key     BUYER_ID = NULL_KEY;

// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}


default {
	
    state_entry() {
        // we need this permission to give change
    }

    touch_end(integer total_number) {
        // if someone touches object describe what's for sale
        BUYER_ID = llDetectedKey(0);
        llGiveInventory(BUYER_ID, ITEM_NAME);

    }

   	on_rez(integer start_param){
        debug("Reseting");
        llResetScript();
    }
  
}
// END //
