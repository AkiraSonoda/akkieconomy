/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */
 // name of the item in object's inventory, to vend
string  ITEM_NAME = "AEWaterTank"; 
integer ITEM_PRICE = 20;  // cost of the item
string  ITEM_TYPE = "WATERTANK";




integer FLAG_DEBUG = FALSE; // see debug messages?
// string DIV = "|";
key REST_REGISTER = NULL_KEY;
key BUYER_ID = NULL_KEY;
integer BUYER_AMOUNT = 0;
string ECONOMYURL="http://akitest.dyndns.info:8010";

string REGISTER_JSON = "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"ownerid\":\"@jOWNERID\",\"itemtype\":\"@jTYPE\",\"origin\":\"00000000-0000-0000-0000-000000000000\"}";


// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

// give the item to a customer
register(key toWhom) {
	string jsonString = REGISTER_JSON;
	jsonString = searchAndReplace(jsonString,"@jOWNERID",(string)toWhom);                 
    jsonString = searchAndReplace(jsonString,"@jTYPE",ITEM_TYPE);                 
    debug("JSON-String to insert MarketInventory: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
	REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/vendor/"+(string)toWhom, POST_PARAMS, jsonString);
}

transactionFailed() {
	llInstantMessage(BUYER_ID, "Transaktion fehlgeschlagen gebe Geld zurück");
    llGiveMoney(BUYER_ID, BUYER_AMOUNT);
}

default {
	
    state_entry() {
        // we need this permission to give change
        llSetClickAction(CLICK_ACTION_PAY);
        llSetPayPrice(PAY_HIDE, [ITEM_PRICE ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
        BUYER_ID = NULL_KEY;
        BUYER_AMOUNT = 0;
    }


    money(key id, integer amt) {
    	BUYER_ID = id;
    	BUYER_AMOUNT = amt;
        register(BUYER_ID);
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
    			llGiveInventory(BUYER_ID, ITEM_NAME);
    			integer amount = BUYER_AMOUNT - ITEM_PRICE;
    			if (amount>0) {
    				llGiveMoney(BUYER_ID, amount);
    			}
            } else {
               	debug("Registering Container failed with Code: " + (string) status);
               	transactionFailed();
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
            transactionFailed();
        } else  {
            debug("Unknown Request received:"+(string)request_id);
            transactionFailed();
        }
        BUYER_ID = NULL_KEY;
        BUYER_AMOUNT = 0;

    } 

   	on_rez(integer start_param){
        debug("Reset Change from 'serving' to 'default'");
        llResetScript();
    }
  
}
// END //
