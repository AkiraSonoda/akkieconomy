/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

 // Food  
string  REZZER_NAME = "Bierdeckel";
string  OBJECT_TO_REZZ = "Singha Flasche 3dl";
integer OBJECT_PRICE = 3;
vector  OBJECT_Z_OFFSET = <0.0,0.0,0.1>;
string  OBJECT_ITEM_TYPE = "WATERTANK";


integer FLAG_DEBUG = FALSE; // see debug messages?

// KEYS for the HTTP Requests
key REST_REGISTER = NULL_KEY;
key REST_REGISTER_ITEM = NULL_KEY;

integer LISTEN_CHANNEL = 1001000003;
integer LISTEN_HANDLE = 0;


string ECONOMYURL="http://akitest.dyndns.info:8010";

key WHO_TOUCHED = NULL_KEY;
integer WHAT_AMOUNT = 0;

string VENDOR_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"@jNAME\",\"itemType\":\"Vendor\",\"price\":@jPRICE,\"origin\":\"00000000-0000-0000-0000-000000000000\"}";
string SOLD_ITEM_JSON = "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"ownerid\":\"@jOWNERID\",\"itemtype\":\"@jITEMTYPE\",\"origin\":\"@jORIGIN\"}";

debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

userMessage(string umessage) {
    if(WHO_TOUCHED == llGetOwner()) {
        llOwnerSay(umessage);
    } else {
        llInstantMessage(WHO_TOUCHED, umessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

//
// REST Calls
//
registerInventory() {
    debug("registerInventory");
    string jsonString = VENDOR_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) llGetOwner());
    jsonString = searchAndReplace(jsonString,"@jPRICE", (string) OBJECT_PRICE);                
    jsonString = searchAndReplace(jsonString,"@jNAME", REZZER_NAME);                
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/vendor", POST_PARAMS, jsonString);
}


// give the item to a customer
register_item(key toWhom) {
    string jsonString = SOLD_ITEM_JSON;
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string)toWhom);
    jsonString = searchAndReplace(jsonString,"@jORIGIN",(string)llGetKey());                 
    jsonString = searchAndReplace(jsonString,"@jITEMTYPE",OBJECT_ITEM_TYPE);                 
    debug("JSON-String to insert MarketInventory: "+jsonString);
    list PUT_PARAMS =[HTTP_METHOD,"PUT",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER_ITEM = llHTTPRequest(ECONOMYURL+"/rest/item/vendor/"+(string)llGetKey()+"/"+(string)llGetOwner()+"/00000000-0000-0000-0000-000000000000", PUT_PARAMS, jsonString);
}


default {

    on_rez( integer Parameter ) { llResetScript(); }
    
    state_entry() {
        debug("The script entered state 'default' registering Container");
        registerInventory();
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
                llOwnerSay("Vendor registriert und in einigen Sekunden bereit");
                state selling;
            } else {
                debug("Registering Vendor failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }
} 

state selling {

    on_rez( integer Parameter ) { llResetScript(); }

    state_entry() {
        llSetClickAction(CLICK_ACTION_PAY);
        llSetPayPrice(PAY_HIDE,[OBJECT_PRICE ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
        debug("The script entered state 'selling'");

    }

    money(key id, integer amount) {
        // Cross Check the Validity of the Vendor
        WHO_TOUCHED = id;
        WHAT_AMOUNT = amount;

        register_item(WHO_TOUCHED);
    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        if (message == "GET_OWNER") {
            llSay(LISTEN_CHANNEL, (string) WHO_TOUCHED + "::" + (string) llGetKey());
            llListenRemove(LISTEN_HANDLE);
            state consuming;
        }
    }


    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER_ITEM) {
            if (status == 200) {
                // Open the channel for short term communication
                LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "GET_OWNER");
                userMessage(" für "+(string)WHAT_AMOUNT+" OS$ gekauft");
                vector position = llGetPos();
                rotation angle = llGetRot();
                llRezObject( OBJECT_TO_REZZ, position + (OBJECT_Z_OFFSET * angle), ZERO_VECTOR, angle, 0 );
            } else {
                userMessage("Vendor scheint ein Problem zu haben, gebe das Geld zurück");
                debug("REST_CHECK Status: "+(string) status);
                llGiveMoney(WHO_TOUCHED, WHAT_AMOUNT);
            }
        } else if (request_id == NULL_KEY) {
            userMessage("Vendor scheint ein Problem zu haben, gebe das Geld zurück");
            llGiveMoney(WHO_TOUCHED, WHAT_AMOUNT);
            debug("Null Key received could not fire the http request");
        } else  {
            userMessage("Vendor scheint ein Problem zu haben, gebe das Geld zurück");
            llGiveMoney(WHO_TOUCHED, WHAT_AMOUNT);
            debug("Unknown Request received:"+(string)request_id);
        }
    } 

}

state consuming { 
    state_entry() {
        LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");
    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        list params = llParseString2List(message, ["::"], []); // BUYER-ID and ORIGIN 
        string command = (string) llList2String(params, 0);
        key origin = (key) llList2String(params, 1);
        if(command == "DELETE") {
            if(origin == llGetKey()) {
                llListenRemove(LISTEN_HANDLE);
                state selling;

            }
        }
    }

}

// END //
