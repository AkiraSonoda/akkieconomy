/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */
string OBJECT_TO_REZZ = "Korb voller Kokosnuesse";

integer PLANT_PRICE = 1000;
string  PLANT_TYPE = "PLANT_TREE";
string  PLANT_STATE = "PLANT_PRODUCING_FRUITS";

integer FLAG_DEBUG = FALSE; // see debug messages?

integer LISTEN_CHANNEL = 1001000001;
integer LISTEN_HANDLE = 0;

// KEYS for the HTTP Requests
key REST_REGISTER_TREE = NULL_KEY;
key REST_REGISTER_BASKET = NULL_KEY;
key REST_DELETE_BASKET = NULL_KEY;
integer OWNER_REQUESTED = FALSE;


string ECONOMYURL="http://akitest.dyndns.info:8010";

// REST
string BASKET_JSON = "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"ownerid\":\"@jOWNERID\",\"itemtype\":\"VENDOR\",\"origin\":\"@jORIGIN\"}";

string PLANT_JSON = "{
	\"id\" :\"@jID\",
	\"ownerid\" : \"@jOWNERID\",
    \"name\" : \"AE Palm Tree\",
    \"itemType\" : \"@jPTYPE\",
    \"price\" : @jPRICE,
    \"pushurl\" : \"@jURL\",  
    \"plantType\" : \"@jPTYPE\",
    \"plantState\" : \"@jPSTATE\",
    \"origin\":\"00000000-0000-0000-0000-000000000000\"
}";


debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}


string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

//
// REST Calls
//
registerTree(string url) {
    debug("registerInventory");
    string jsonString = PLANT_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) llGetOwner());
    jsonString = searchAndReplace(jsonString,"@jPRICE", (string) PLANT_PRICE);                
    jsonString = searchAndReplace(jsonString,"@jPTYPE", (string) PLANT_TYPE);                
    jsonString = searchAndReplace(jsonString,"@jPSTATE", (string) PLANT_STATE);                
    jsonString = searchAndReplace(jsonString,"@jURL", url);                
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER_TREE = llHTTPRequest(ECONOMYURL+"/rest/item/plant", POST_PARAMS, jsonString);
}

// give the item to a customer
registerBasket() {
	string jsonString = BASKET_JSON;
	jsonString = searchAndReplace(jsonString,"@jOWNERID",(string)llGetOwner());                 
	jsonString = searchAndReplace(jsonString,"@jORIGIN",(string)llGetKey());                 
    debug("JSON-String to insert MarketInventory: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"PUT",HTTP_MIMETYPE,"application/json"];
	REST_REGISTER_BASKET = llHTTPRequest(ECONOMYURL+"/rest/item/plant/"+(string) llGetKey(), POST_PARAMS, jsonString);
}

deleteBasket(key vendor) {
    debug("deleteBasket");
    list DELETE_PARAMS =[HTTP_METHOD,"DELETE"];
    REST_DELETE_BASKET = llHTTPRequest(ECONOMYURL+"/rest/item/vendor/"+(string)vendor+"/PLANT/"+(string)llGetKey(), DELETE_PARAMS,"");
}


default {

    on_rez( integer Parameter ) { llResetScript(); }
    
    state_entry() {
        debug("The script entered state 'default' registering Container");
        llRequestURL();
    }

    changed(integer change) {
        if (change & CHANGED_REGION_START) { // region restart
            float sleepTime = 30.0 + llFrand(120.0); // Do the resets randomly
            llSleep(sleepTime);
            llOwnerSay("Restarting the Script");
            llResetScript();
        }
        if (change & CHANGED_INVENTORY) { // Inventory Change
            llResetScript();
        }
    }

    http_request(key id, string method, string body) {
        debug("Url Key: " + (string) id);
        if (method == URL_REQUEST_DENIED) {
            llOwnerSay("The following error occurred while attempting to get a free URL for this device:\n \n" + body);
        } else if (method == URL_REQUEST_GRANTED) {
            string url = body;
            debug("Going To Connect to the Economy Server.");
            registerTree(url);
        } else if (method == "GET") {
            llHTTPResponse(id,200,"");
        } else if (method == "PUT") {
        	string message = body;
            debug("Got User Data: "+message);
            if (message == "PLANT_HARVEST") {
            	// register a new Fruit Basket
                llHTTPResponse(id,200,"PLANT_HARVEST_OK");            	    
            	registerBasket();
            } else if (message == "PLANT_DEAD") {
            	llHTTPResponse(id,200,"PLANT_DEAD_OK");
            	llSleep(1.0);
            	llDie();
            } else {
            	llHTTPResponse(id,404,"");
            }
        }
    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        if (message == "GET_OWNER") {
        	if(OWNER_REQUESTED == FALSE) {
            	llSay(LISTEN_CHANNEL, (string)llGetOwner()+"::"+(string)llGetKey());
            	OWNER_REQUESTED = TRUE;
        	}
        } else {
        	list fields = llParseString2List(message,["::"],[]);
        	string command = llList2String(fields,0);
        	key vendor = llList2String(fields,1);
        	key origin = llList2String(fields,2);
        	if ( command == "VENDOR_DELETE") {
        		if( origin == llGetKey()) {
        			OWNER_REQUESTED = FALSE;
        			deleteBasket(vendor);
        		}
        	}
        	llListenRemove(LISTEN_HANDLE);
        }
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER_TREE) {
            if (status == 200) {
                llOwnerSay("ist registriert und wird in wenigen Tagen Früchte tragen");
            } else {
                debug("Registering Tree failed with Code: " + (string) status);
            }
        } else if (request_id == REST_REGISTER_BASKET) {
        	if (status == 200) {
        		// Basket is registered we will rezz it now
                // We need to open a Listener for the 
                LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");

                vector position = llGetPos();
                rotation angle = llGetRot();
                vector offset = <0.0,0.0,0.1>;
                llRezObject( OBJECT_TO_REZZ, position + (offset * angle), ZERO_VECTOR, angle, 0 );

        	} else {
                debug("Registering Basket failed with Code: " + (string) status);
        	}
        } else if (request_id == REST_DELETE_BASKET) {
        	if (status != 200) {
        		debug("Failure deleting Basket: " + (string) status );
        	}
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }
} 


// END //
