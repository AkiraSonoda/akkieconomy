/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

string ITEM_TYPE = "BASKET_COCONUT";

integer FLAG_DEBUG = FALSE; // see debug messages?
// string DIV = "|";

integer NEGOTIATION_CHANNEL = 1002000001;
integer BUYER_CHANNEL = 0;
integer LISTEN_HANDLE = 0;


// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}


default {
	
    on_rez( integer Parameter ) { llResetScript(); }
    
    state_entry() {
        debug("The script entered state 'default' registering Vendor");
        // We need to negotiate a channel 
        LISTEN_HANDLE = llListen(NEGOTIATION_CHANNEL, "", "", "");
        llWhisper(NEGOTIATION_CHANNEL, "GET_CHANNEL");
    }


    listen ( integer channel, string name , key id, string message ) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        BUYER_CHANNEL = (integer) message;
        llListenRemove(LISTEN_HANDLE);
        llSleep(1.0);
        state selling;
    }

}

state selling {

    on_rez( integer Parameter ) { llResetScript(); }

    state_entry() {
        // We open a channel to settle the transaction
        LISTEN_HANDLE = llListen(BUYER_CHANNEL, "", "", "");
        llWhisper(BUYER_CHANNEL, "ITEM_TO_SELL::"+(string)llGetKey()+"::"+(string)llGetOwner()+"::"+ITEM_TYPE);
    }

    listen ( integer channel, string name , key id, string message ) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        if(message == "TRANSACTION_OK") {
            llDie();
        }
    }
}
// END //
