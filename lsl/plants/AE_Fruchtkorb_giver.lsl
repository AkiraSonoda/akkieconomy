/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

// name of the item in object's inventory, to vend
string GIVER_ITEM_NAME = "AE Basket Coconut"; 
string GIVER_ITEM_TYPE = "BASKET_COCONUT";

string GIVER_ITEM_JSON = "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"ownerid\":\"@jOWNERID\",\"itemtype\":\"@jITEMTYPE\",\"origin\":\"00000000-0000-0000-0000-000000000000\"}";
string VENDOR_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"@jNAME\",\"itemType\":\"Vendor\",\"price\":0,\"origin\":\"@jORIGIN\"}";

integer FLAG_DEBUG = FALSE; // see debug messages?
// string DIV = "|";
key REST_REGISTER_VENDOR = NULL_KEY;
key REST_REGISTER_ITEM = NULL_KEY;

key BUYER_ID = NULL_KEY;
key VENDOR_OWNER = NULL_KEY;
key VENDOR_ORIGIN = NULL_KEY;

string ECONOMYURL="http://akitest.dyndns.info:8010";

integer LISTEN_CHANNEL = 1001000001;
integer LISTEN_HANDLE = 0;
integer GIVER_CHANNEL = -1001999999;


// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}

//
// REST Calls
//
registerVendor(string toWhom, string fromWhere) {
    debug("registerInventory");
    string jsonString = VENDOR_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",toWhom);                 
    jsonString = searchAndReplace(jsonString,"@jORIGIN",fromWhere);
    jsonString = searchAndReplace(jsonString,"@jNAME",GIVER_ITEM_NAME);                 
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER_VENDOR = llHTTPRequest(ECONOMYURL+"/rest/item/vendor", POST_PARAMS, jsonString);
}

registerItem(string toWhom) {
    string jsonString = GIVER_ITEM_JSON;
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string)toWhom);                 
    jsonString = searchAndReplace(jsonString,"@jITEMTYPE",GIVER_ITEM_TYPE);                 
    debug("JSON-String to insert MarketInventory: "+jsonString);
    list PUT_PARAMS =[HTTP_METHOD,"PUT",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER_ITEM = llHTTPRequest(ECONOMYURL+"/rest/item/vendor/"+(string)llGetKey()+"/"+(string)llGetOwner()+"/"+(string)VENDOR_ORIGIN, PUT_PARAMS, jsonString);
}




default {
	
    on_rez( integer Parameter ) { llResetScript(); }
    
    state_entry() {
        debug("The script entered state 'default' registering Vendor");
        // WE need to get the correct Owner of the Vendor from the Tree
        LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");
        llSay(LISTEN_CHANNEL, "GET_OWNER");
    }

    touch_end(integer total_number) {
        // if someone touches object describe what's for sale
        BUYER_ID = llDetectedKey(0);
        if ( ( BUYER_ID != NULL_KEY ) && ( VENDOR_OWNER != NULL_KEY ) ) {
            if ( BUYER_ID == VENDOR_OWNER ) {
                registerItem(BUYER_ID);
            } else {
                llInstantMessage(BUYER_ID, "Die Ernte kann nur durch den Besitzer bezogen werden");
            }
        }
    }

    listen ( integer channel, string name , key id, string message ) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        list params = llParseString2List(message, ["::"], []);
        VENDOR_OWNER = (key) llList2String(params, 0);
        VENDOR_ORIGIN = (key) llList2String(params, 1);
        registerVendor( (string) VENDOR_OWNER, (string) VENDOR_ORIGIN );
        llListenRemove(LISTEN_HANDLE);
    }



    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER_VENDOR) {
            if (status == 200) {
    			llInstantMessage( VENDOR_OWNER, "20 Kg Kokosnüsse sind geerntet und stehen zum Abholen bereit" );
            } else {
               	debug("Registering Vendor failed with Code: " + (string) status);
            }
        } else if (request_id == REST_REGISTER_ITEM) {
            if (status == 200) {
                llRegionSay(GIVER_CHANNEL, "GIVE_ITEM::"+GIVER_ITEM_NAME+"::"+(string)NULL_KEY+"::"+(string)VENDOR_OWNER+"::"+GIVER_ITEM_TYPE+"::"+(string)NULL_KEY);
                llSleep(1.0);
                llSay(LISTEN_CHANNEL, "VENDOR_DELETE::"+(string)llGetKey()+"::"+(string)VENDOR_ORIGIN);
                llDie();
            } else {
                debug("Registering Item failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
        BUYER_ID = NULL_KEY;

    }   
}
// END //
