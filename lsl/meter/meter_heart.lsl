/*
 * Copyright (c) Akira Sonoda 2017.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

// Message format
//
// METER|int HUNGER|int THIRST|int HEALTH
 
integer FLAG_DEBUG=TRUE;                             // show debugging messages?
integer CHANATTACH;                             // dynamic channel for player attachments 
integer HANDATTACH;                             // attach channel handle for llRemove
string  CHAN_PREFIX = "0x";                     // prefix to convert to hexadecimal
string  DIVIDER = "|";                          // Myriad message field divider
vector  COLOR = <0,1,0>;                        // set text color
float   ALPHA = 1.0;                            // set text alpha 0.0 = clear, 1.0 = solid
vector  GREEN  = <0.000, 0.502, 0.000>;
vector  YELLOW = <0.502, 0.502, 0.000>;                       
vector  ORANGE = <1.000, 0.502, 0.000>;                       
vector  RED    = <1.000, 0.000, 0.000>;                       
key     WHO_TOUCHED = NULL_KEY; 
integer hunger = 0;
integer thirst = 0;
integer health = 0;



debug(string debugmsg) {
    if ( FLAG_DEBUG == TRUE ) llOwnerSay("METER DEBUG: "+debugmsg);
}
 
 
setup() {
    debug("setup()");
    llSetText("--- Waiting for Economy Update ---",<1,0,0>,1);                      // set a default banner to show we haven't been updated yet
    CHANATTACH = (integer)(CHAN_PREFIX+llGetSubString((string)llGetOwner(),1,7));   // calculate wearer's dynamic attachment channel
    if ( HANDATTACH != 0 ) llListenRemove(HANDATTACH);                              // remove previously open channel
    HANDATTACH = llListen(CHANATTACH,"",NULL_KEY,"");                               // start a listener on the dynamic channel
    llWhisper(CHANATTACH,"ATTACHMETER");                                            // tell HUD we're attached
}

userMessage(string umessage) {
    if(WHO_TOUCHED == llGetOwner()) {
        llOwnerSay(umessage);
    } else {
        llInstantMessage(WHO_TOUCHED, umessage);
    }
}


default {
 
    attach(key id) {
        debug("attach("+(string)id+")");
        // wearing, let's setup
        if ( id != NULL_KEY ) {
            setup();                                                                
            return;
        }
        // detach, drop, derezzed to inventory - NOT ON LOGOUT        
        if ( id == NULL_KEY ) {                                                     
            if ( HANDATTACH != 0 ) llListenRemove(HANDATTACH);
            llWhisper(CHANATTACH,"DETACHMETER");
            return;
        }
    }

    changed(integer changes) {
        debug("changed("+(string)changes+")");
        // if owner has changed, we need to recalculate the dynamic channel
        // if ( changes & CHANGED_OWNER ) {                                           
        //    setup();                                                                
        //    return;
        // }
        // owner jumped to new location? restart
        if ( changes & CHANGED_REGION || changes & CHANGED_TELEPORT ) {
            setup();                                                                
            return;
        }
    }
 
    listen(integer channel,string name,key id,string message) {
        debug("listen("+(string)channel+","+name+","+(string)id+","+message+")");
        if ( channel == CHANATTACH ) {                                              
            list fields = llParseString2List(message,[DIVIDER],[]);                 
            string command = llList2String(fields,0);                               
            if ( command == "METER") {                                              
                hunger   = llList2Integer(fields,1);                         
                thirst   = llList2Integer(fields,2);
                health   = llList2Integer(fields,3); 
 
                llSetText("",COLOR,ALPHA); 
                // PICK Color based on status
                COLOR = GREEN; // start with fully healthy color
                if ( hunger >= 25 || thirst >= 25) { COLOR = YELLOW; } 
                if ( hunger >= 50 || thirst >= 50) { COLOR = ORANGE; }    
                if ( hunger >= 75 || thirst >= 75) { COLOR = RED; } 
                llSetLinkColor(LINK_SET,COLOR, ALL_SIDES);
                return; 
            } 
            if ( command == "REGISTERATTACHMENTS" ) { // HUD asking for attachments attached?
                setup(); // just setup
                return;
            }
        } 
    }

    touch_end(integer num_detected) {
        WHO_TOUCHED = llDetectedKey(0);
        if (hunger < 25 && thirst < 25) { userMessage("Alles Ok"); return; }
        else if (hunger < 25 && thirst >= 25) { userMessage("Ich bin etwas durstig"); return;}
        else if (hunger < 25 && thirst >= 50) { userMessage("Ich bin sehr durstig"); return;}
        else if (hunger < 25 && thirst >= 75) { userMessage("Ich bin am verdursten"); return;}
        else if (hunger >= 25 && thirst < 25) { userMessage("Ich bin etwas hungrig"); return;}
        else if (hunger >= 25 && thirst >= 25) { userMessage("Ich bin etwas hungrig und etwas durstig"); return;}
        else if (hunger >= 25 && thirst >= 50) { userMessage("Ich bin etwas hungrig und sehr durstig"); return;}
        else if (hunger >= 25 && thirst >= 75) { userMessage("Ich bin etwas hungrig und am verdursten"); return;}
        else if (hunger >= 50 && thirst < 25)  { userMessage("Ich bin sehr hungrig"); return;}
        else if (hunger >= 50 && thirst >= 25) { userMessage("Ich bin sehr hungrig und etwas durstig"); return;}
        else if (hunger >= 50 && thirst >= 50) { userMessage("Ich bin sehr hungrig und sehr durstig"); return;}
        else if (hunger >= 50 && thirst >= 75) { userMessage("Ich bin sehr hungrig und am verdursten"); return;}
        else if (hunger >= 75 && thirst < 25)  { userMessage("Ich bin am verhungern"); return;}
        else if (hunger >= 75 && thirst >= 25) { userMessage("Ich bin am verhungern und etwas durstig"); return;}
        else if (hunger >= 75 && thirst >= 50) { userMessage("Ich bin am verhungern und sehr durstig"); return;}
        else if (hunger >= 75 && thirst >= 75) { userMessage("Ich bin am verhungern und am verdursten"); return;}
    }
 
    on_rez(integer param) {
        debug("on_rez");
        param = 0; // LSLINT
        setup(); 
    }
 
    state_entry() {
        debug("state_entry");
        setup(); 
    }
 
} 
