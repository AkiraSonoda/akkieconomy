/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

 // Market Buyer
string  BUYER_NAME="Market Buyer";  
string  OBJECT_TYPE = "MARKET_BUYER";
integer OBJECT_PRICE = 0;

integer FLAG_DEBUG = FALSE; // see debug messages?

// KEYS for the HTTP Requests
key REST_REGISTER = NULL_KEY;
key REST_CHECK_ITEM = NULL_KEY;
key REST_GET_PRICE = NULL_KEY;

key ITEM_SELLER = NULL_KEY;
key ITEM_ID = NULL_KEY;
string ITEM_TYPE = NULL_KEY;

integer NEGOTIATION_CHANNEL = 1002000001;
integer TRANSACTION_CHANNEL = 1002000002;
integer LISTEN_HANDLE = 0;
integer PRICE_OFFERING = 0;


string ECONOMYURL="http://akitest.dyndns.info:8010";

string BUYER_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"@jNAME\",\"itemType\":\"@jTYPE\",\"price\":@jPRICE,\"origin\":\"00000000-0000-0000-0000-000000000000\"}";
string SOLD_ITEM_JSON = "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"ownerid\":\"@jOWNERID\",\"name\":\"check\",\"itemType\":\"@jITEMTYPE\",\"price\":@jPRICE,\"origin\":\"00000000-0000-0000-0000-000000000000\"}";

// Helper
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}

userMessage(string umessage) {
    if(ITEM_SELLER == llGetOwner()) {
        llOwnerSay(umessage);
    } else {
        llInstantMessage(ITEM_SELLER, umessage);
    }
}

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}


list json2List(string jsonString) {
    string aString = jsonString;
    aString = searchAndReplace(aString,"\"", "");
    aString = searchAndReplace(aString," ", "");
    aString = searchAndReplace(aString,"\n", "");
    aString = searchAndReplace(aString,"{","");
    aString = searchAndReplace(aString,"}","");
    aString = searchAndReplace(aString,",",":");
    debug("Json without Boilerplate: " + aString);
    return llParseString2List(aString, [":"], []);
}

//
// REST Calls
//
registerBuyer() {
    debug("registerBuyer");
    string jsonString = BUYER_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) llGetOwner());
    jsonString = searchAndReplace(jsonString,"@jNAME",BUYER_NAME);
    jsonString = searchAndReplace(jsonString,"@jPRICE", (string) OBJECT_PRICE);
    jsonString = searchAndReplace(jsonString,"@jTYPE",OBJECT_TYPE);               
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/buyer", POST_PARAMS, jsonString);
}


// give the item to a customer
checkItem( key toWhom ) {
    string jsonString = SOLD_ITEM_JSON;
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string)toWhom);
    jsonString = searchAndReplace(jsonString,"@jITEMTYPE",ITEM_TYPE);
    jsonString = searchAndReplace(jsonString,"@jPRICE",(string)PRICE_OFFERING);
    debug("JSON-String to insert MarketInventory: "+jsonString);
    list PUT_PARAMS =[HTTP_METHOD,"PUT",HTTP_MIMETYPE,"application/json"];
    REST_CHECK_ITEM = llHTTPRequest(ECONOMYURL+"/rest/item/buyer/"+(string)llGetKey()+"/"+(string)llGetOwner()+"/00000000-0000-0000-0000-000000000000", PUT_PARAMS, jsonString);
}

// get the price of the Item to be bought
getPrices(string item) {
    list GET_PARAMS =[HTTP_METHOD,"GET",HTTP_MIMETYPE,"application/json"];
    REST_GET_PRICE = llHTTPRequest(ECONOMYURL+"/rest/item/buyer/price/"+item, GET_PARAMS, "");
}

showDialog(integer price) {
     llDialog( ITEM_SELLER, "Preis Offerte", [(string)price+" L$","Ablehnen"], TRANSACTION_CHANNEL );
}

default {

    on_rez( integer Parameter ) { llResetScript(); }
    
    state_entry() {
        debug("The script entered state 'default' registering Container");
        llRequestPermissions(llGetOwner(), PERMISSION_DEBIT);
        registerBuyer();
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
                llOwnerSay("Vendor registriert und in einigen Sekunden bereit");
                state negotiating;
            } else {
                debug("Registering Vendor failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }
} 

state negotiating {

    on_rez( integer Parameter ) { llResetScript(); }

    state_entry() {
        llSetClickAction(CLICK_ACTION_PAY);
        llSetPayPrice(PAY_HIDE,[OBJECT_PRICE ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
        debug("The script entered state 'buying' opening negitiation channel");
        LISTEN_HANDLE = llListen(NEGOTIATION_CHANNEL, "", "", "GET_CHANNEL");

    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);
        if (message == "GET_CHANNEL") {
            llWhisper(NEGOTIATION_CHANNEL,(string)TRANSACTION_CHANNEL);
            llListenRemove(LISTEN_HANDLE);
            state trading;
        }
    }

}

state trading { 
    state_entry() {
        LISTEN_HANDLE = llListen(TRANSACTION_CHANNEL, "", "", "");
    }

    listen(integer channel , string name, key id, string message) {
        debug("listen: channel: " +(string) channel+ " message: " + message);

        if (message == (string) PRICE_OFFERING +" L$") {
            checkItem( ITEM_SELLER );
        } else if ( message == "Ablehnen") {
            llListenRemove(LISTEN_HANDLE);
            state negotiating;
        } else {
            list params = llParseString2List(message, ["::"], []); // BUYER-ID and ORIGIN 
            string command = (string) llList2String(params, 0);
            ITEM_ID  = (key) llList2String(params, 1);
            ITEM_SELLER = (key) llList2String(params, 2);
            ITEM_TYPE = llList2String(params, 3);
            if(command == "ITEM_TO_SELL") {
                // Get the Price as JSON: {"kind" : "BASKET_COCONUT","price" : 50}
                debug("Getting the price for: " + ITEM_TYPE);
                getPrices(ITEM_TYPE);
            }
        }
    }

    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_GET_PRICE) {
            if (status == 200) {
                debug("Result from Price Inquiry: " + body);
                list priceList = json2List(body);
                PRICE_OFFERING = llList2Integer(priceList, 3);
                debug("Price: "+(string) PRICE_OFFERING);
                showDialog(PRICE_OFFERING);
            } else {
                debug("Price Request failed with Code: " + (string) status);
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            }
        } else if (request_id == REST_CHECK_ITEM) {
            if ( status == 200 ) {
                llWhisper(TRANSACTION_CHANNEL, "TRANSACTION_OK");
                userMessage("Verkauft für "+(string)PRICE_OFFERING+" OS$");
                llGiveMoney(ITEM_SELLER, PRICE_OFFERING);
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            } else if ( status == 404 ) {
                userMessage("Unregistriertes Objekt kann nicht verkauft werden");
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
                
            } else {
                debug("Price Request failed with Code: " + (string) status);
                llListenRemove(LISTEN_HANDLE);
                state negotiating;
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    }
}

// END //
