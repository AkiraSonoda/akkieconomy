/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

string  REZZER_NAME = "Watercooler";
integer OBJECT_PRICE = 100;
string  OBJECT_ITEM_TYPE = "WATERCOOLER";


integer LISTEN_HANDLE = 0;
integer LISTEN_CHANNEL = 1001000004;
key REST_REGISTER = NULL_KEY;

integer FLAG_DEBUG = FALSE; // see debug messages?

string ECONOMYURL="http://akitest.dyndns.info:8010";

string VENDOR_JSON = "{\"id\":\"@jID\",\"ownerid\":\"@jOWNERID\",\"name\":\"@jNAME\",\"itemType\":\"@jTYPE\",\"price\":@jPRICE,\"origin\":\"00000000-0000-0000-0000-000000000000\"}";

string searchAndReplace(string input, string old, string new)  {
   return llDumpList2String(llParseString2List(input, [old], []), new);
}


registerInventory() {
    debug("registerInventory");
    string jsonString = VENDOR_JSON;
    jsonString = searchAndReplace(jsonString,"@jID",(string) llGetKey()); 
    jsonString = searchAndReplace(jsonString,"@jOWNERID",(string) llGetOwner());
    jsonString = searchAndReplace(jsonString,"@jPRICE", (string) OBJECT_PRICE);                
    jsonString = searchAndReplace(jsonString,"@jTYPE", OBJECT_ITEM_TYPE);                
    jsonString = searchAndReplace(jsonString,"@jNAME", REZZER_NAME);                
    debug("JSON-String to register Container: "+jsonString);
    list POST_PARAMS =[HTTP_METHOD,"POST",HTTP_MIMETYPE,"application/json"];
    REST_REGISTER = llHTTPRequest(ECONOMYURL+"/rest/item/vendor", POST_PARAMS, jsonString);
}




// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}


default {
    state_entry() {
    	registerInventory();
    }


    http_response(key request_id, integer status, list metadata, string body) {
        debug("http_response"); 
        if (request_id == REST_REGISTER) {
            if (status == 200) {
                llOwnerSay(REZZER_NAME + " installiert und in einigen Sekunden bereit");
                state listening_forPosition;
            } else {
                debug("Registering Container failed with Code: " + (string) status);
            }
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received:"+(string)request_id);
        }
    } 


    on_rez(integer start_param){
        llResetScript();
    }

    changed(integer change) {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }
}

state listening_forPosition {
    state_entry() {
        debug("The script entered state 'listening_forPosition'");
        // LISTEN_HANDLE = llListen(3072, "WaterTank", llGetOwner(), "");
        LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");
    }

    listen(integer channel, string name, key id, string message) {
    	debug("Got a message from Client: " + message);
        if (message == "GET_POSITION") {
            vector position = llGetPos();
            position = <position.x, position.y, position.z + 0.08815>;
            llSay(LISTEN_CHANNEL, (string)position+"::"+(string) llGetKey());
            llListenRemove(LISTEN_HANDLE);
            state listening_forRemoval;
        }
    }

    on_rez(integer start_param){
        llResetScript();
    }
 
    state_exit() {
        debug("The script leaves state 'listening_forPosition'");
    }
}

state listening_forRemoval {
    state_entry() {
        debug("The script entered state 'listening_forPosition'");
        // LISTEN_HANDLE = llListen(3072, "WaterTank", llGetOwner(), "");
        LISTEN_HANDLE = llListen(LISTEN_CHANNEL, "", "", "");
    }

    listen(integer channel, string name, key id, string message) {
        debug("Got a message from Client: " + message);
        list fields = llParseString2List(message,["::"],[]);  // break into list of fields based on DIVider
        string command = llList2String(fields,0);
        string posObject = (key) llList2String(fields,1);
        if (command == "RemoveWaterTank") {
            if(posObject == llGetKey()) {
                state listening_forPosition;
            }
        }
    }


    on_rez(integer start_param){
        llResetScript();
    }
 
    state_exit() {
        debug("The script leaves state 'listening_forPosition'");
    }
}
