/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */
integer FLAG_DEBUG = TRUE; // see debug messages?

// KEYS for the HTTP Requests
key REST_DELETE = NULL_KEY;

string ECONOMYURL="http://akitest.dyndns.info:8010";

// Helper Functions
//
// debug - show debug chat with wearer name for sorting
debug(string dmessage) {
    if ( FLAG_DEBUG == TRUE ) { // are we debugging?
        llOwnerSay("DEBUG "+dmessage);
    }
}


deleteContainer() {
    debug("deleteContainer");
    list DELETE_PARAMS =[HTTP_METHOD,"DELETE"];
    REST_DELETE = llHTTPRequest(ECONOMYURL+"/rest/item/container/3c86d5d7-2d05-4bdb-9318-34ce27e4afcb", DELETE_PARAMS," ");
}

// setup() {
//     debug("setup()");
//     CHANATTACH = (integer)(CHAN_PREFIX+llGetSubString((string)llGetOwner(),1,7));   // calculate wearer's dynamic attachment channel
//     HANDATTACH = llListen(CHANATTACH,"",NULL_KEY,"");                               // start a listener on the dynamic channel
// }



default {
    state_entry() {
    	state serving;
    }

    on_rez(integer start_param){
        llResetScript();
    }

    changed(integer change) {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }
}



state serving {
    state_entry() {
        debug("The script entered state 'serving' registering Container");
    }

    touch_end(integer num_detected) {
        debug("User clicked Updating the Container");
        deleteContainer();

    }


    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id == REST_DELETE) {
	        debug("http_response::REST_DELETE"); 
            if(status == 200) {
                llOwnerSay("Wassertank ist leer wird nun recycled");
                llSay(3074, "RemoveWaterTank|"+(string)llGetKey());
                llDie();                
            } else {
 				debug("REST_DELETE Status:" + (string)status);           	
            }           
        } else if (request_id == NULL_KEY) {
            debug("Null Key received could not fire the http request");
        } else  {
            debug("Unknown Request received: "+(string)request_id);
        }
    } 
 
    on_rez(integer start_param){
        debug("Reset Change from 'serving' to 'default'");
        llResetScript();
    }
 
    state_exit() {
        debug("The script leaves state 'serving'");
    }
}

