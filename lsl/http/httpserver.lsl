string url;
key urlRequestId;
 
default {
    on_rez(integer start_param) {
        llResetScript();
    }
 
    changed(integer change) {
        if (change & (CHANGED_OWNER | CHANGED_INVENTORY))
            llResetScript();
 
        if (change & (CHANGED_REGION | CHANGED_REGION_START | CHANGED_TELEPORT)) {
            llOwnerSay("Teleporting...");
            urlRequestId = llRequestURL();
            llResetScript();
        }
    }

    state_entry() {
        urlRequestId = llRequestURL();
        llOwnerSay("Requested Key: "+(string)urlRequestId);
    }
 
    http_request(key id, string method, string body) {
        llOwnerSay("Url Key: " + (string) id);
        if (method == URL_REQUEST_DENIED) {
            llOwnerSay("The following error occurred while attempting to get a free URL for this device:\n \n" + body);
        } else if (method == URL_REQUEST_GRANTED) {
            url = body;
            llOwnerSay("URL: " +url);
        } else if (method == "GET") {
                llHTTPResponse(id,200,"Hello World!");
        }
    }
}